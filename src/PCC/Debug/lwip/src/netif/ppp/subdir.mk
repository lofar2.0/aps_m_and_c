################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../lwip/src/netif/ppp/auth.c \
../lwip/src/netif/ppp/ccp.c \
../lwip/src/netif/ppp/chap-md5.c \
../lwip/src/netif/ppp/chap-new.c \
../lwip/src/netif/ppp/chap_ms.c \
../lwip/src/netif/ppp/demand.c \
../lwip/src/netif/ppp/eap.c \
../lwip/src/netif/ppp/eui64.c \
../lwip/src/netif/ppp/fsm.c \
../lwip/src/netif/ppp/ipcp.c \
../lwip/src/netif/ppp/ipv6cp.c \
../lwip/src/netif/ppp/lcp.c \
../lwip/src/netif/ppp/lwip_ecp.c \
../lwip/src/netif/ppp/magic.c \
../lwip/src/netif/ppp/mppe.c \
../lwip/src/netif/ppp/multilink.c \
../lwip/src/netif/ppp/ppp.c \
../lwip/src/netif/ppp/pppapi.c \
../lwip/src/netif/ppp/pppcrypt.c \
../lwip/src/netif/ppp/pppoe.c \
../lwip/src/netif/ppp/pppol2tp.c \
../lwip/src/netif/ppp/pppos.c \
../lwip/src/netif/ppp/upap.c \
../lwip/src/netif/ppp/utils.c \
../lwip/src/netif/ppp/vj.c 

OBJS += \
./lwip/src/netif/ppp/auth.o \
./lwip/src/netif/ppp/ccp.o \
./lwip/src/netif/ppp/chap-md5.o \
./lwip/src/netif/ppp/chap-new.o \
./lwip/src/netif/ppp/chap_ms.o \
./lwip/src/netif/ppp/demand.o \
./lwip/src/netif/ppp/eap.o \
./lwip/src/netif/ppp/eui64.o \
./lwip/src/netif/ppp/fsm.o \
./lwip/src/netif/ppp/ipcp.o \
./lwip/src/netif/ppp/ipv6cp.o \
./lwip/src/netif/ppp/lcp.o \
./lwip/src/netif/ppp/lwip_ecp.o \
./lwip/src/netif/ppp/magic.o \
./lwip/src/netif/ppp/mppe.o \
./lwip/src/netif/ppp/multilink.o \
./lwip/src/netif/ppp/ppp.o \
./lwip/src/netif/ppp/pppapi.o \
./lwip/src/netif/ppp/pppcrypt.o \
./lwip/src/netif/ppp/pppoe.o \
./lwip/src/netif/ppp/pppol2tp.o \
./lwip/src/netif/ppp/pppos.o \
./lwip/src/netif/ppp/upap.o \
./lwip/src/netif/ppp/utils.o \
./lwip/src/netif/ppp/vj.o 

C_DEPS += \
./lwip/src/netif/ppp/auth.d \
./lwip/src/netif/ppp/ccp.d \
./lwip/src/netif/ppp/chap-md5.d \
./lwip/src/netif/ppp/chap-new.d \
./lwip/src/netif/ppp/chap_ms.d \
./lwip/src/netif/ppp/demand.d \
./lwip/src/netif/ppp/eap.d \
./lwip/src/netif/ppp/eui64.d \
./lwip/src/netif/ppp/fsm.d \
./lwip/src/netif/ppp/ipcp.d \
./lwip/src/netif/ppp/ipv6cp.d \
./lwip/src/netif/ppp/lcp.d \
./lwip/src/netif/ppp/lwip_ecp.d \
./lwip/src/netif/ppp/magic.d \
./lwip/src/netif/ppp/mppe.d \
./lwip/src/netif/ppp/multilink.d \
./lwip/src/netif/ppp/ppp.d \
./lwip/src/netif/ppp/pppapi.d \
./lwip/src/netif/ppp/pppcrypt.d \
./lwip/src/netif/ppp/pppoe.d \
./lwip/src/netif/ppp/pppol2tp.d \
./lwip/src/netif/ppp/pppos.d \
./lwip/src/netif/ppp/upap.d \
./lwip/src/netif/ppp/utils.d \
./lwip/src/netif/ppp/vj.d 


# Each subdirectory must supply rules for building sources it contributes
lwip/src/netif/ppp/%.o: ../lwip/src/netif/ppp/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -std=gnu11 -D__NEWLIB__ -DXIP_BOOT_HEADER_DCD_ENABLE -DDATA_SECTION_IS_CACHEABLE -DCPU_MIMXRT1062DVL6A -DCPU_MIMXRT1062DVL6A_cm7 -DXIP_EXTERNAL_FLASH=1 -DXIP_BOOT_HEADER_ENABLE=1 -DSERIAL_PORT_TYPE_UART=1 -DSDK_DEBUGCONSOLE=0 -DFSL_RTOS_FREE_RTOS -DSDK_OS_FREE_RTOS -D__MCUXPRESSO -D__USE_CMSIS -DDEBUG -DFSL_SDK_ENABLE_DRIVER_CACHE_CONTROL -DUA_ARCHITECTURE_FREERTOSLWIP -DUA_ARCH_FREERTOS_USE_OWN_MEMORY_FUNCTIONS -DOPEN62541_FEERTOS_USE_OWN_MEM -DFSL_FEATURE_PHYKSZ8081_USE_RMII50M_MODE -D_POSIX_SOURCE -DUSE_RTOS=1 -DPRINTF_ADVANCED_ENABLE=1 -DCR_INTEGER_PRINTF -DPRINTF_FLOAT_ENABLE=0 -I"D:\projects\MCUXpresso\PCC\component\i2c" -I"D:\projects\MCUXpresso\PCC\drivers" -I"D:\projects\MCUXpresso\PCC\source" -I"D:\projects\MCUXpresso\PCC\utilities" -I"D:\projects\MCUXpresso\PCC\CMSIS" -I"D:\projects\MCUXpresso\PCC\xip" -I"D:\projects\MCUXpresso\PCC\component\lists" -I"D:\projects\MCUXpresso\PCC\device" -I"D:\projects\MCUXpresso\PCC\component\timer_manager" -I"D:\projects\MCUXpresso\PCC\component\serial_manager" -I"D:\projects\MCUXpresso\PCC\component\uart" -I"D:\projects\MCUXpresso\PCC\component\timer" -I"D:\projects\MCUXpresso\PCC\drivers" -I"D:\projects\MCUXpresso\PCC\component\timer" -I"D:\projects\MCUXpresso\PCC\device" -I"D:\projects\MCUXpresso\PCC\source" -I"D:\projects\MCUXpresso\PCC\component\uart" -I"D:\projects\MCUXpresso\PCC\component\i2c" -I"D:\projects\MCUXpresso\PCC\component\serial_manager" -I"D:\projects\MCUXpresso\PCC\CMSIS" -I"D:\projects\MCUXpresso\PCC\xip" -I"D:\projects\MCUXpresso\PCC\utilities" -I"D:\projects\MCUXpresso\PCC\component\lists" -I"D:\projects\MCUXpresso\PCC\component\timer_manager" -I"D:\projects\MCUXpresso\PCC\amazon-freertos\freertos_kernel\include" -I"D:\projects\MCUXpresso\PCC\amazon-freertos\freertos_kernel\portable\GCC\ARM_CM4F" -I"D:\projects\MCUXpresso\PCC\board" -I"D:\projects\MCUXpresso\PCC" -I"D:\projects\MCUXpresso\PCC\lwip\port\arch" -I"D:\projects\MCUXpresso\PCC\lwip\src\include\compat\posix\arpa" -I"D:\projects\MCUXpresso\PCC\lwip\src\include\compat\posix\net" -I"D:\projects\MCUXpresso\PCC\lwip\src\include\compat\posix" -I"D:\projects\MCUXpresso\PCC\lwip\src\include\compat\posix\sys" -I"D:\projects\MCUXpresso\PCC\lwip\src\include\compat\stdc" -I"D:\projects\MCUXpresso\PCC\lwip\src\include\lwip" -I"D:\projects\MCUXpresso\PCC\lwip\src\include\lwip\priv" -I"D:\projects\MCUXpresso\PCC\lwip\src\include\lwip\prot" -I"D:\projects\MCUXpresso\PCC\lwip\src\include\netif" -I"D:\projects\MCUXpresso\PCC\lwip\src\include\netif\ppp" -I"D:\projects\MCUXpresso\PCC\lwip\src\include\netif\ppp\polarssl" -I"D:\projects\MCUXpresso\PCC\lwip\port" -I"D:\projects\MCUXpresso\PCC\lwip\src" -I"D:\projects\MCUXpresso\PCC\lwip\src\include" -I"D:\projects\MCUXpresso\PCC\open62541" -I"D:\projects\MCUXpresso\PCC\nanopb" -I"D:\projects\MCUXpresso\PCC\nanopb\generated" -O0 -fno-common -g3 -Wall -c -ffunction-sections -fdata-sections -ffreestanding -fno-builtin -fmacro-prefix-map="../$(@D)/"=. -mcpu=cortex-m7 -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -D__NEWLIB__ -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


