################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../lwip/src/core/ipv6/dhcp6.c \
../lwip/src/core/ipv6/ethip6.c \
../lwip/src/core/ipv6/icmp6.c \
../lwip/src/core/ipv6/inet6.c \
../lwip/src/core/ipv6/ip6.c \
../lwip/src/core/ipv6/ip6_addr.c \
../lwip/src/core/ipv6/ip6_frag.c \
../lwip/src/core/ipv6/mld6.c \
../lwip/src/core/ipv6/nd6.c 

OBJS += \
./lwip/src/core/ipv6/dhcp6.o \
./lwip/src/core/ipv6/ethip6.o \
./lwip/src/core/ipv6/icmp6.o \
./lwip/src/core/ipv6/inet6.o \
./lwip/src/core/ipv6/ip6.o \
./lwip/src/core/ipv6/ip6_addr.o \
./lwip/src/core/ipv6/ip6_frag.o \
./lwip/src/core/ipv6/mld6.o \
./lwip/src/core/ipv6/nd6.o 

C_DEPS += \
./lwip/src/core/ipv6/dhcp6.d \
./lwip/src/core/ipv6/ethip6.d \
./lwip/src/core/ipv6/icmp6.d \
./lwip/src/core/ipv6/inet6.d \
./lwip/src/core/ipv6/ip6.d \
./lwip/src/core/ipv6/ip6_addr.d \
./lwip/src/core/ipv6/ip6_frag.d \
./lwip/src/core/ipv6/mld6.d \
./lwip/src/core/ipv6/nd6.d 


# Each subdirectory must supply rules for building sources it contributes
lwip/src/core/ipv6/%.o: ../lwip/src/core/ipv6/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -std=gnu11 -D__NEWLIB__ -DXIP_BOOT_HEADER_DCD_ENABLE -DDATA_SECTION_IS_CACHEABLE -DCPU_MIMXRT1062DVL6A -DCPU_MIMXRT1062DVL6A_cm7 -DXIP_EXTERNAL_FLASH=1 -DXIP_BOOT_HEADER_ENABLE=1 -DSERIAL_PORT_TYPE_UART=1 -DSDK_DEBUGCONSOLE=0 -DFSL_RTOS_FREE_RTOS -DSDK_OS_FREE_RTOS -D__MCUXPRESSO -D__USE_CMSIS -DDEBUG -DFSL_SDK_ENABLE_DRIVER_CACHE_CONTROL -DUA_ARCHITECTURE_FREERTOSLWIP -DUA_ARCH_FREERTOS_USE_OWN_MEMORY_FUNCTIONS -DOPEN62541_FEERTOS_USE_OWN_MEM -DFSL_FEATURE_PHYKSZ8081_USE_RMII50M_MODE -D_POSIX_SOURCE -DUSE_RTOS=1 -DPRINTF_ADVANCED_ENABLE=1 -DCR_INTEGER_PRINTF -DPRINTF_FLOAT_ENABLE=0 -I"D:\projects\MCUXpresso\PCC\component\i2c" -I"D:\projects\MCUXpresso\PCC\drivers" -I"D:\projects\MCUXpresso\PCC\source" -I"D:\projects\MCUXpresso\PCC\utilities" -I"D:\projects\MCUXpresso\PCC\CMSIS" -I"D:\projects\MCUXpresso\PCC\xip" -I"D:\projects\MCUXpresso\PCC\component\lists" -I"D:\projects\MCUXpresso\PCC\device" -I"D:\projects\MCUXpresso\PCC\component\timer_manager" -I"D:\projects\MCUXpresso\PCC\component\serial_manager" -I"D:\projects\MCUXpresso\PCC\component\uart" -I"D:\projects\MCUXpresso\PCC\component\timer" -I"D:\projects\MCUXpresso\PCC\drivers" -I"D:\projects\MCUXpresso\PCC\component\timer" -I"D:\projects\MCUXpresso\PCC\device" -I"D:\projects\MCUXpresso\PCC\source" -I"D:\projects\MCUXpresso\PCC\component\uart" -I"D:\projects\MCUXpresso\PCC\component\i2c" -I"D:\projects\MCUXpresso\PCC\component\serial_manager" -I"D:\projects\MCUXpresso\PCC\CMSIS" -I"D:\projects\MCUXpresso\PCC\xip" -I"D:\projects\MCUXpresso\PCC\utilities" -I"D:\projects\MCUXpresso\PCC\component\lists" -I"D:\projects\MCUXpresso\PCC\component\timer_manager" -I"D:\projects\MCUXpresso\PCC\amazon-freertos\freertos_kernel\include" -I"D:\projects\MCUXpresso\PCC\amazon-freertos\freertos_kernel\portable\GCC\ARM_CM4F" -I"D:\projects\MCUXpresso\PCC\board" -I"D:\projects\MCUXpresso\PCC" -I"D:\projects\MCUXpresso\PCC\lwip\port\arch" -I"D:\projects\MCUXpresso\PCC\lwip\src\include\compat\posix\arpa" -I"D:\projects\MCUXpresso\PCC\lwip\src\include\compat\posix\net" -I"D:\projects\MCUXpresso\PCC\lwip\src\include\compat\posix" -I"D:\projects\MCUXpresso\PCC\lwip\src\include\compat\posix\sys" -I"D:\projects\MCUXpresso\PCC\lwip\src\include\compat\stdc" -I"D:\projects\MCUXpresso\PCC\lwip\src\include\lwip" -I"D:\projects\MCUXpresso\PCC\lwip\src\include\lwip\priv" -I"D:\projects\MCUXpresso\PCC\lwip\src\include\lwip\prot" -I"D:\projects\MCUXpresso\PCC\lwip\src\include\netif" -I"D:\projects\MCUXpresso\PCC\lwip\src\include\netif\ppp" -I"D:\projects\MCUXpresso\PCC\lwip\src\include\netif\ppp\polarssl" -I"D:\projects\MCUXpresso\PCC\lwip\port" -I"D:\projects\MCUXpresso\PCC\lwip\src" -I"D:\projects\MCUXpresso\PCC\lwip\src\include" -I"D:\projects\MCUXpresso\PCC\open62541" -I"D:\projects\MCUXpresso\PCC\nanopb" -I"D:\projects\MCUXpresso\PCC\nanopb\generated" -O0 -fno-common -g3 -Wall -c -ffunction-sections -fdata-sections -ffreestanding -fno-builtin -fmacro-prefix-map="../$(@D)/"=. -mcpu=cortex-m7 -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -D__NEWLIB__ -fstack-usage -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


