/*
 * Copyright 2020 Stichting Nederlandse Wetenschappelijk Onderzoek Instituten,
 * ASTRON Netherlands Institute for Radio Astronomy
 * Licensed under the Apache License, Version 2.0 (the "License");
 *
 * you may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * See ../LICENSE.txt for more info.
 */


/*
 * This file contains a bunch of miscelanious functions that were making the main file cluttered.
 * */

#ifndef INIT_AND_UTIL_H_
#define INIT_AND_UTIL_H_

///* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
#include "semphr.h"


#include "pin_mux.h"
//#include "commands.h"



/* System clock name. */
#define CoreSysClk kCLOCK_CoreSysClk
#define SEMC_CLK_FREQ CLOCK_GetFreq(kCLOCK_SemcClk)
#define SEMC_START_ADDRESS (0x80000000U)

//simple indicator LED pins and GPIO
#define LED_GPIO    BOARD_INITPINS_TESTIO_LED_GPIO
#define LED_PIN     BOARD_INITPINS_TESTIO_LED_PIN

#define WDOG_TESTIO0_RST_GPIO                                       BOARD_INITPINS_WDOG_RST_GPIO   /*!< GPIO device name: GPIO1 */
#define WDOG_TESTIO0_RST_PIN                                        BOARD_INITPINS_WDOG_RST_PIN  /*!< GPIO1 pin index: 1 */


#define TICK_PERIOD_MS      portTICK_PERIOD_MS  //how many ms there are in a tick (note: unsigned int, beware low number)


//Defined because these arguments are required and very large. Using a simple define improves readability
#define method_func_inargs       UA_Server *server,                                 \
                                const UA_NodeId *sessionId, void *sessionHandle,    \
                                const UA_NodeId *methodId, void *methodContext,     \
                                const UA_NodeId *objectId, void *objectContext,     \
                                size_t inputSize, const UA_Variant *input,          \
                                size_t outputSize, UA_Variant *output


#define callback_func_inargs    UA_Server *server,                                      \
                                const UA_NodeId *sessionId, void *sessionContext,       \
                                const UA_NodeId *nodeid, void *nodeContext,             \
                                const UA_NumericRange *range, const UA_DataValue *data


void WDOG1_IRQHandler(void)
{
    //Set testIO to 1. THis pin is connected to the power reset pin
    GPIO_PinWrite(WDOG_TESTIO0_RST_GPIO, WDOG_TESTIO0_RST_PIN, 0);

    WDOG_ClearInterruptStatus(WDOG1, kWDOG_InterruptFlag);
    /* User code. User can do urgent case before timeout reset.
     * IE. user can backup the ram data or ram log to flash.
     * the period is set by config.interruptTimeValue, user need to
     * check the period between interrupt and timeout.
     */
    SDK_ISR_EXIT_BARRIER;
}
#pragma GCC push_options
#pragma GCC optimize ("O0")

class opc_ua_time{
public:
    UA_NodeId control_nodeID;
    UA_NodeId monitor_nodeID;
    UA_NodeId object_nodeID;
    static opc_ua_time *self;

    opc_ua_time(UA_Server *_server);
    static void set_unix_offset(callback_func_inargs);
};

opc_ua_time *opc_ua_time::self = nullptr;

void opc_ua_time::set_unix_offset(callback_func_inargs){
/*      NOTE: added a function to open62541 to set and get the time offset. ( UA_DateTime UA_DateTime_offset(UA_DateTime set_offset=0) )
 *      NOTE: also modified the UA_DateTime_now() function to include the offset function
 *          modification included: changing  "UA_DATETIME_UNIX_EPOCH" to "UA_DateTime_offset()"
 *
 *
 *      NOTE: added function below:
 *      UA_DateTime UA_DateTime_offset(UA_DateTime set_offset) {
            static UA_DateTime offset = UA_DATETIME_UNIX_EPOCH;

            if(!set_offset){
                //if offset == 0 this is a get function, otherwise a set
                return offset;
            }
            //else set the offset
            offset = set_offset;
            return 0;
        }
 *      NOTE: if, default value is zero. If no input arguments are used, this is a get function
 * */

    int64_t unix_offset = *(int64_t*)data->value.data;     //get the offset value in unix time
    UA_DateTime offset;                       //time variable
    offset = UA_DateTime_fromUnixTime(unix_offset);  //convert unix to DateTime
    UA_DateTime_offset(offset);               //call added offset function

    UA_Variant value_var;
    UA_Variant_init(&value_var);
    UA_Variant_setScalar(&value_var, &unix_offset, &UA_TYPES[UA_TYPES_INT64]);
    status_t status = UA_Server_writeValue(server, self->monitor_nodeID, value_var);
    if(status) {
      printf("couldn't time offset node\r\n");
    }
    printf("changed time offset\r\n");
}


opc_ua_time::opc_ua_time(UA_Server *server){
    self = this;
    control_nodeID = UA_NODEID_STRING(2, "time_offset_RW");
    monitor_nodeID = UA_NODEID_STRING(2, "time_offset_R");
    object_nodeID = UA_NODEID_STRING(2, "time_offset_node");

    //--------------------------------------------------------------------------------
    //add object node

    UA_ObjectAttributes oAttr = UA_ObjectAttributes_default;
    oAttr.description = UA_LOCALIZEDTEXT("en-US", "control an monitor point to set the time offset, takes unix time int64_t");
    status_t status = UA_Server_addObjectNode(server, UA_NODEID_NULL,
                            UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER),
                            UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                            UA_QUALIFIEDNAME(2, "time_offset"),
                            UA_NODEID_NUMERIC(0, UA_NS0ID_BASEOBJECTTYPE),
                            oAttr, NULL, &object_nodeID);
    if(status) printf("couldn't add node\r\n");

    //----------------------------------------------------------------------------------
    // add control point
    UA_VariableAttributes vAttr = UA_VariableAttributes_default;
    int64_t initial_value = 0;

    vAttr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;
    vAttr.description = UA_LOCALIZEDTEXT("en-US", "control point for setting the time offset");
    UA_Variant_setScalar(&vAttr.value, &initial_value, &UA_TYPES[UA_TYPES_INT64]);
    status = UA_Server_addVariableNode(server, UA_NODEID_NULL,   //UA_NODEID_NULL
                        self->object_nodeID,
                        UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                        UA_QUALIFIEDNAME(2, "time_offset_RW"),
                        UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                        vAttr, NULL, &control_nodeID);
    if(status) printf("couldn't add node\r\n");

    //----------------------------------------------------------------------------------
    // add monitor point
    vAttr = UA_VariableAttributes_default;
    initial_value = 0;

    vAttr.description = UA_LOCALIZEDTEXT("en-US", "monitor point for viewing the time offset");
    UA_Variant_setScalar(&vAttr.value, &initial_value, &UA_TYPES[UA_TYPES_INT64]);
    status = UA_Server_addVariableNode(server, UA_NODEID_NULL,   //UA_NODEID_NULL
                        object_nodeID,
                        UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                        UA_QUALIFIEDNAME(2, "time_offset_R"),
                        UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                        vAttr, NULL, &monitor_nodeID);
    if(status) printf("couldn't add node\r\n");

    vTaskDelay(pdMS_TO_TICKS(1));

    UA_ValueCallback callback ;
    callback.onRead = nullptr;
    callback.onWrite = set_unix_offset;
    UA_StatusCode UA_status = UA_Server_setVariableNode_valueCallback(server, control_nodeID, callback);
}
#pragma GCC pop_options

enum ip_type {
    netif_ip,
    gateway_ip,
    netmask_ip
};

//function in anticipation of having to get the IP address through subrack identification
uint8_t* get_ip_address(ip_type type=netif_ip){
//    static uint8_t ipv4_address[4] = {192,168,137,102};

#define NETWORKED
#ifdef NETWORKED
    static uint8_t netif_ipv4_address[4] = {10,87,2,8};
    static uint8_t gateway_ipv4_address[4] = {10, 87, 5, 200};
    static uint8_t netmask_ipv4_address[4] = {255, 255, 255, 0};
#else
    static uint8_t netif_ipv4_address[4] = {192,168,137,102};
    static uint8_t gateway_ipv4_address[4] = {192, 168, 137, 1};
    static uint8_t netmask_ipv4_address[4] = {255, 255, 255, 0};
#endif

    static bool init = false;

    //first time read the IP address and then store it
    if(init == false){
        printf("TODO: get non-hard coded IP address\r\n");

        //TODO: figure out what kind of subrack identification exists.
        //TODO: read the subrack identification and use it to determine the IP address
        init = true;
    }

    if(type == netif_ip){
        return netif_ipv4_address;
    }
    else if(type == gateway_ip){
        return gateway_ipv4_address;
    }
    else if (type == netmask_ip){
        return netmask_ipv4_address;
    }
    return nullptr;
}



//serves as both a set and get method to the cmd_handler object
cmd_handler* set_get_cmd_handler_ptr(cmd_handler *cmd_handler_ptr=nullptr){
    static struct cmd_handler *handler_ptr = nullptr;

    //if null its a coming from the interrupt and will thus be a get command
    if(cmd_handler_ptr == nullptr){
        return handler_ptr;
    }
    else {
        handler_ptr = cmd_handler_ptr;
        return nullptr;
    }
}

/* GPIO1_Combined_0_15_IRQn interrupt handler */
void PULSE_GPIO1_0_15_IRQHANDLER(void) {
    volatile static struct cmd_handler    *IRQ_cmd_handler_ptr  = nullptr;


    static uint32_t pulse_cnt = 0;  //counts all pulses

    //check if the object has been intitialised yet
    if(!IRQ_cmd_handler_ptr){
        //skips this pulse but that doesn't matter that much
        IRQ_cmd_handler_ptr = set_get_cmd_handler_ptr();
        GPIO_PortClearInterruptFlags(BOARD_INITPINS_PULSE_GPIO, 1U << BOARD_INITPINS_PULSE_PIN);
        SDK_ISR_EXIT_BARRIER;
        return;
    }

    pulse_cnt++;
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;

    /* clear the interrupt status */
    GPIO_PortClearInterruptFlags(BOARD_INITPINS_PULSE_GPIO, 1U << BOARD_INITPINS_PULSE_PIN);


    if(IRQ_cmd_handler_ptr->active_deadline){

        //if this is the sync pulse
        if(IRQ_cmd_handler_ptr->next_deadline == 0){
            //go to the command handler task after this
            vTaskNotifyGiveFromISR( nullptr, &xHigherPriorityTaskWoken );
        }
        else{
            IRQ_cmd_handler_ptr->next_deadline--;
        }
    }
    portYIELD_FROM_ISR( xHigherPriorityTaskWoken );
}


//initialise the external memory
void BOARD_InitSEMC(void)
{
    status_t status =0;

    //SEMC
    CLOCK_InitSysPfd(kCLOCK_Pfd2, 29);
    /* Set semc clock to 163.86 MHz */
    CLOCK_SetMux(kCLOCK_SemcMux, 1);
    CLOCK_SetDiv(kCLOCK_SemcDiv, 1);

    semc_config_t config;
    semc_sdram_config_t sdramconfig;
    uint32_t clockFrq = SEMC_CLK_FREQ;

    /* Initializes the MAC configure structure to zero. */
    memset(&config, 0, sizeof(semc_config_t));
    memset(&sdramconfig, 0, sizeof(semc_sdram_config_t));

    /* Initialize SEMC. */
    SEMC_GetDefaultConfig(&config);
    config.dqsMode = kSEMC_Loopbackdqspad; /* For more accurate timing. */
    SEMC_Init(SEMC, &config);

    /* Configure SDRAM. (values from EmbeddedArtist)*/
    sdramconfig.csxPinMux           = kSEMC_MUXCSX0;
    sdramconfig.address             = 0x80000000;   //starting address
    sdramconfig.memsize_kbytes      = 32 * 1024; /* 32MB = 32*1024*1KBytes*/
    sdramconfig.portSize            = kSEMC_PortSize16Bit;
    sdramconfig.burstLen            = kSEMC_Sdram_BurstLen8;
    sdramconfig.columnAddrBitNum    = kSEMC_SdramColunm_9bit;
    sdramconfig.casLatency          = kSEMC_LatencyThree;
    sdramconfig.tPrecharge2Act_Ns   = 18; /* Trp 18ns */
    sdramconfig.tAct2ReadWrite_Ns   = 18; /* Trcd 18ns */
    sdramconfig.tRefreshRecovery_Ns = 67; /* Use the maximum of the (Trfc , Txsr). */
    sdramconfig.tWriteRecovery_Ns   = 12; /* 12ns */
    sdramconfig.tCkeOff_Ns          = 42; /* The minimum cycle of SDRAM CLK off state. CKE is off in self refresh at a minimum period tRAS.*/
    sdramconfig.tAct2Prechage_Ns       = 42; /* Tras 42ns */
    sdramconfig.tSelfRefRecovery_Ns    = 67;
    sdramconfig.tRefresh2Refresh_Ns    = 60;
    sdramconfig.tAct2Act_Ns            = 60;
    sdramconfig.tPrescalePeriod_Ns     = 160 * (1000000000 / clockFrq);
    sdramconfig.refreshPeriod_nsPerRow = 64 * 1000000 / 8192; /* 64ms/8192 */
    sdramconfig.refreshUrgThreshold    = sdramconfig.refreshPeriod_nsPerRow;
    sdramconfig.refreshBurstLen        = 1;

    status = SEMC_ConfigureSDRAM(SEMC, kSEMC_SDRAM_CS0, &sdramconfig, clockFrq);
    if(status){
        printf("\r\n SEMC SDRAM Init Failed\r\n");
        for(;;);
    }
    return;
}

void delay(void)
{
    volatile uint32_t i = 0;
    for (i = 0; i < 1000000; ++i)
    {
        __asm("NOP"); /* delay */
    }
}

//created the RTOS heap for HEAP_5. currently has 3 separate regions: DTC RAM, OC RAM and BOARD SRAM.
void RTOS_heap_init(void){
    /*
     *      Heap 5 allows for the heap to be split over multiple non adjacent regions.
     *      The only catch is that they are filled from high to low
     *      They also need to be defined from high to low in the "HeapRegion_t" list
     *      Luckily, the memory is already going from fastest (DTC_RAM) to slowest (BOARD_SDRAM) with OC_RAM in-between
     *      Because the heap still fills up sequentially, the fastest memory will be used first.
     *      This means that most of the OPC UA address space will likely end up in BOARD_SDRAM
     *
     *      NOTE: heap 5 allows for creating additional heap regions during runtime https://embeddedartistry.com/blog/2018/01/15/implementing-malloc-with-freertos/
     */

    //__attribute__ ((used,section(".noinit.$SRAM_DTC")))

    //fastest speed,    lowest address,  smallest heap region.
    const size_t DTC_heapsize = 123 * 1024;  //120 of 128  kB
    __NOINIT(SRAM_DTC)    static uint8_t DTC_ram_heap[DTC_heapsize];     //start address: 0x20000000, memory size: 128 kB

    //medium speed,     middle address,   medium sized heap region
    const size_t OC_heapsize = 512 * 1024;  //512 of 768 kB
    __NOINIT(SRAM_OC)     static uint8_t OC_ram_heap[OC_heapsize];    //start address: 0x20200000, memory size: 768 kB

    //slowest speed,    highest address,    largest heap region
    const size_t SEMC_heapsize = 16 * 1024 * 1024;   //16 of 32 MB
    __NOINIT(BOARD_SDRAM) static uint8_t SEMC_ram_heap[SEMC_heapsize];  //start address: 0x80000000, memory size: 32 MB

    static HeapRegion_t xHeapRegions[] =
    {
        { ( uint8_t * ) &DTC_ram_heap[0], DTC_heapsize },
        { ( uint8_t * ) &OC_ram_heap[0], OC_heapsize },
        { ( uint8_t * ) &SEMC_ram_heap[0], SEMC_heapsize },
        { NULL, 0 }// Terminates the array.
    };

    /* Pass the array into vPortDefineHeapRegions(). */
    vPortDefineHeapRegions( xHeapRegions );


#if configFRTOS_MEMORY_SCHEME != 5  //if FreeRTOS heap 5 is used
    printf("hey buddy, you should probably be using heap 5");
#endif
}

void BOARD_Init_ETH_peripheral(void)
{
    const clock_enet_pll_config_t config = {.enableClkOutput = true, .enableClkOutput25M = false, .loopDivider = 1};
    CLOCK_InitEnetPll(&config);

    //pins for ethernet. taken directly from example, not sure what they all exactly do
    IOMUXC_EnableMode(IOMUXC_GPR, kIOMUXC_GPR_ENET1TxClkOutputDir, 1); //true -> 1
    gpio_pin_config_t gpio_config = {kGPIO_DigitalOutput, 0, kGPIO_NoIntmode};
    GPIO_PinInit(GPIO1, 9, &gpio_config);
    GPIO_PinInit(GPIO1, 10, &gpio_config);
    /* pull up the ENET_INT before RESET. */
    GPIO_WritePinOutput(GPIO1, 10, 1);
    GPIO_WritePinOutput(GPIO1, 9, 0);
    delay();
    GPIO_WritePinOutput(GPIO1, 9, 1);
}



char *get_readable_runtime(){
    uint32_t runtime =  (xTaskGetTickCount() / TICK_PERIOD_MS) / 1000; //get the runtime of FreerRTOS  in ticks and convert to seconds

    static uint32_t last_time = 0;  //time since last call. for detecting overflows
    static uint32_t overflow_counter = 0;
    static char readable_time[15];

    //if the runtime has gotten lower than last time, there has been an overflow
    if(last_time > runtime){
        overflow_counter++;
    }

    uint64_t actual_time = (uint64_t(overflow_counter) * uint64_t(UINT32_MAX + 1)) + runtime;
    uint32_t days, hours, minutes, seconds;

    days = actual_time/86400;
    actual_time %= 86400;

    hours = actual_time/3600;
    actual_time %= 3600;

    minutes = actual_time/60;

    seconds = actual_time % 60;


    //insert the values into the radable string.  days:hours:minuts:seconds/0
    sprintf(readable_time, "%d:%02d:%02d:%02d\0", days, hours, minutes, seconds);

    return readable_time;
}

////wait until the next readout interval, accounting for the time it took to read out all the sensors
//inline void wait_remainder(uint32_t &next_time){
//    static const uint32_t readout_interval = pdMS_TO_TICKS(1000);        //the intervals between readouts (1000ms)
//    static const uint32_t command_interval = 50;   //check for new commands each this many ms
//
//    uint32_t wait_time = next_time - pdMS_TO_TICKS(xTaskGetTickCount()); //calculate how long the task has to wait
//    //  note, can overflow: ex with uint8_t    wait_time = 250 - 266 = -10  --->   wait_time = 250 - 10 = 240
//
//    if(wait_time <= readout_interval){    //account for overflows and > 1s readouts
//        //loop with delay and command check until the wait time is over
//        while(wait_time > command_interval){
//            vTaskDelay(command_interval);
//
//
//
//            wait_time -= command_interval;
//        }
//        vTaskDelay(wait_time);
//    }
//    else{
//        //NOTE if the counter overflows, will just report a bogus wait time and do another readout instantly instead of waiting a second
//        printf("I2C3 took longer than 1000ms, %d \r\n", pdMS_TO_TICKS(xTaskGetTickCount()) - (next_time - readout_interval));
//    }
//
//    next_time = pdMS_TO_TICKS(xTaskGetTickCount()) + readout_interval; //get the time just before the next buffer update started in ms
//}

//FreeRTOS funtions (enabled in FreeRTOSConfig.h)
extern "C" {
    //FreeRTOS function, gets called when memory allocation fails
    void vApplicationMallocFailedHook(){
        printf("Oh boy, time to die! (Malloc fail)\r\n");
        for(int i = 0; i < 1000000; i++){
            __asm("NOP"); /* delay */
        }

        WDOG_TriggerSystemSoftwareReset (WDOG_BASE);
    }
    //FreeRTOS function, gets called when a stack overflow happens in a task
    void vApplicationStackOverflowHook( TaskHandle_t xTask, char *pcTaskName ){
        printf("Oh boy, time to die! (stack overflow)\r\n");
        printf("This death was brought to you by: %s\r\n", pcTaskName); //print task name
        for(int i = 0; i < 1000000; i++){
            __asm("NOP"); /* delay */
        }

        WDOG_TriggerSystemSoftwareReset (WDOG_BASE);
    }

    void vApplicationIdleHook(void){
        for(;;);
    }
}



void blinky(){
    //TODO doesnt blink rapidly enough. Rapidly blinker LEDS are cooler
    static bool toggle = 0;
    GPIO_PinWrite(LED_GPIO, LED_PIN, toggle);
    toggle = !toggle;
}

#endif /* INIT_H_ */


