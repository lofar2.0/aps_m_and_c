/*
 * Copyright 2020 Stichting Nederlandse Wetenschappelijk Onderzoek Instituten,
 * ASTRON Netherlands Institute for Radio Astronomy
 * Licensed under the Apache License, Version 2.0 (the "License");
 *
 * you may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * See ../LICENSE.txt for more info.
 */

#include "common.h"

#include "fsl_device_registers.h"
#include "fsl_debug_console.h"
#include "peripherals.h"


#define RETRIES 4 // the number of consecutive times an I2C read/write action can be retried whenever one fails
#define AUTO_RESET 1 //if 1, automatically try to reset the unit if the transfer runs out of retries

//#define DELAY for(int i = 0; i < 10000; i++){__asm("NOP");}    //adds a delay for the I2C line to recover, should probably be replaced with vTaskDelay(n) at some point
#define DELAY vTaskDelay(pdMS_TO_TICKS(1))    //delays the task for 2 ticks

void I2C_reset(lpi2c_rtos_handle_t *I2C_Handle){
    //if an error is detected, reset the entire thing
    //reset function gets called in the init function

//    printf("something went wrong with one of the I2C units, attempting reset and re-init\r\n");

    if(     I2C_Handle == &LPI2C1_masterHandle){
        LPI2C_RTOS_Deinit(I2C_Handle);
        LPI2C1_init();
    }
    else if(I2C_Handle == &LPI2C2_masterHandle){
        LPI2C_RTOS_Deinit(I2C_Handle);
        LPI2C2_init();
    }
    else if(I2C_Handle == &LPI2C3_masterHandle){
        LPI2C_RTOS_Deinit(I2C_Handle);
        LPI2C3_init();
    }
    else if(I2C_Handle == &LPI2C4_masterHandle){
        LPI2C_RTOS_Deinit(I2C_Handle);
        LPI2C4_init();
    }
    else {
        printf("cant determine I2C handle");
    }

    DELAY;
}


status_t I2C_read_byte(lpi2c_rtos_handle_t *I2C_Handle, uint8_t address, uint8_t register_address, uint8_t *raw_byte){
    status_t status;
    uint8_t retry_cnt = RETRIES;

    lpi2c_master_transfer_t transfer_config = {
            .flags          = kLPI2C_TransferDefaultFlag,
            .device_address = address,
            .subaddress     = register_address,
            .subaddressSize = sizeof(uint8_t),
            .data           = raw_byte,
            .dataSize       = 1,
            .direction      = kLPI2C_Read
    };    //I2C unit transfer config

    status = LPI2C_RTOS_Transfer(I2C_Handle, &transfer_config);
    if(status == kStatus_Success){
        DELAY;
        return status;
    }
    else{
        //keep retrying until the max number of retries has been reached
        while(status != kStatus_Success && retry_cnt > 0){
            retry_cnt--;
            DELAY;
            status = LPI2C_RTOS_Transfer(I2C_Handle, &transfer_config);

        }
#if AUTO_RESET == 1
        if(status != kStatus_Success){
            I2C_reset(I2C_Handle);
        }
#endif

        return status;
    }
}
status_t I2C_read_word(lpi2c_rtos_handle_t *I2C_Handle, uint8_t address, uint8_t register_address, uint16_t *raw_word){
    status_t status;
    uint8_t retry_cnt = RETRIES;

    lpi2c_master_transfer_t transfer_config = {    //I2C unit transfer config
        .flags          = kLPI2C_TransferDefaultFlag,
        .device_address = address,
        .subaddress     = register_address,
        .subaddressSize = sizeof(uint8_t),
        .data           = raw_word,
        .dataSize       = 2,
        .direction      = kLPI2C_Read
    };

    status = LPI2C_RTOS_Transfer(I2C_Handle, &transfer_config);
    if(status == kStatus_Success){
        DELAY;
        return status;
    }
    else{
        //keep retrying until the max number of retries has been reached
        while(status != kStatus_Success && retry_cnt > 0){
            retry_cnt--;
            DELAY;
            status = LPI2C_RTOS_Transfer(I2C_Handle, &transfer_config);

        }
#if AUTO_RESET == 1
        if(status != kStatus_Success){
            I2C_reset(I2C_Handle);
        }
#endif

        return status;
    }
}
status_t I2C_read_block(lpi2c_rtos_handle_t *I2C_Handle, uint8_t address, uint8_t register_address,  void* raw_data, uint16_t data_size){
    status_t status;
    uint8_t retry_cnt = RETRIES;

    lpi2c_master_transfer_t transfer_config = {    //I2C unit transfer config
        .flags          = kLPI2C_TransferDefaultFlag,
        .device_address = address,
        .subaddress     = register_address,
        .subaddressSize = sizeof(uint8_t),
        .data           = raw_data,
        .dataSize       = data_size,
        .direction      = kLPI2C_Read
    };

    status = LPI2C_RTOS_Transfer(I2C_Handle, &transfer_config);
    if(status == kStatus_Success){
        DELAY;
        return status;
    }
    else{
        //keep retrying until the max number of retries has been reached
        while(status != kStatus_Success && retry_cnt > 0){
            retry_cnt--;
            DELAY;
            status = LPI2C_RTOS_Transfer(I2C_Handle, &transfer_config);

        }
#if AUTO_RESET == 1
        if(status != kStatus_Success){
            I2C_reset(I2C_Handle);
        }
#endif

        return status;
    }
}

status_t I2C_write_byte(lpi2c_rtos_handle_t *I2C_Handle, uint8_t address, uint8_t register_address, uint8_t *write_byte){
    status_t status;
    uint8_t retry_cnt = RETRIES;

    lpi2c_master_transfer_t transfer_config = {    //I2C unit transfer config
        .flags          = kLPI2C_TransferDefaultFlag,
        .device_address   = address,
        .subaddress     = register_address,
        .subaddressSize = sizeof(uint8_t),
        .data           = write_byte,
        .dataSize       = 1,
        .direction      = kLPI2C_Write
    };

    status = LPI2C_RTOS_Transfer(I2C_Handle, &transfer_config);
    if(status == kStatus_Success){
        DELAY;
        return status;
    }
    else{
        //keep retrying until the max number of retries has been reached
        while(status != kStatus_Success && retry_cnt > 0){
            retry_cnt--;
            DELAY;
            status = LPI2C_RTOS_Transfer(I2C_Handle, &transfer_config);

        }
#if AUTO_RESET == 1
        if(status != kStatus_Success){
            I2C_reset(I2C_Handle);
        }
#endif

        return status;
    }
}
status_t I2C_write_word(lpi2c_rtos_handle_t *I2C_Handle, uint8_t address, uint8_t register_address, uint16_t *write_word){
    status_t status;
    uint8_t retry_cnt = RETRIES;

    lpi2c_master_transfer_t transfer_config = {    //I2C unit transfer config
        .flags          = kLPI2C_TransferDefaultFlag,
        .device_address   = address,
        .subaddress     = register_address,
        .subaddressSize = sizeof(uint8_t),
        .data           = write_word,
        .dataSize       = 2,
        .direction      = kLPI2C_Write
    };

    status = LPI2C_RTOS_Transfer(I2C_Handle, &transfer_config);
    if(status == kStatus_Success){
        DELAY;
        return status;
    }
    else{
        //keep retrying until the max number of retries has been reached
        while(status != kStatus_Success && retry_cnt > 0){
            retry_cnt--;
            DELAY;
            status = LPI2C_RTOS_Transfer(I2C_Handle, &transfer_config);

        }
#if AUTO_RESET == 1
        if(status != kStatus_Success){
            I2C_reset(I2C_Handle);
        }
#endif

        return status;
    }
}
status_t I2C_write_block(lpi2c_rtos_handle_t *I2C_Handle, uint8_t address, uint8_t register_address, void* write_data, uint16_t data_size){
    status_t status;
    uint8_t retry_cnt = RETRIES;

    lpi2c_master_transfer_t transfer_config = {    //I2C unit transfer config
        .flags          = kLPI2C_TransferDefaultFlag,
        .device_address   = address,
        .subaddress     = register_address,
        .subaddressSize = sizeof(uint8_t),
        .data           = write_data,
        .dataSize       = data_size,
        .direction      = kLPI2C_Write
    };

    status = LPI2C_RTOS_Transfer(I2C_Handle, &transfer_config);
    if(status == kStatus_Success){
        DELAY;
        return status;
    }
    else{
        //keep retrying until the max number of retries has been reached
        while(status != kStatus_Success && retry_cnt > 0){
            retry_cnt--;
            DELAY;
            status = LPI2C_RTOS_Transfer(I2C_Handle, &transfer_config);

        }
#if AUTO_RESET == 1
        if(status != kStatus_Success){
            I2C_reset(I2C_Handle);
        }
#endif

        return status;
    }
}
//the I2C switch doesn't have any registers, instead it just configures itself according to the data it received
status_t I2C_write_reg(lpi2c_rtos_handle_t *I2C_Handle, uint8_t address, uint8_t *write_byte){
    status_t status;
    uint8_t retry_cnt = RETRIES;

    lpi2c_master_transfer_t transfer_config = {    //I2C unit transfer config
        .flags          = kLPI2C_TransferDefaultFlag,
        .device_address   = address,
        .subaddress     = 0,
        .subaddressSize = 0,
        .data           = write_byte,
        .dataSize       = 1,
        .direction      = kLPI2C_Write
    };

    status = LPI2C_RTOS_Transfer(I2C_Handle, &transfer_config);
    if(status == kStatus_Success){
        DELAY;
        return status;
    }
    else{
        //keep retrying until the max number of retries has been reached
        while(status != kStatus_Success && retry_cnt > 0){
            retry_cnt--;
            DELAY;
            //vTaskdelay(10/portTICK_PERIOD_MS);
            status = LPI2C_RTOS_Transfer(I2C_Handle, &transfer_config);
        }
#if AUTO_RESET == 1
        if(status != kStatus_Success){
            I2C_reset(I2C_Handle);
        }
#endif

        return status;
    }
}





