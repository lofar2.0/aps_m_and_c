/*
 * Copyright 2020 Stichting Nederlandse Wetenschappelijk Onderzoek Instituten,
 * ASTRON Netherlands Institute for Radio Astronomy
 * Licensed under the Apache License, Version 2.0 (the "License");
 *
 * you may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * See ../LICENSE.txt for more info.
 */



#define SPEEDTEST

#include "commands.h"
#include "string.h"

const uint8_t *test_pb(){
    //array of the protocol buffer encoded .dat files content converted to hex
    static const uint8_t test_data[801] = {
        0x08, 0x01, 0x12, 0x14, 0x08, 0x00, 0x12, 0x04, 0x41, 0x74, 0x74, 0x30, 0x1A, 0x06, 0x08, 0x75,
        0x10, 0x02, 0x18, 0x05, 0x28, 0x2A, 0x38, 0x01, 0x12, 0x14, 0x08, 0x01, 0x12, 0x04, 0x41, 0x74,
        0x74, 0x31, 0x1A, 0x06, 0x08, 0x75, 0x10, 0x03, 0x18, 0x05, 0x28, 0x2A, 0x38, 0x01, 0x12, 0x14,
        0x08, 0x02, 0x12, 0x04, 0x41, 0x74, 0x74, 0x32, 0x1A, 0x06, 0x08, 0x76, 0x10, 0x02, 0x18, 0x05,
        0x28, 0x30, 0x38, 0x01, 0x12, 0x17, 0x08, 0x03, 0x12, 0x07, 0x50, 0x77, 0x72, 0x5F, 0x64, 0x69,
        0x67, 0x1A, 0x08, 0x08, 0x76, 0x10, 0x02, 0x18, 0x01, 0x30, 0x06, 0x38, 0x01, 0x12, 0x18, 0x08,
        0x04, 0x12, 0x08, 0x50, 0x77, 0x72, 0x5F, 0x41, 0x6E, 0x74, 0x30, 0x1A, 0x08, 0x08, 0x75, 0x10,
        0x02, 0x18, 0x01, 0x30, 0x06, 0x38, 0x01, 0x12, 0x18, 0x08, 0x05, 0x12, 0x08, 0x50, 0x77, 0x72,
        0x5F, 0x41, 0x6E, 0x74, 0x31, 0x1A, 0x08, 0x08, 0x75, 0x10, 0x02, 0x18, 0x01, 0x30, 0x07, 0x38,
        0x01, 0x12, 0x18, 0x08, 0x06, 0x12, 0x08, 0x50, 0x77, 0x72, 0x5F, 0x41, 0x6E, 0x74, 0x32, 0x1A,
        0x08, 0x08, 0x75, 0x10, 0x03, 0x18, 0x01, 0x30, 0x06, 0x38, 0x01, 0x12, 0x13, 0x08, 0x07, 0x12,
        0x05, 0x42, 0x61, 0x6E, 0x64, 0x30, 0x1A, 0x06, 0x08, 0x76, 0x10, 0x03, 0x18, 0x02, 0x38, 0x01,
        0x12, 0x15, 0x08, 0x08, 0x12, 0x05, 0x42, 0x61, 0x6E, 0x64, 0x31, 0x1A, 0x08, 0x08, 0x76, 0x10,
        0x03, 0x18, 0x02, 0x30, 0x02, 0x38, 0x01, 0x12, 0x15, 0x08, 0x09, 0x12, 0x05, 0x42, 0x61, 0x6E,
        0x64, 0x32, 0x1A, 0x08, 0x08, 0x76, 0x10, 0x03, 0x18, 0x02, 0x30, 0x04, 0x38, 0x01, 0x12, 0x14,
        0x08, 0x0A, 0x12, 0x04, 0x4C, 0x45, 0x44, 0x30, 0x1A, 0x08, 0x08, 0x76, 0x10, 0x03, 0x18, 0x01,
        0x30, 0x06, 0x38, 0x01, 0x12, 0x14, 0x08, 0x0B, 0x12, 0x04, 0x4C, 0x45, 0x44, 0x31, 0x1A, 0x08,
        0x08, 0x76, 0x10, 0x03, 0x18, 0x01, 0x30, 0x07, 0x38, 0x01, 0x12, 0x19, 0x08, 0x0C, 0x12, 0x09,
        0x41, 0x44, 0x43, 0x5F, 0x6C, 0x6F, 0x63, 0x6B, 0x30, 0x1A, 0x08, 0x08, 0x00, 0x10, 0x0A, 0x18,
        0x08, 0x20, 0x01, 0x38, 0x00, 0x12, 0x19, 0x08, 0x0D, 0x12, 0x09, 0x41, 0x44, 0x43, 0x5F, 0x6C,
        0x6F, 0x63, 0x6B, 0x31, 0x1A, 0x08, 0x08, 0x01, 0x10, 0x0A, 0x18, 0x08, 0x20, 0x01, 0x38, 0x00,
        0x12, 0x19, 0x08, 0x0E, 0x12, 0x09, 0x41, 0x44, 0x43, 0x5F, 0x6C, 0x6F, 0x63, 0x6B, 0x32, 0x1A,
        0x08, 0x08, 0x02, 0x10, 0x0A, 0x18, 0x08, 0x20, 0x01, 0x38, 0x00, 0x12, 0x19, 0x08, 0x0F, 0x12,
        0x05, 0x56, 0x5F, 0x32, 0x76, 0x35, 0x1A, 0x0C, 0x08, 0x14, 0x10, 0xB8, 0x01, 0x18, 0x18, 0x3D,
        0x0A, 0xD7, 0x23, 0x3C, 0x38, 0x00, 0x12, 0x1A, 0x08, 0x10, 0x12, 0x06, 0x49, 0x5F, 0x41, 0x6E,
        0x74, 0x30, 0x1A, 0x0C, 0x08, 0x14, 0x10, 0xB2, 0x01, 0x18, 0x18, 0x3D, 0x00, 0x00, 0x40, 0x41,
        0x38, 0x00, 0x12, 0x1A, 0x08, 0x11, 0x12, 0x06, 0x49, 0x5F, 0x41, 0x6E, 0x74, 0x31, 0x1A, 0x0C,
        0x08, 0x14, 0x10, 0xBA, 0x01, 0x18, 0x18, 0x3D, 0x00, 0x00, 0x40, 0x41, 0x38, 0x00, 0x12, 0x1A,
        0x08, 0x12, 0x12, 0x06, 0x49, 0x5F, 0x41, 0x6E, 0x74, 0x32, 0x1A, 0x0C, 0x08, 0x14, 0x10, 0xB3,
        0x01, 0x18, 0x18, 0x3D, 0x00, 0x00, 0x40, 0x41, 0x38, 0x00, 0x1A, 0xA8, 0x01, 0x08, 0x00, 0x12,
        0x06, 0x08, 0x76, 0x10, 0x06, 0x28, 0x00, 0x12, 0x06, 0x08, 0x76, 0x10, 0x07, 0x28, 0x00, 0x12,
        0x06, 0x08, 0x76, 0x10, 0x02, 0x28, 0x40, 0x12, 0x07, 0x08, 0x20, 0x10, 0x02, 0x28, 0xFF, 0x01,
        0x12, 0x06, 0x08, 0x20, 0x10, 0x03, 0x28, 0x0F, 0x12, 0x06, 0x08, 0x20, 0x10, 0x06, 0x28, 0x00,
        0x12, 0x06, 0x08, 0x20, 0x10, 0x07, 0x28, 0x00, 0x12, 0x06, 0x08, 0x75, 0x10, 0x06, 0x28, 0x00,
        0x12, 0x06, 0x08, 0x75, 0x10, 0x07, 0x28, 0x00, 0x12, 0x08, 0x08, 0x00, 0x10, 0x5F, 0x20, 0x01,
        0x28, 0x14, 0x12, 0x08, 0x08, 0x00, 0x10, 0x15, 0x20, 0x01, 0x28, 0x07, 0x12, 0x09, 0x08, 0x00,
        0x10, 0xFF, 0x01, 0x20, 0x01, 0x28, 0x01, 0x12, 0x08, 0x08, 0x01, 0x10, 0x5F, 0x20, 0x01, 0x28,
        0x14, 0x12, 0x08, 0x08, 0x01, 0x10, 0x15, 0x20, 0x01, 0x28, 0x07, 0x12, 0x09, 0x08, 0x01, 0x10,
        0xFF, 0x01, 0x20, 0x01, 0x28, 0x01, 0x12, 0x08, 0x08, 0x02, 0x10, 0x5F, 0x20, 0x01, 0x28, 0x14,
        0x12, 0x08, 0x08, 0x02, 0x10, 0x15, 0x20, 0x01, 0x28, 0x07, 0x12, 0x09, 0x08, 0x02, 0x10, 0xFF,
        0x01, 0x20, 0x01, 0x28, 0x01, 0x22, 0x32, 0x12, 0x08, 0x08, 0x20, 0x10, 0x02, 0x18, 0x01, 0x30,
        0x01, 0x1A, 0x08, 0x08, 0x20, 0x10, 0x02, 0x18, 0x01, 0x30, 0x00, 0x22, 0x08, 0x08, 0x20, 0x10,
        0x00, 0x18, 0x01, 0x30, 0x00, 0x2A, 0x08, 0x08, 0x20, 0x10, 0x06, 0x18, 0x01, 0x30, 0x00, 0x32,
        0x08, 0x08, 0x20, 0x10, 0x03, 0x18, 0x01, 0x30, 0x00, 0x22, 0x32, 0x12, 0x08, 0x08, 0x20, 0x10,
        0x02, 0x18, 0x01, 0x30, 0x03, 0x1A, 0x08, 0x08, 0x20, 0x10, 0x02, 0x18, 0x01, 0x30, 0x02, 0x22,
        0x08, 0x08, 0x20, 0x10, 0x00, 0x18, 0x01, 0x30, 0x02, 0x2A, 0x08, 0x08, 0x20, 0x10, 0x06, 0x18,
        0x01, 0x30, 0x02, 0x32, 0x08, 0x08, 0x20, 0x10, 0x03, 0x18, 0x01, 0x30, 0x01, 0x22, 0x32, 0x12,
        0x08, 0x08, 0x20, 0x10, 0x02, 0x18, 0x01, 0x30, 0x05, 0x1A, 0x08, 0x08, 0x20, 0x10, 0x02, 0x18,
        0x01, 0x30, 0x04, 0x22, 0x08, 0x08, 0x20, 0x10, 0x00, 0x18, 0x01, 0x30, 0x04, 0x2A, 0x08, 0x08,
        0x20, 0x10, 0x06, 0x18, 0x01, 0x30, 0x04, 0x32, 0x08, 0x08, 0x20, 0x10, 0x03, 0x18, 0x01, 0x30,
        0x02
    };
    return test_data;
}


#define  RCU_CMD_TYPE   "RCU"
#define  HBA_CMD_TYPE   "HBA"
#define  UNB_CMD_TYPE   "UNB"
#define SYNC_CMD_TYPE   "SYN"
#define  OTH_CMD_TYPE   "OTH"

//this function takes a UA_DateTime and returns a human readable UA_String
UA_String UA_readable_time(UA_DateTime){
    char readable_time[] = "9999-12-31 23:59:59.999";   //char array for readable time
    UA_DateTimeStruct dts = UA_DateTime_toStruct(UA_DateTime_now());  //readable time
    sprintf(readable_time, "%04u-%02u-%02u %02u:%02u:%02u.%03u", dts.year, dts.month, dts.day, dts.hour, dts.min, dts.sec, dts.milliSec);
    UA_String time_string = UA_STRING_ALLOC(readable_time);
    return time_string;
}


uint32_t     cmd_handler::nodeID_cnt = 20000;
cmd_handler *cmd_handler::self = nullptr;
uint8_t      RCU::RCU_cnt = 0;


void cmd_handler::check_commands(){
    cmd received_cmd;
    int config_number = 0;
    static uint32_t commands = 0;

    if(uxQueueMessagesWaiting(cmd_queue)){              //if the queue isnt empty

        xQueuePeek(cmd_queue, &received_cmd, 1);

        char cmp[] = {"RCU"};
        if(strcmp(received_cmd.cmd_type, cmp)){
            return; //only receive commands that are RCU or HBA
        }

        //get the correct unit
        for(int config_number = 0; config_number < MAX_I2C_UNITS; config_number++){
            if(received_cmd.RCU_num > I2C_configs[config_number].RCU_num_offset){
                if(received_cmd.RCU_num <= I2C_configs[config_number].RCU_num_offset + I2C_configs[config_number].nof_RCUs){
                    break;
                }
            }
        }

        //make sure the bus is available
        I2C_configs[config_number].base_ptr->cmd_busy_flag = true;

        //delay while still unavailable
        while(!I2C_configs[config_number].base_ptr->sleep_flag && !I2C_configs[config_number].base_ptr->ack_cmd_flag){
                vTaskDelay(pdMS_TO_TICKS(1));
        }


        xQueueReceive(cmd_queue, &received_cmd, 0);  //if so, receive the command data
            process_RCU_cmd(&received_cmd);


        I2C_configs[config_number].base_ptr->cmd_busy_flag = false;
        I2C_configs[config_number].base_ptr->ack_cmd_flag = false;

    }
}

void cmd_handler::new_deadline_unix(callback_func_inargs){

    int64_t unix_time = *(int64_t*)data->value.data;     //copy RCU number
    UA_DateTime dateTime = UA_DateTime_fromUnixTime(unix_time);
    self->active_deadline = true;

    //UA_DateTime counts in 100ns, gets converted to seconds here
    //calculates the amount of seconds (and presumeably
     UA_DateTime time = dateTime - UA_DateTime_now();   //time left before deadline arrives in 100ns increments

    if(time % UA_DATETIME_MSEC > 500){  //round up
        time += UA_DATETIME_SEC; //add 1 second to round up
    }
    time = time / UA_DATETIME_SEC;  //convert 100ns to 1s

    self->next_deadline = time;

    //write deadline to OPC ua
    //--------------------------------------------------------------------------------------------------------
    static UA_Variant   deadline_var;
    UA_Variant_init(&deadline_var);
    UA_String time_string = UA_readable_time(time);
    UA_Variant_setScalar(&deadline_var, &time_string, &UA_TYPES[UA_TYPES_STRING]);
    UA_StatusCode status = UA_Server_writeValue(self->server, self->deadline_nodeID, deadline_var);
    if(status) printf("couldn't write deadline node\r\n");

}

void cmd_handler::cmd_handler_task(){
//    const uint8_t WDOG_task_number = WDOG_sys.WDOG_thread_add();    //add this task to the WDOG_sys_manager

    //if these things aren't initialised yet
    while(cmd_queue == NULL || nof_registered_nodes == 0){
        vTaskDelay(pdMS_TO_TICKS(0));
    }

    for(;;){
        sync_loop();
        check_commands();
        vTaskDelay(pdMS_TO_TICKS(0));

//        WDOG_sys.WDOG_task_refresh(WDOG_task_number);   //refresh this task

//        readout_loop();
    }
}

//cmd_handler::update_status(){
//    static uint32_t last_time;
//    uint32_t current_time;
//    const uint32_t interval = 1000;
//
//    if(last_time >)
//
//}

void cmd_handler::sync_loop(){
    const TickType_t xBlockTime = pdMS_TO_TICKS( 500 );
    uint32_t ulNotifiedValue;

    uint8_t switch_configs[total_switches]; //stores the current configuration of the switches to restore later
    uint8_t switch_counter = 0;

    if(active_deadline){
        if(next_deadline == 0){                                             //set broadcast in anticipation of the sync
            for(int i = 0; i < nof_registered_nodes; i ++){                        //go through the I2C units
                for(int j = 0; j < I2C_configs[i].nof_switches; j++){       //go through the switches on each i2C unit
                    switch_configs[switch_counter] = I2C_configs[i].I2C_switches->current_config;
                    switch_counter++;
                    I2C_configs[i].I2C_switches->set_broadcast(I2C_configs[i].base_ptr->I2C_Handle);    //set switch to broadcast
                }
            }
        }

        while(next_deadline == 0 && active_deadline){  //next interrupt is the sync pulse

            //NOTE: this means the task waits about 1 second without doing anything else. some more commnads could be send in that time
            ulNotifiedValue = ulTaskNotifyTake( pdFALSE, xBlockTime );  //blocks 500ms or until notified by IRQ
            if( ulNotifiedValue > 0 ){
                sync();

                active_deadline = false;

                //write deadline to OPC ua
                //--------------------------------------------------------------------------------------------------------
                UA_Variant  deadline_var;
                UA_StatusCode status;

                UA_Variant_init(&deadline_var);
                UA_String deadline = UA_STRING("---");
                UA_Variant_setScalar(&deadline_var, &deadline, &UA_TYPES[UA_TYPES_STRING]);
                status = UA_Server_writeValue(server, deadline_nodeID, deadline_var);
                if(status) printf("couldn't write deadline node\r\n");

                UA_Variant_init(&deadline_var);
                UA_String previous_deadline = UA_readable_time(next_deadline);
                UA_Variant_setScalar(&deadline_var, &previous_deadline, &UA_TYPES[UA_TYPES_STRING]);
                status = UA_Server_writeValue(server, previous_deadline_nodeID, deadline_var);
                if(status) printf("couldn't write previous_deadline node\r\n");

                switch_counter = 0;
                for(int i = 0; i < nof_registered_nodes; i ++){                        //go through the I2C units
                    for(int j = 0; j < I2C_configs[j].nof_switches; j++){       //go through the switches on each i2C unit
                        I2C_configs[i].I2C_switches->set_configuration(I2C_configs[i].base_ptr->I2C_Handle, switch_configs[switch_counter]);    //set switch to broadcast
                        switch_counter++;
                    }
                }
            }
        }
    }
}

//FreeRTOS doesnt like non-static member functions so this wrapper is used
void cmd_handler::task_wrapper(void* args){
    static_cast<cmd_handler*>(args)->cmd_handler_task();
}

void cmd_handler::sync(){
    //unknown what exactly the command is
    //likely just a simple command
}

void cmd_handler::add_base_node(base_node *base_ptr, I2C_switch *I2Cswitch, uint8_t nof_RCUs, TaskHandle_t *taskhandle){
    //copy values and set pointers about how the RCUs are configured
    I2C_configs[nof_registered_nodes].base_ptr = base_ptr;          //pointer to base node
    I2C_configs[nof_registered_nodes].I2C_switches = I2Cswitch;      //pointer to switch
    I2C_configs[nof_registered_nodes].nof_switches = 1;  //number of switches
    I2C_configs[nof_registered_nodes].nof_RCUs = nof_RCUs;          //number of RCU's for this base node
    I2C_configs[nof_registered_nodes].RCU_num_offset = total_RCUs;  //starting RCU number
    I2C_configs[nof_registered_nodes].taskhandle = taskhandle;

    total_RCUs += nof_RCUs;             //total nof RCUs
    total_switches += 1;     //total nof switches

    get_RCU_config(I2C_configs[nof_registered_nodes]);

    nof_registered_nodes++;
}

cmd_handler::cmd_handler(UA_Server *_server, uint16_t idx){
    server = _server;
    lofar_idx = idx;
    self = this;
    add_OPCua_stuff();
}

void  cmd_handler::get_RCU_config(I2C_config &config){
    /*      This function is called while adding a new I2C base node
     *      It assumes there is no monitoring operation going on yet in the node
     *      reads out all the RCUs and their configs for the given I2C units
     * */

    //pointer to reduce size of name and increase readability
    lpi2c_rtos_handle_t *I2C_handle = config.base_ptr->I2C_Handle;


    //allocate memory for all the RCU configs
    config.RCUs =  (RCU*)pvPortMalloc(sizeof(RCU) * config.nof_RCUs);

    //set all switches to off before beginning (dont check since this is initialisation and the config can be inaccurate)
    for(int i = 0; i < config.nof_switches; i++){
        uint8_t off = 0;
        if(config.I2C_switches->current_config != off){
            config.I2C_switches->set_off(I2C_handle);
        }
    }

    //go through all the RCUs
    for(int i = 0; i < config.nof_RCUs; i++){
        //go through all the channels in a sequential order.

        if(config.I2C_switches->current_config != i){
            config.I2C_switches->set_channel(I2C_handle, i);
        }

        config.RCUs[i] = RCU(&config, this, i);   //read RCU rom and get config
        printf("inited RCU %d\r\n", i);
    }

    //set the last switch to off
    for(int i = 0; i < config.nof_switches; i++){
        if(config.I2C_switches->current_config){
            config.I2C_switches->set_off(I2C_handle);
        }
    }
}

void cmd_handler::add_OPCua_stuff(){
    status_t status;
#ifndef IGNORE_NODE_STRUCTURE
    //cmd handler base object node
    //--------------------------------------------------------------------
    UA_ObjectAttributes oAttr = UA_ObjectAttributes_default;
    cmd_handler_nodeID = UA_NODEID_STRING(lofar_idx, "cmd_handler");
    status = UA_Server_addObjectNode(   server, UA_NODEID_NULL,
                                        UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER),
                                        UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                                        UA_QUALIFIEDNAME(lofar_idx, "cmd_handler"),
                                        UA_NODEID_NUMERIC(0, UA_NS0ID_BASEOBJECTTYPE),
                                        oAttr, NULL, &cmd_handler_nodeID);
    if(status) printf("couldn't add base cmd node\r\n");
    //cmd status object node
    //--------------------------------------------------------------------
    oAttr = UA_ObjectAttributes_default;
    status_nodeID = UA_NODEID_STRING(lofar_idx, "cmd_status_node");
    oAttr.description = UA_LOCALIZEDTEXT("en-US", "contains the general status of the cmd handler");

    status = UA_Server_addObjectNode(   server, UA_NODEID_NULL,
                                        cmd_handler_nodeID,
                                        UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                                        UA_QUALIFIEDNAME(lofar_idx, "cmd_status"),
                                        UA_NODEID_NUMERIC(0, UA_NS0ID_BASEOBJECTTYPE),
                                        oAttr, NULL, &status_nodeID);
    if(status) printf("couldn't add cmd status node\r\n");

    //cmd RCUs object node (underlying objects are the RCU's)
    //--------------------------------------------------------------------
    oAttr = UA_ObjectAttributes_default;
    RCU_nodes_nodeID = UA_NODEID_STRING(lofar_idx, "cmd_RCU_base");
    oAttr.description = UA_LOCALIZEDTEXT("en-US", "contains each RCU as an object");

    status = UA_Server_addObjectNode(   server, UA_NODEID_NULL,
                                        cmd_handler_nodeID,
                                        UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                                        UA_QUALIFIEDNAME(lofar_idx, "I2C_info_handler"),
                                        UA_NODEID_NUMERIC(0, UA_NS0ID_BASEOBJECTTYPE),
                                        oAttr, NULL, &RCU_nodes_nodeID);
    if(status) printf("couldn't add cmd RCU base node\r\n");

#endif

    //cmd deadline node
    //--------------------------------------------------------------------
    UA_VariableAttributes vAttr = UA_VariableAttributes_default;
    UA_String deadline = UA_STRING("--no deadline--");

    UA_Variant_setScalar(&vAttr.value, &deadline, &UA_TYPES[UA_TYPES_STRING]);
    deadline_nodeID = UA_NODEID_STRING(lofar_idx, "deadline_node");
    vAttr.description = UA_LOCALIZEDTEXT("en-US", "contains the next deadline if one is active");

    status = UA_Server_addVariableNode(   server, UA_NODEID_NULL,
                                        UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER),
                                        UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                                        UA_QUALIFIEDNAME(lofar_idx, "sync_deadline"),
                                        UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                                        vAttr, NULL, &deadline_nodeID);
    if(status) printf("couldn't add deadline node\r\n");


    // deadline function
    //----------------------------------------------------------------------------------
    UA_ValueCallback deadline_callback ;
    deadline_callback.onRead = nullptr;
    deadline_callback.onWrite = new_deadline_unix;
    UA_Server_setVariableNode_valueCallback(server, deadline_nodeID, deadline_callback);

    // read interval function
    //----------------------------------------------------------------------------------
    UA_ValueCallback interval_callback ;
    interval_callback.onRead = nullptr;
    interval_callback.onWrite = set_RCU_read_interval;
    UA_Server_setVariableNode_valueCallback(server, interval_nodeID, interval_callback);


    //method that flushes the command queue should a buildup occur
    //----------------------------------------------------------------------------------
    UA_ValueCallback flush_callback ;
    flush_callback.onRead = nullptr;
    flush_callback.onWrite = flush_cmd_queue;
    UA_Server_setVariableNode_valueCallback(server, flush_nodeID, flush_callback);
}

status_t cmd_handler::process_RCU_cmd(cmd *cmd){

    if(cmd->RCU_num > total_RCUs){
        return 1;
    }
    status_t status = 0;

    RCU *used_RCU = nullptr;
    I2C_config *I2C_cfg = nullptr;
    lpi2c_rtos_handle_t *I2C_handle = nullptr;

    if(cmd->RCU_num == 0){
        //TODO: unsure how do do this for broadcast, maybe check all RCUs version first and if they have the specific command
        used_RCU = &I2C_configs[0].RCUs[0];
    }
    else{

        //point to the correct objects
        for(int i = 0; i < nof_registered_nodes; i++){
            if(cmd->RCU_num <= I2C_configs[i].RCU_num_offset + I2C_configs[i].nof_RCUs){
                I2C_cfg = &I2C_configs[i];
                used_RCU = &I2C_cfg->RCUs[cmd->RCU_num-1 - I2C_cfg->RCU_num_offset];
                I2C_handle = I2C_cfg->base_ptr->I2C_Handle;

                break;
            }
        }

        //go through all the commands and check if the command ID matches (can't assume a direct match between the ID and the number)
        for(int j = 0; j < used_RCU->variable_table_entries; j++){
            if(cmd->id != used_RCU->variable_table[j].id){   //check if the command ID matches any of the table IDs
                continue;   //skip if not equal
            }
            printf("attempting to do the %s command\r\n", used_RCU->variable_table[j].name);

            uint64_t write_value = 0;
            status = used_RCU->compose_command(cmd, &used_RCU->variable_table[j], write_value);
            if(status){
                printf("command validity check failed (ignored)");
                //return status;
            }

            //store current switch configs
            uint8_t channel_config[I2C_cfg->nof_switches];
            for(int i = 0; i < I2C_cfg->nof_switches; i++){
                channel_config[i] = I2C_cfg->I2C_switches->current_config;
            }

            //set channels
            for(int i = 0; i < I2C_cfg->nof_switches; i++){
                if(cmd->RCU_num-1 - I2C_cfg->RCU_num_offset < I2C_switch::nof_channels * (i+1)){
                    I2C_cfg->I2C_switches->set_channel(I2C_handle, cmd->RCU_num - I2C_cfg->RCU_num_offset);
//                    I2C_cfg->I2C_switches->set_broadcast(I2C_handle);
                }
                else {
                    I2C_cfg->I2C_switches->set_off(I2C_handle);
                }
            }


            uint8_t byte_length = 1;

            if( used_RCU->variable_table[j].has_width ){

                byte_length = used_RCU->variable_table[j].width / 8;

                if(used_RCU->variable_table[j].width % 8 != 0){
                    byte_length++;
                }
            }

            //write command
            status = I2C_write_block(I2C_handle, used_RCU->variable_table[j].addr , used_RCU->variable_table[j].reg, (uint8_t*)write_value, byte_length);
            if(status){
                printf("writing command was unsuccesful\r\n");
            }
            else {
                printf(" \t>>>\t writing command was success!\r\n");
                used_RCU->variable_table[j].value = write_value;
                used_RCU->sync_vtable_registers(&used_RCU->variable_table[j], write_value);
            }

            //restore channels
            for(int i = 0; i < I2C_cfg->nof_switches; i++){
                I2C_cfg->I2C_switches->set_configuration(I2C_handle, channel_config[i]);
            }

            return status;
        }
    }

    //if broadcast
    if(cmd->RCU_num == 0){

//        for(int j = 0; j < nof_I2C_units; j ++){   //go through the I2C units
//            I2C_handle = I2C_configs[j].I2C_handle;
//
//            for(int k = 0; k < I2C_configs[j].nof_switches; k++){    //go through the switches o each i2C unit
//                //XXX I2C_configs[j].I2C_switches[k].set_broadcast(I2C_handle);
//            }
//                printf("broadcast not yet clear\r\n");
////                status = I2C_write_block(I2C_handle, variable_table[i].addr , test_variable_table[i].reg, cmd->cmd_data, cmd->data_size);
//        }
//        return status;
    }

    return 1; //command not found
}

//UA_StatusCode cmd_handler::RCU_cmd(method_func_inargs){
//
//    //uint16_t data3 = *(uint16_t*)input[2].data;
//    UA_ByteString datastring;// = (UA_ByteString*)input[2].data;
//    memcpy(&datastring, (UA_ByteString*)input[2].data, sizeof(UA_ByteString));
//
//    if(datastring.length > sizeof(uint64_t)){
//        printf("command longer than 32 bytes\r\n");
//        return 1;
//    }
//
//    cmd RCU_cmd = {
//            .cmd_type   = "RCU",                        //RCU command
//            .RCU_num    = *(uint8_t*)input[0].data,     //copy RCU number
//            .HBA_num    = -1,                           //HBA unused
//            .id         = *(uint32_t*)input[1].data,      //unqiue identifier of the command
//            .data       = 0
//    };
//
////    uint8_t *ptr &datastring.data;
//
//    for(int i = 0; i < datastring.length || i < sizeof(uint64_t); i++){
//        RCU_cmd.data += datastring.data[i] <<  8 *i;
//    }
//
//
////    memcpy(RCU_cmd.data, datastring.data, datastring.length);
//
//    //UA_String_copy((UA_String*)input[1].data, &RCU_cmd.name);    //copy command name pointer
//
//
//    if(self->cmd_queue == nullptr){
//        return UA_STATUSCODE_BADRESOURCEUNAVAILABLE;
//    }
//    xQueueSend(cmd_handler::self->cmd_queue, &RCU_cmd, 0);
//
//    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_SERVER, "RCU command method was called");
//    return UA_STATUSCODE_GOOD;
//}
void cmd_handler::set_RCU_read_interval(callback_func_inargs){
    printf("Doesn't exist yet\r\n");
}
void cmd_handler::flush_cmd_queue(callback_func_inargs){
    xQueueReset(self->cmd_queue);
}

RCU::RCU(I2C_config *_config, cmd_handler *_handler, uint8_t _RCUnum){
    config  = _config;
    handler = _handler;
    RCUnum = _RCUnum;

    char RCU_name_arr[] = "RCU_00";
    sprintf(RCU_name_arr, "RCU_%02d", RCUnum);
    RCU_name = RCU_name_arr;

#ifndef IGNORE_NODE_STRUCTURE
    RCU_nodeID = UA_NODEID_STRING(lofar_idx, RCU_name_arr);

    //--------------------------------------------------------------------
    UA_ObjectAttributes oAttr = UA_ObjectAttributes_default;
    RCU_nodeID = UA_NODEID_STRING(lofar_idx, RCU_name_arr);
    status_t status = UA_Server_addObjectNode(   handler->server, UA_NODEID_NULL,
                                        handler->RCU_nodes_nodeID,
                                        UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                                        UA_QUALIFIEDNAME(lofar_idx, RCU_name_arr),
                                        UA_NODEID_NUMERIC(0, UA_NS0ID_BASEOBJECTTYPE),
                                        oAttr, NULL, &RCU_nodeID);
    if(status) printf("couldn't add %s node\r\n", RCU_name_arr);
#endif

    //variable containing data size
    uint16_t data_size = 0;

    //read the ROM data, first the length, then the data block using the obtained length
    //XXX I2C_read_word( config->I2C_handle, ROM_address , 0x00, &data_size);  //read data size from ROM
    data_size = 801; //XXX

//    uint8_t *data_block = (uint8_t*)pvPortMalloc(sizeof(uint8_t) * data_size);  //create buffer
//    memset(data_block, 0, data_size);   // and set to zero
    //XXX I2C_read_block(config->I2C_handle, ROM_address , 0x01, &data_block[0], data_size);  //read to whole data block from ROM


    //https://stackoverflow.com/questions/45979984/creating-callbacks-and-structs-for-repeated-field-in-a-protobuf-message-in-nanop
    //set all the callback functions for nano-pb
    RCU_info RCU_info = RCU_info_init_zero;
    RCU_info.RCU_var.arg = this;
    RCU_info.RCU_var.funcs.decode   = variable_decode_pb;    //add function to get any number of these items
    RCU_info.RCU_SPI.arg = this;
    RCU_info.RCU_SPI.funcs.decode   = spi_decode_pb;        //add function to get any number of these items
    RCU_info.RCU_input.arg = this;
    RCU_info.RCU_input.funcs.decode = input_decode_pb;      //add function to get any number of these items
    RCU_info.RCU_mode.arg = this;
    RCU_info.RCU_mode.funcs.decode  = mode_decode_pb;       //add function to get any number of these items


    // Create a stream that reads from the buffer.
    //pb_istream_t stream = pb_istream_from_buffer(data_block, data_size);  //real deal
    pb_istream_t stream = pb_istream_from_buffer(test_pb(), data_size);    //test

    //decode the message. callbacks will be called for repeated items
    pb_decode(&stream, RCU_info_fields, &RCU_info);


    printf("RCU has version: %d\r\n", RCU_info.version);

    //Because items are put in an array and incremented, once done they need to be decremented by 1
    Mode_table_entries--;
    variable_table_entries--;
    ant_table_entries--;
    SPI_bridge_table_entries--;

    //now the setup table has been read, excecute that table

    set_mode(true, Mode_Modes_Init);
    vTaskDelay(pdMS_TO_TICKS(10));
    for(int i =0; i< 100000; i++){
        __asm("NOP"); /* delay */
    }
    vTaskDelay(pdMS_TO_TICKS(1000));
}

void RCU::sync_vtable_registers(Vtable_entry *entry, uint64_t value){
    //mask to width
    uint64_t width = 8;
    if(entry->has_width){
        width = entry->width;
    }
    //set all bits to 1
    uint64_t mask = -1;                     //1111 1111
    //leave <width> number of bits 1
    mask = ~(mask << width);                //0000 0111
    if(entry->has_bitoffset){
        //shift the bits by the bitoffset
        mask = mask << entry->bitoffset;    //0011 1000
        value = value << entry->bitoffset;  //00xx x000
    }
    // leave only the newly written bits in the right place
    value = value & mask;                   //00xx x000 & 0011 1000
    //blank out the soon to be written value
    value |= entry->value & ~mask;          //xxxx xxxx & 1100 0111 -> xx00 0xxx


    for(int i = 0; i < variable_table_entries; i++){
        if(variable_table[i].addr == entry->addr && variable_table[i].reg == entry->reg){
            variable_table[i].value = value;
        }
    }

    return;
}


status_t RCU::set_mode(bool set_switches, _Mode_Modes Mode){
    status_t status;
    uint8_t switch_config;


    if(set_switches){   //if set, sets the switches to this RCU
        //TODO: set_switches
        for(int i =0; i < config->nof_switches; i++){
            switch_config = config->I2C_switches->current_config;
            config->I2C_switches->set_channel(config->base_ptr->I2C_Handle, RCUnum + 1);
//            config->I2C_switches->set_broadcast(config->base_ptr->I2C_Handle);
        }
    }



    for(int i = 0; i < Mode_table_entries; i++){
        //if its not the used mode, jsut skip it.
//        if(mode_table_entry_mode[i].mode != Mode){
//            continue;
//        }
        if(Mode_table[i].d_type){

        }

        uint16_t width = 1;
        if(Mode_table[i].has_width){
            width = Mode_table[i].width/8;
            if(Mode_table[i].width%8 != 0){
                width++;
            }
        }

        status = I2C_write_block(config->base_ptr->I2C_Handle, uint8_t(Mode_table[i].addr) , uint8_t(Mode_table[i].reg), (uint8_t*)&Mode_table[i].value, width);

        if(status){
            printf("fail\r\n");    //if writing failed, skip this function
        }
        else{
            printf("\t>>>\tcould write setup table entry %d\r\n",i);
        }
    }
    printf("finished setting mode\r\n");
    return status;
}


void cmd_handler::set_deadline(callback_func_inargs) {

    int64_t unix_time = *(int64_t*)data->value.data;
    UA_DateTime dateTime = UA_DateTime_fromUnixTime(unix_time);
    self->active_deadline = true;

    //UA_DateTime counts in 100ns, gets converted to seconds here
    //calculates the amount of seconds (and presumeably
     UA_DateTime time = dateTime - UA_DateTime_now();   //time left before deadline arrives in 100ns increments

    if(time % UA_DATETIME_MSEC > 500){  //round up
        time += UA_DATETIME_SEC; //add 1 second to round up
    }
    time = time / UA_DATETIME_SEC;  //convert 100ns to 1s

    self->next_deadline = time;

    //write deadline to OPC ua
    //--------------------------------------------------------------------------------------------------------
    static UA_Variant   deadline_var;
    UA_Variant_init(&deadline_var);
    UA_String time_string = UA_readable_time(time);
    UA_Variant_setScalar(&deadline_var, &time_string, &UA_TYPES[UA_TYPES_STRING]);
    UA_StatusCode status = UA_Server_writeValue(self->server, self->deadline_nodeID, deadline_var);
    if(status) printf("couldn't write deadline node\r\n");

}

bool cmd_handler::virtual_switch(I2C_config *config, int8_t channel, bool includes_offset, bool force_set) {
    /*  abstracts the multiple switches away by wrapping them all in a single function
     *
     *  @param: config, the I2C config used
     *  @param: channel the channel to set (if < 0 sets all switches off)
     *  @param: includes_offset, optional, defaults to true. subtracts the RCU_num_offset from the channel
     *
     * @return: true if success, false if failure
     * */
    status_t status;
    uint8_t max_channel =  config->nof_RCUs;
    if(includes_offset && channel >= 0){
        channel -= config->RCU_num_offset;
    }

    if(channel < 0){
        // < 0 means set to Off
        if(config->nof_switches == 1){
            status = config->I2C_switches->set_off(config->base_ptr->I2C_Handle);
            if(status != kStatus_Success){
                return false;
            }
        }
        else
        {
            for(int i = 0; i < config->nof_switches; i++){
                status = config->I2C_switches[i].set_off(config->base_ptr->I2C_Handle);
                if(status != kStatus_Success){
                    return false;
                }
            }
        }
    }
    else if( channel <= max_channel){
        //set 1 specific channel

        // if not this switch, set to off if its not already in that setting
        if(config->nof_switches == 1){
            //set specific channel if not there already
            if(force_set || config->I2C_switches->current_config != (1 << channel)){
                status = config->I2C_switches->set_channel(config->base_ptr->I2C_Handle, channel);
                if(status != kStatus_Success){
                    return false;
                }
            }
        }
        else {
            for(int i = 0; i < config->nof_switches; i++){
                if(channel >= I2C_switch::nof_channels ){
                    if(force_set || config->I2C_switches[i].current_config != 0x00){
                        status = config->I2C_switches[i].set_off(config->base_ptr->I2C_Handle);
                        if(status != kStatus_Success){
                            return false;
                        }
                    }
                channel -= 8;
                }
                else {
                    //set specific channel if not there already
                    if(force_set || config->I2C_switches[i].current_config != (1 << channel)){
                        status = config->I2C_switches[i].set_channel(config->base_ptr->I2C_Handle, channel);
                        if(status != kStatus_Success){
                            return false;
                        }
                    }
                }
            }
        }

    }
    else {
        printf("virtual switch - Invalid channel\r\n");
        return false;
    }

    return true;
}

void cmd_handler::read_point(callback_func_inargs) {


    //get pointer to the RCU, which was set when the entry was created
    RCU *rcu = (RCU*)(nodeContext);

    for(uint32_t k = 0; k < rcu->variable_table_entries; k++){

        if(rcu->variable_table[k].monitor_point_nodeID.identifier.numeric == nodeid->identifier.numeric){

            if(self->virtual_switch(rcu->config, rcu->RCUnum + 1, true, true)){
                //short name for readability
                Vtable_entry *entry = &rcu->variable_table[k];

                //check read/write //NOTE
    //                if(!entry->has_RW || (entry->has_RW  && entry->RW != variable_ReadWriteType_WriteOnly)){
                status_t status;
                uint64_t readout = 0;
                uint8_t byte_size = 1;

                if(entry->has_width){
                    byte_size = entry->width / 8;
                    if(entry->width % 8 != 0){
                        byte_size++;
                    }
                }

//                LPI2C_MasterClearStatusFlags(rcu->config->base_ptr->I2C_Handle->base, 0);
//                rcu->config->I2C_switches->set_broadcast(rcu->config->base_ptr->I2C_Handle);
                status = I2C_read_block(rcu->config->base_ptr->I2C_Handle, entry->addr, entry->reg, (uint8_t*)&readout, byte_size);
                if(status != kStatus_Success){
                    printf("I2C read in callback failed\r\n");
                    return;
                }
                else {
                    printf("I2C read success. %s read: %ld\r\n", entry->name, readout);

                    if(entry->has_bitoffset){
                        readout = readout >> entry->bitoffset;
                    }

                    //mask to width
                    if(entry->has_width){
                        //mask the values to the width
                        uint64_t mask = -1;                //                           1111 1111   set all bits to 1
                        mask = ~(mask << entry->width);    //              1111 1000 -> 0000 0111   shift the 1's entry->width bits and invert all the bits
                        readout = readout & mask;      // 1001 0110 &  0000 0111 -> 0000 0110   bitwise and to mask all the bits
                    }

                    UA_Variant value_var;
                    UA_Variant_init(&value_var);

                    if(entry->has_scale){
                        double setvalue = entry->has_scale * float(readout);

                        UA_Variant_setScalar(&value_var, &setvalue, &UA_TYPES[UA_TYPES_DOUBLE]);
                    }
                    else if(entry->has_width && entry->width == 1){
                        bool setvalue = false;
                        if(readout){
                            setvalue = true;
                        }

                        UA_Variant_setScalar(&value_var, &setvalue, &UA_TYPES[UA_TYPES_BOOLEAN]);
                    }
                    else{
                        UA_Variant_setScalar(&value_var, &readout, &UA_TYPES[UA_TYPES_INT64]);
                    }
                    status = UA_Server_writeValue(server, entry->monitor_point_nodeID, value_var);
                    if(status) {
                        printf("couldn't write node\r\n");
                    }
                }
            }
        }
    }
//    for(uint32_t k = 0; k < rcu->SPI_bridge_table_entries; k++){
//        printf("is SPI device\r\n");
//
//        if(rcu->SPI_bridge_table[k].monitor_point_nodeID.identifier.numeric == nodeid->identifier.numeric){
//
//            if(self->virtual_switch(rcu->config, rcu->RCUnum + 1, true, true)){
//                //short name for readability
//                SPI *entry = &rcu->SPI_bridge_table[k];
//
//                //check read/write //NOTE
//    //                if(!entry->has_RW || (entry->has_RW  && entry->RW != variable_ReadWriteType_WriteOnly)){
//                status_t status;
//                uint64_t readout = 0;
//                uint8_t byte_size = 1;
//
//
//
//                UA_Variant value_var;
//                UA_Variant_init(&value_var);
//
//                if(entry->has_scale){
//                    double setvalue = entry->has_scale * float(readout);
//
//                    UA_Variant_setScalar(&value_var, &setvalue, &UA_TYPES[UA_TYPES_DOUBLE]);
//                }
//                else if(entry->has_width && entry->width == 1){
//                    bool setvalue = false;
//                    if(readout){
//                        setvalue = true;
//                    }
//
//                    UA_Variant_setScalar(&value_var, &setvalue, &UA_TYPES[UA_TYPES_BOOLEAN]);
//                }
//                else{
//                    UA_Variant_setScalar(&value_var, &readout, &UA_TYPES[UA_TYPES_INT64]);
//                }
//                status = UA_Server_writeValue(server, entry->monitor_point_nodeID, value_var);
//                if(status) {
//                    printf("couldn't write node\r\n");
//                }
//            }
//        }
//    }
}


void cmd_handler::afterWrite(callback_func_inargs) {

    /*  during initialisation the RCU's address is added to the user nodeContext.
     * This allows bypassing comparing the nodeID to all the nodes, saving a lot of processing time
     *
     *
     * */

    //get pointer to the RCU, which was set when the entry was created
    RCU *rcu = (RCU*)(nodeContext);

    for(uint32_t k = 0; k < rcu->variable_table_entries; k++){
        // check if the nodeID in the variable table matches
        // NOTE: can be optimised further by creating a struct containing both the RCU and the table entry
        if(rcu->variable_table[k].control_point_nodeID.identifier.numeric == nodeid->identifier.numeric){

            uint64_t *ptr = (uint64_t*)(data->value.data);
//            uint64_t test1 = *(uint64_t*)(data->value.data);
//            uint64_t test2 = *(uint64_t*)(data->value.data);

            cmd RCU_cmd = {
                    .cmd_type   = "RCU",                        //RCU command
                    .RCU_num    = rcu->config->RCU_num_offset + rcu->RCUnum + 1, //copy RCU number
                    .HBA_num    = -1,                           //HBA TODO
                    .id         = k,      //unqiue identifier of the command
                    .data       = uint64_t(*ptr)
            };


//            RCU_cmd.data = uint64_t(data->value.data);
//            uint64_t test3 = *(uint64_t*)&data->value.data;


            if(self->cmd_queue == nullptr){
                return;
            }
            xQueueSend(cmd_handler::self->cmd_queue, &RCU_cmd, 0);
        }
    }
}


status_t cmd_handler::readout_loop(){
    //NOTE: does NOT go to the HBA's due to interference
    static uint32_t last_time = 0;
    const uint32_t interval = 1000;
    uint32_t right_now = pdMS_TO_TICKS(xTaskGetTickCount());

    if(right_now < last_time){  //overflow
        last_time = right_now - interval;
    }
    //only read out the devices once per interval loop
    if(!(last_time + interval < right_now)){
        return 0;
    }
    last_time = pdMS_TO_TICKS(xTaskGetTickCount());

    status_t status = 0;

    // All registered nodes
    for(uint32_t i = 0; i < nof_registered_nodes; i++){

        //create storage for current switch status
        uint8_t current_config[I2C_configs[i].nof_switches];
        for(uint32_t sw = 0; sw < I2C_configs[sw].nof_switches; sw++){
            current_config[sw] =  I2C_configs[i].I2C_switches[sw].current_config;
        }


        //All RCU on the node
        for(uint32_t j = 0; j < I2C_configs[i].nof_RCUs; j++){
            //all variable table entries on the node
            for(uint32_t k = 0; k < I2C_configs[i].RCUs[j].variable_table_entries; k++){

                //short name for readability
                Vtable_entry *entry = &I2C_configs[i].RCUs[j].variable_table[k];

                //check read/write //NOTE
//                if(!entry->has_RW || (entry->has_RW  && entry->RW != variable_ReadWriteType_WriteOnly)){

                    uint64_t readout = 0;
                    uint8_t byte_size = 1;
                    uint8_t channel = I2C_configs[i].RCUs[j].RCUnum;

                    //for all the switches
                    for(uint32_t l = 0; l < I2C_configs[i].nof_switches; l++){

                        //if the current config isnt already the switch
                        if(I2C_configs[i].I2C_switches[l].current_config != (1 << channel)){
                            if(channel < I2C_switch::nof_channels){
                                I2C_configs[i].I2C_switches[l].set_channel(I2C_configs[i].base_ptr->I2C_Handle, channel);
                            }
                            else {
                                I2C_configs[i].I2C_switches[l].set_off(I2C_configs[i].base_ptr->I2C_Handle);
                                channel += -8;
                            }
                        }
                    }

                    if(entry->has_width){
                        byte_size = entry->width / 8;
                        if(entry->width % 8 != 0){
                            byte_size++;
                        }
                    }

                    status = I2C_read_block(I2C_configs[i].base_ptr->I2C_Handle, entry->addr, entry->reg, &readout, byte_size);
                    if(status != kStatus_Success){
                        return status;
                    }

                    if(entry->has_bitoffset){
                        readout = readout >> entry->bitoffset;
                    }

                    //mask to width
                    if(entry->has_width){
                        //mask the values to the width
                        uint64_t mask = -1;                //                           1111 1111   set all bits to 1
                        mask = ~(mask << entry->width);    //              1111 1000 -> 0000 0111   shift the 1's entry->width bits and invert all the bits
                        readout = readout & mask;      // 1001 0110 &  0000 0111 -> 0000 0110   bitwise and to mask all the bits
                    }

                    UA_Variant value_var;
                    UA_Variant_init(&value_var);
                    UA_Variant_setScalar(&value_var, (double*)readout, &UA_TYPES[UA_TYPES_DOUBLE]);
                    status = UA_Server_writeValue(server, entry->monitor_point_nodeID, value_var);
                    if(status) {
                        printf("couldn't write temperature node\r\n");
                    }
                //} //NOTE
            }
            for(uint32_t k = 0; k < I2C_configs[i].RCUs[j].SPI_bridge_table_entries; k++){


            }
        }
        //restore the switches to how they were
        for(uint32_t sw = 0; sw < I2C_configs[sw].nof_switches; sw++){
            I2C_configs[sw].I2C_switches[sw].set_configuration(I2C_configs[sw].base_ptr->I2C_Handle, current_config[sw]);
        }

    }
    return 0;
}

void devReg_to_table_entry(Vtable_entry &entry, device_register &device){
    entry.id =             0;
    entry.reg =            device.reg;
    entry.addr =           device.addr;
    entry.has_minimum =    false;
//    entry.minimum =        device.minimum;
    entry.has_maximum =    false;
//    entry.maximum =        device.maximum;
    entry.has_RW =         false;
//    entry.RW =             device.RW;
    entry.has_value =      device.has_value;
    entry.value =          device.value;
    entry.has_width =      device.has_width;
    entry.width =          device.width;
    entry.has_scale =      device.has_scale;
    entry.scale =          device.scale;
    entry.has_bitoffset =  device.has_bitoffset;
    entry.bitoffset =      device.bitoffset;
    entry.has_d_type =     device.has_d_type;
    entry.d_type =         device.d_type;
}


bool RCU::variable_decode_pb(pb_istream_t *istream, const pb_field_t *field, void **arg){
    RCU *rcu = (RCU*)(*arg);

    variable variable_entry;
    Vtable_entry *entry = &rcu->variable_table[rcu->variable_table_entries];

    variable_entry = variable_init_default;
    variable_entry.name.arg = entry;
    variable_entry.name.funcs.decode = name_decode_pb;


    //decode the table
    if (!pb_decode(istream, variable_fields, &variable_entry)){
        printf("decoding failed\r\n");
        return false;
    }


    //data gets stored in a more convinient table, but a slightly different format
    entry->id =             variable_entry.id;
    entry->reg =            variable_entry.device.reg;
    entry->addr =           variable_entry.device.addr;
    entry->has_minimum =    variable_entry.has_minimum;
    entry->minimum =        variable_entry.minimum;
    entry->has_maximum =    variable_entry.has_maximum;
    entry->maximum =        variable_entry.maximum;
    entry->has_RW =         variable_entry.has_RW;
    entry->RW =             variable_entry.RW;
    entry->has_value =      variable_entry.device.has_value;
    entry->value =          variable_entry.device.value;
    entry->has_width =      variable_entry.device.has_width;
    entry->width =          variable_entry.device.width;
    entry->has_scale =      variable_entry.device.has_scale;
    entry->scale =          variable_entry.device.scale;
    entry->has_bitoffset =  variable_entry.device.has_bitoffset;
    entry->bitoffset =      variable_entry.device.bitoffset;
    entry->has_d_type =     variable_entry.device.has_d_type;
    entry->d_type =         variable_entry.device.d_type;
    //name gets copied in the name decode function


    string base_name = rcu->RCU_name + "_" + entry->name;


    //==monitor and control points==================================================================
    // add monitor point
    UA_VariableAttributes vAttr = UA_VariableAttributes_default;
    double initial_value_double = NAN;
    uint64_t initial_value_uint64_t = -1;
    bool initial_value_bool = false;
    status_t status;

    //NOTE: currently the proto buff only contains R and W, no RW. because of this W only monitor points are still added
    //if(!entry->has_RW || (entry->has_RW && entry->RW != variable_ReadWriteType_WriteOnly){
        char monitor_name[max_name_size];
        sprintf(monitor_name, "%s_R", base_name.c_str());

        entry->monitor_point_nodeID = UA_NODEID_NUMERIC(rcu->handler->lofar_idx, ++rcu->handler->nodeID_cnt);
        UA_QualifiedName qname = UA_QUALIFIEDNAME(rcu->handler->lofar_idx, monitor_name);
        UA_NodeId parentNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER);
        UA_NodeId parentReferenceNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES);


        vAttr = UA_VariableAttributes_default;
        vAttr.valueRank = UA_VALUERANK_SCALAR;

        //determine data type. (scale = double, 1 bit = bool, else = uin64_t)
        if(entry->has_scale){
            vAttr.dataType = UA_TYPES[UA_TYPES_DOUBLE].typeId;
            UA_Variant_setScalar(&vAttr.value, &initial_value_double, &UA_TYPES[UA_TYPES_DOUBLE]);
        }
        else if(entry->has_width && entry->width == 1){
            vAttr.dataType = UA_TYPES[UA_TYPES_BOOLEAN].typeId;
            UA_Variant_setScalar(&vAttr.value, &initial_value_bool, &UA_TYPES[UA_TYPES_BOOLEAN]);
        }
        else{
            vAttr.dataType = UA_TYPES[UA_TYPES_INT64].typeId;
            UA_Variant_setScalar(&vAttr.value, &initial_value_uint64_t, &UA_TYPES[UA_TYPES_INT64]);
        }
        status = UA_Server_addVariableNode(rcu->handler->server, entry->monitor_point_nodeID,   //UA_NODEID_NULL
                            parentNodeId,
                            parentReferenceNodeId,
                            qname,
                            UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                            vAttr, rcu, NULL);
        if(status){
            printf("couldn't add node\r\n");
        }
    //}

        UA_ValueCallback read_callback;
        read_callback.onRead = cmd_handler::read_point;
        read_callback.onWrite = nullptr;
        status = UA_Server_setVariableNode_valueCallback(rcu->handler->server, entry->monitor_point_nodeID, read_callback);
        if(status) {
            printf("couldn't add callback\r\n");
        }
    //--------------------------------------------------------------------------------
    // add control point

    if(!entry->has_RW || (entry->has_RW && entry->RW != variable_ReadWriteType_ReadOnly)){
        char control_name[max_name_size];
        sprintf(control_name, "%s_RW", base_name.c_str());


        entry->control_point_nodeID = UA_NODEID_NUMERIC(rcu->handler->lofar_idx, ++rcu->handler->nodeID_cnt);
        UA_QualifiedName ctrl_qname = UA_QUALIFIEDNAME(rcu->handler->lofar_idx, control_name);
        UA_NodeId ctrl_parentNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER);
        UA_NodeId ctrl_parentReferenceNodeId = UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES);

        vAttr = UA_VariableAttributes_default;
        //determine data type. (scale = double, 1 bit = bool, else = uin64_t)
        if(entry->has_scale){
            UA_Variant_setScalar(&vAttr.value, &initial_value_double, &UA_TYPES[UA_TYPES_DOUBLE]);
        }
        else if(entry->has_width && entry->width == 1){
            UA_Variant_setScalar(&vAttr.value, &initial_value_bool, &UA_TYPES[UA_TYPES_BOOLEAN]);
        }
        else{
            UA_Variant_setScalar(&vAttr.value, &initial_value_uint64_t, &UA_TYPES[UA_TYPES_INT64]);
        }
        vAttr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;
        status = UA_Server_addVariableNode(rcu->handler->server, entry->control_point_nodeID,   //UA_NODEID_NULL
                            ctrl_parentNodeId,
                            ctrl_parentReferenceNodeId,
                            ctrl_qname,
                            UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                            vAttr, rcu, NULL);
        if(status) printf("couldn't add node\r\n");


        UA_ValueCallback write_callback ;
        write_callback.onRead = nullptr;
        write_callback.onWrite = cmd_handler::afterWrite;
        UA_Server_setVariableNode_valueCallback(rcu->handler->server, entry->control_point_nodeID, write_callback);
    }


    rcu->variable_table_entries++;
    return true;
}
bool RCU::mode_decode_pb(pb_istream_t *istream, const pb_field_t *field, void **arg){
    RCU *rcu = (RCU*)(*arg);

    rcu->mode_table_entry_mode[rcu->Mode_table_entries] = Mode_init_default;
    rcu->mode_table_entry_mode[rcu->Mode_table_entries].V.arg = rcu;
    rcu->mode_table_entry_mode[rcu->Mode_table_entries].V.funcs.decode = device_decode_pb;

    if (!pb_decode(istream, Mode_fields, &rcu->mode_table_entry_mode[rcu->Mode_table_entries])){
        printf("decoding failed\r\n");
        return false;
    }

    return true;
}
bool RCU::spi_decode_pb(pb_istream_t *istream, const pb_field_t *field, void **arg){
    RCU *rcu = (RCU*)(*arg);
    _SPI_bridge SPI_device;

    if (!pb_decode(istream, SPI_bridge_fields, &SPI_device)){
        printf("decoding failed\r\n");
        return false;
    }

    devReg_to_table_entry(rcu->SPI_bridge_table[rcu->SPI_bridge_table_entries].DIR, SPI_device.DIR );
    devReg_to_table_entry(rcu->SPI_bridge_table[rcu->SPI_bridge_table_entries].CLK, SPI_device.CLK );
    devReg_to_table_entry(rcu->SPI_bridge_table[rcu->SPI_bridge_table_entries].SDO, SPI_device.SDO );
    devReg_to_table_entry(rcu->SPI_bridge_table[rcu->SPI_bridge_table_entries].SDI, SPI_device.SDI );
    if(SPI_device.has_CS){
        devReg_to_table_entry(rcu->SPI_bridge_table[rcu->SPI_bridge_table_entries].CS, SPI_device.CS );
    }

    rcu->SPI_bridge_table_entries++;
    return true;
}
bool RCU::input_decode_pb(pb_istream_t *istream, const pb_field_t *field, void **arg){
    RCU *rcu = (RCU*)(*arg);

    if (!pb_decode(istream, variable_fields, &rcu->ant_table[rcu->ant_table_entries])){
        printf("decoding failed\r\n");
        return false;
    }

    rcu->ant_table_entries++;
    return true;
}
bool RCU::device_decode_pb(pb_istream_t *istream, const pb_field_t *field, void **arg){
    RCU *rcu = (RCU*)(*arg);

    if (!pb_decode(istream, device_register_fields, &rcu->Mode_table[rcu->Mode_table_entries])){
        printf("decoding failed\r\n");
        return false;
    }


    rcu->Mode_table_entries++;
    return true;
}
bool RCU::name_decode_pb(pb_istream_t *istream, const pb_field_t *field, void **arg){
    Vtable_entry *entry = (Vtable_entry*)(*arg);
    const uint8_t max_name_size = 32;

    //check size
    if (istream->bytes_left > max_name_size - 1)
        return false;

    //alloc memory the size of the string
    entry->name = (char*)pvPortMalloc(sizeof(char) * istream->bytes_left + 1);
    memset(entry->name, 0 , sizeof(char) * istream->bytes_left + 1);

    //decode string and place in buffer
    if (!pb_read(istream, (uint8_t*) entry->name, istream->bytes_left)){
        printf("decoding failed\r\n");
        return false;
    }

    return true;
}

status_t RCU::compose_command(cmd *cmd, Vtable_entry *entry, uint64_t &value){
    status_t status = 0;

    value = uint64_t(cmd->data);

    //command data size is not longer than 1 byte
    if(entry->has_RW){
        if(entry->RW == variable_ReadWriteType_ReadOnly ){
            printf("entry is read only\r\n");
            return 1;
        }
    }

    if(entry->has_d_type){
        // TODO not yet implemented
    }

    uint64_t width = 8;
    //mask to width
    if(entry->has_width){
        width = entry->width;
    }

    if(!strcmp(entry->name, "LED0")){
        printf("LED 0 debug break point\r\n");
    }

    //mask the values to the width
    uint64_t mask = -1;                //                           1111 1111   set all bits to 1
    mask = ~(mask << width);    //              1111 1000 -> 0000 0111   shift the 1's entry->width bits and invert all the bits
    value = value & mask;      // 1001 0110 &  0000 0111 -> 0000 0110   bitwise and to mask all the bits

    //create mask for the existing values
    mask = -1;                              // 1111 1111
    mask = ~(mask << width);                   // 0000 0111

    //shift the value and the mask if neccecary
    if(entry->has_bitoffset){
        value = value << entry->bitoffset;
        mask = ~(mask << (entry->bitoffset));
    }

    //mask the bits of other entries and apply the new mask
    value |= (entry->value & mask);

    return status;
}

struct args{
    uint8_t type;
    void * data;
    UA_String* cmd_register;
};



bool RCU::name2_decode_pb(pb_istream_t *istream, const pb_field_t *field, void **arg){
    RCU *rcu = (RCU*)(*arg);
    const uint8_t max_name_size = 20;

    uint8_t buffer[64] = {0};

    if (istream->bytes_left > max_name_size - 1)
        return false;

    if (!pb_read(istream, buffer, istream->bytes_left))
        return false;

    rcu->Mode_names[rcu->Mode_number] = UA_STRING_ALLOC((char*)buffer);

    return true;
}



bool doMask(Vtable_entry device)
{
   return (device.has_width && (device.width%8!=0)) || (device.has_bitoffset && (device.bitoffset>0));
//   return (device.has_width && (device.width<=8)) || (device.has_bitoffset && (device.bitoffset>0));
}


uint8_t RCU::get_stored_i2cregister(Vtable_entry device)
{
   for (int x=0;x<register_values_entries;x++)
      if ((register_values_table[x].addr==device.addr) && (register_values_table[x].reg==device.reg))
         return register_values_table[x].value;
   return 0;
}

void RCU::set_stored_i2cregister(Vtable_entry device,uint8_t value)
{  //printf("I2C store x%.2X:x%.2X=x%.2X\n",device.addr,device.reg,value);
   for (int x=0;x<register_values_entries;x++)
      if ((register_values_table[x].addr==device.addr) && (register_values_table[x].reg==device.reg))
      {
         register_values_table[x].value=value;
         return ;
      }
   register_values_table[register_values_entries].addr=device.addr;
   register_values_table[register_values_entries].reg=device.reg;
   register_values_table[register_values_entries].value=value;
   register_values_entries++;
}

void RCU::set_I2Cregister( Vtable_entry device, uint8_t *bytes,uint8_t length)
{ //TODO: Apply mask
   if (doMask(device))
   {
      uint8_t previous=get_stored_i2cregister(device);
      uint8_t mask;
      if (device.has_width) mask=(1<<device.width)-1;
      else mask=0xff;
      if (device.has_bitoffset && (device.bitoffset>0))
      {
          mask<<=device.bitoffset;
          bytes[0]<<=device.bitoffset;
      }
     //printf("Mask x%.2X, Previous x%.2X, old x%.2X, new x%.2X,\n",mask,previous,bytes[0],(bytes[0] & mask) + (previous- (previous & mask)));
     bytes[0]=(bytes[0] & mask) + (previous- (previous & mask));
   }
   I2C_write_block(config->base_ptr->I2C_Handle, uint8_t(device.addr),  uint8_t(device.reg) , bytes, length);
   i2c_writecount++;
   if (length==1) set_stored_i2cregister(device,bytes[0]);
}

uint16_t RCU::get_I2Cregister( Vtable_entry device)
{ //TODO: Apply mask
   uint8_t value;
   I2C_read_block(config->base_ptr->I2C_Handle,  uint8_t(device.addr),  uint8_t(device.reg), &value, 1);
   i2c_readcount++;
   value=get_stored_i2cregister(device);  //For testing!!
   //printf("I2C read  x%.2X:x%.2X=x%.2X\n",device.addr,device.reg,value);
   if (doMask(device))
   {
      set_stored_i2cregister(device,value);
      if (device.has_bitoffset) value>>=device.bitoffset;
      if (device.has_width) value&=(1<<device.width)-1;
   }
   return value;
}

void RCU::set_SPIregister( Vtable_entry device, uint8_t *bytes,uint8_t length)
{
    if (device.addr>=SPI_bridge_table_entries) return;
    if (length!=1) return;
    SPI *SPI=&SPI_bridge_table[device.addr];

    //uint8_t ADC_bytes = 0x00;
    //uint8_t ADC_rw    = 0x00 // 0 for write, 1 for read
    //uint32_t data = ( ADC_rw << 23 ) + ( ADC_bytes << 21 ) + ( device.reg << 8 ) + bytes[0];
    uint32_t data =  ( device.reg << 8 ) + bytes[0];
    printf("SPI Reg=x%.2X Data=x%.6X\n",device.reg,data);
    uint8_t byte;
    set_I2Cregister(SPI->CS,&(byte=0),1);  //Select chip
    for (int x=23;x>=0;x--)  //3 bytes
    {
       byte=(data >> x)&1;
       set_I2Cregister(SPI->SDO,&byte,1);
       set_I2Cregister(SPI->CLK,&(byte=1),1);  //Read on upward edge - swap these two around?
       set_I2Cregister(SPI->CLK,&(byte=0),1);
    }
    set_I2Cregister(SPI->SDO,&(byte=0),1);  //Is this needed?? Should this be high?
    set_I2Cregister(SPI->CS,&(byte=1),1);
}

uint16_t RCU::get_SPIregister( Vtable_entry device)
{
    if (device.addr>=SPI_bridge_table_entries) return 0;
    SPI *SPI=&SPI_bridge_table[device.addr];

    //ADC_bytes = 0x00 = one byte
    //ADC_rw    = 0x01 # 0 for write, 1 for read
    //data = ( ADC_rw << 15) + ( ADC_bytes << 13 ) + reg
    uint16_t data = (1<<15) +  device.reg;
    uint8_t byte;
    set_I2Cregister(SPI->CS,&(byte=0),1);  //Select chip
    for (int x=15;x>=0;x--)  //2 bytes
    {
       byte=(data >> x)&1;
       set_I2Cregister(SPI->SDO,&byte,1);
       set_I2Cregister(SPI->CLK,&(byte=1),1);  //Read on upward edge - swap these two around?
       set_I2Cregister(SPI->CLK,&(byte=0),1);
    }
    set_I2Cregister(SPI->CS,&(byte=1),1);
    //read byte
    set_I2Cregister(SPI->DIR,&(byte=1),1); //make SDIO an input
    set_I2Cregister(SPI->CS,&(byte=0),1);

    data=0;
    for (int x=0;x<8;x++) //one byte
    {
        data+=(get_I2Cregister(SPI->SDI) & 1);
        data<<=1;
        set_I2Cregister(SPI->CLK,&(byte=1),1);
        set_I2Cregister(SPI->CLK,&(byte=0),1);  //Read after faling edge
    }
    set_I2Cregister(SPI->CS,&(byte=1),1);
    set_I2Cregister(SPI->DIR,&(byte=0),1); //make SDIO an output again
    printf("SPI Reg=x%.2X Data=x%.2X\n",device.reg,data);
    return data;
}



void RCU::set_register( Vtable_entry device, uint8_t *bytes,uint8_t length)
{
    if (not(device.has_d_type) || device.d_type==device_register_device_type_i2c) set_I2Cregister(device,bytes,length);
    else if (device.d_type==device_register_device_type_spi) set_SPIregister(device,bytes,length);
}

uint16_t RCU::get_register( Vtable_entry device)
{
    if (not(device.has_d_type) || device.d_type==device_register_device_type_i2c) return get_I2Cregister(device);
    else if (device.d_type==device_register_device_type_spi) return  get_SPIregister(device);
}


void RCU::set_variable(uint8_t number,uint8_t *bytes,uint8_t length)
{
//TODO: Check min,max range
    set_register(variable_table[number],bytes,length);
}

uint16_t RCU::get_variable(uint8_t number)
{
    uint16_t value=get_register(variable_table[number]);
//TODO: Apply scaling, min
   return value;
}

//void RCU::set_method(uint8_t number)
//{
//  for (int x=Mode_index[number];x<Mode_index[number+1];x++)
//  {
//     set_register(Mode_table[x], Mode_values[x], Mode_value_len[x]);
//
//  }
//}


