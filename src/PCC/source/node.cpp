/*
 * Copyright 2020 Stichting Nederlandse Wetenschappelijk Onderzoek Instituten,
 * ASTRON Netherlands Institute for Radio Astronomy
 * Licensed under the Apache License, Version 2.0 (the "License");
 *
 * you may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * See ../LICENSE.txt for more info.
 */



//#include <iostream>

#include <string>
#include <cmath>

//#include "fsl_common.h"
//#include "peripherals.h"

#include <fsl_device_registers.h>
#include "fsl_debug_console.h"

//#include "fsl_lpi2c.h"
//#include "fsl_lpi2c_freertos.h"

extern "C" {
    #include <common.h>
}

#include "node.h"
//#include "commands.h"
#include "devices.h"


#define do_compact  true //creates a seperate OPC node that contains a simple array of all values, requires extra processing and memory
#define IGNORE_SWITCH_ERRORS true   //do not skip subnodes when a switch fails. useful for debugging


//this function takes a UA_DateTime and returns a human readable UA_String
UA_String UA_readable_time(){
    char readable_time[] = "9999-12-31 23:59:59.999";   //char array for readable time
    UA_DateTimeStruct dts = UA_DateTime_toStruct(UA_DateTime_now());  //readable time
    sprintf(readable_time, "%04u-%02u-%02u %02u:%02u:%02u.%03u", dts.year, dts.month, dts.day, dts.hour, dts.min, dts.sec, dts.milliSec);
    UA_String time_string = UA_STRING_ALLOC(readable_time);
    return time_string;
}


/*      node class implementation
 *  ==========================================
 * */
node::node( monitored_device    **monitored_ptr_arr,    uint8_t _nof_monitored, // **monitored_ptr_arr: pointer to pointer array of all monitored devices,
            node                **subnodes_ptr_arr,     uint8_t _nof_subnodes,  // **subnodes_ptr_arr: pointer to pointer array of all subnodes
            device              **devices_ptr_arr,      uint8_t _nof_devices,   // **devices_ptr_arr: pointer to pointer array of all non-monitored devices
            string              _name,
            int8_t              _switch_channel,        int8_t _switch_nr){     //, I2C_switch *_switcher = nullptr)    // data about how to reach this node and where it is located

    name = _name;

    switch_nr = _switch_nr;             //to which switch this node is connected to (NONE means previous node doesn't have a switch)
    switch_channel  = _switch_channel;  //to which channel of a switch this node is connected to

    nof_monitored = _nof_monitored;
    monitored = monitored_ptr_arr;

    nof_subnodes = _nof_subnodes;
    subnodes = subnodes_ptr_arr;

    nof_devices = _nof_devices;
    devices = devices_ptr_arr;

    //check if any of the devices are an I2C switch, if so, set the switch pointer to it(only one switch allowed currently)
    for (int i = 0; i < _nof_devices; i++) {
        if (devices_ptr_arr[i]->type == I2C_switch_device) {
            switcher = reinterpret_cast<I2C_switch*>(devices_ptr_arr[i]);
            break;
        }
    }

    //get the number of data points on this node
    for (int i = 0; i < nof_monitored; i++) {
        nof_datapoints = nof_datapoints + monitored[i]->nof_sensors;
    }
    //create storage for the number of data points
    if(nof_datapoints > 0){
        datapoints = (float*)pvPortMalloc(sizeof(float) * nof_datapoints);
    }

    //set the parent for all the subnodes
    for(int i =0; i < nof_subnodes; i++){
        subnodes[i]->parent_node = this;
    }
}


uint16_t node::readout_node(lpi2c_rtos_handle_t *I2C_Handle, uint16_t &datapoint_cnt, base_node *base_ptr){ //read all the devices on this node and its subnodes
    status_t status = 0;       //holds the status of the I2C read
    uint16_t failure_cnt = 0;   //whenever setting a switch fails, this counter is incremented
    uint8_t data_ptr = 0;
    uint16_t busy_flag_cnt = 0;


#if LOGGING_GENERAL == true
    printf( "entering node: %s\r\n", name.c_str());
#endif

//    base_ptr->command_handling(); //check if there is a new command, if so handle it

    //go through all devices on the current node
    for (int i = 0; i < nof_monitored; i++) {

        //check if the command handler wants to do something and we should halt
        while(base_ptr->cmd_busy_flag && busy_flag_cnt < 25){
            base_ptr->ack_cmd_flag = true;
            vTaskDelay(pdMS_TO_TICKS(1));
            busy_flag_cnt++;
        }
        busy_flag_cnt = 0;
        base_ptr->ack_cmd_flag = false;

        status = monitored[i]->device_readout(I2C_Handle, &datapoints[data_ptr]);

        if(status){
            //if any were unsuccesful, log this
            base_ptr->I2C_log_failure(monitored_type);
        }
        base_ptr->monitored_counter++;

        for(int j = 0; j < monitored[i]->nof_sensors; j++){
            update_OPCua(base_ptr->server, datapoint_cnt, datapoints[data_ptr]);
            data_ptr++;
        }
    }


    //go through the all the subnodes this node has
    for (int i = 0; i < nof_subnodes; i++) {

#if LOGGING_GENERAL == true
            printf("%s - switching to channel: %d", subnodes[i]->name.c_str(), int(subnodes[i]->switch_channel));
            printf( ", of switch: %d, %s located on: %s\r\n", int(switch_nr), switcher->name.c_str(), name.c_str());
#endif

#if IGNORE_SWITCH_ERRORS == false
        //if they have a switch, first set the switch correctly
        if (switch_nr != NONE && switcher != nullptr) {

            status = switcher->set_channel(I2C_Handle, subnodes[i]->switch_channel);    //change the selected channel to that of the next sub node

            //if switching worked, go into the subnodes
            if(status == kStatus_Success ){
                base_ptr->switch_counter++;
                subnodes[i]->readout_node(I2C_Handle, datapoint_cnt, base_ptr);
            }
            //if switching failed, skip all subnodes and get the number of datapoints skipped
            else{
                status = subnodes[i]->get_nof_monitored();    //this many devices are skipped
                base_ptr->I2C_log_failure(unmonitored_type);
                for(int j = 0; j < status; j++){
                    base_ptr->I2C_log_failure(monitored_type);
                    base_ptr->monitored_counter++;
                }
                base_ptr->switch_counter++;
            }
        }
        //if there is no switch to worry about
        else{
            subnodes[i]->readout_node(I2C_Handle, datapoint_cnt, base_ptr);
        }
#else
        //dont skip subnodes if the swithc to then cant be reached
        if (switch_nr != NONE && switcher != nullptr) {
            status = switcher->set_channel(I2C_Handle, subnodes[i]->switch_channel);    //change the selected channel to that of the next sub node
            if(status != kStatus_Success){
                base_ptr->I2C_log_failure(unmonitored_type);

            }
            base_ptr->switch_counter++;
        }
        subnodes[i]->readout_node(I2C_Handle, datapoint_cnt, base_ptr);
#endif

    }
    if (switch_nr != NONE && switcher != nullptr) {
        switcher->set_off(I2C_Handle); //turn of this switch when done
    }
    return failure_cnt;
}


//status_t node::check_queue(base_node *base_ptr){
//    base_ptr->
//}

//does all the extra neccecary init stuff, inlcuding initialising all devices that may require it
uint16_t node::init(lpi2c_rtos_handle_t *I2C_Handle, uint16_t next_num){
    uint16_t failure_cnt = 0;

    //go through all the subnodes
    for (int i = 0; i < nof_subnodes; i++) {
        failure_cnt += subnodes[i]->init(I2C_Handle, next_num);
    }

    //init the monitored devices on this node
    for (int i = 0; i < nof_monitored; i++) {
        failure_cnt += monitored[i]->init(I2C_Handle);
    }

    //init the unmonitored devices on this node
    for (int i = 0; i < nof_devices; i++) {
        failure_cnt += devices[i]->init(I2C_Handle);
    }

    return failure_cnt;
}

//node *node::find_node(uint16_t node_num, bool found){
//    node *return_node = 0;
//
//    for (int i = 0; i < nof_subnodes; i++) {
//        return_node = subnodes[i]->find_node(node_num);
//    }
//    return return_node;
//}

//contains what were previously a number of seperate funcions that count the number of items in the device tree
void node::get_tree_info(uint16_t &_nof_datapoints, uint16_t &_nof_devices, uint16_t &_nof_monitored, uint16_t &_nof_nodes, uint16_t &_nof_switched_nodes, uint16_t &_nof_switches){

    //get the number of data points the subnodes contain
    for (int i = 0; i < nof_subnodes; i++) {
        subnodes[i]->get_tree_info(_nof_datapoints, _nof_devices, _nof_monitored, _nof_nodes, _nof_switched_nodes, _nof_switches);
    }

    _nof_nodes += nof_subnodes;   //add the number of subnodes in this node
    _nof_devices += nof_devices;   //add devices in this node
    _nof_monitored += nof_monitored;   //add monitored devices in this node

    //count the number of nodes that are reached by switching an I2C switch
    if(switch_channel != NONE){
        _nof_switched_nodes++;
    }

    //count the number of nodes that have  a switch in them
    if(switcher != nullptr){
        _nof_switches++;
    }

    //get the number of data points on this node
    for (int i = 0; i < nof_monitored; i++) {
        _nof_datapoints += monitored[i]->nof_sensors;
    }
}


uint16_t node::get_nof_datapoints() {   //counts the total number of datapoints this nodes (its devices and subnodes) has
    uint16_t nof_datapoints_cnt = 0;

    //get the number of data points the subnodes contain
    for (int i = 0; i < nof_subnodes; i++) {
        nof_datapoints_cnt += subnodes[i]->get_nof_datapoints();
    }

    //get the number of data points on this node
    for (int i = 0; i < nof_monitored; i++) {
        nof_datapoints_cnt += monitored[i]->nof_sensors;
    }

    return nof_datapoints_cnt;
}

uint16_t node::get_nof_monitored(){       //get the number get the total number of monitored devices in this node and all its subnodes
    uint16_t nof_devices_cnt = 0;

    for (int i = 0; i < nof_subnodes; i++) {
        nof_devices_cnt += subnodes[i]->get_nof_monitored();
    }

    nof_devices_cnt += nof_monitored;   //add own devices

    return nof_devices_cnt;
}

void node::store_switches(base_node *base, uint8_t &switch_count){

    for (int i = 0; i < nof_subnodes; i++) {
        subnodes[i]->store_switches(base, switch_count);
    }
    if(switcher != nullptr){
        base->switch_ptr_arr[switch_count] = switcher;
        switch_count++;
    }
}

base_node *node::get_base_node(){
    base_node *base_ptr = nullptr;

    //only the base node has as parent node a nullptr
    if(parent_node != nullptr){
        base_ptr = parent_node->get_base_node();
        return base_ptr;
    }
    else {
        return (base_node*)parent_node;
    }
}


//duplicate the node as well as all its subnodes (not devices since they dont (TODO maybe?) serve any storage purposes)
node* node::duplicate_node(node *original_node){

    //allocate memory for a node object. Then copy the old node
    node  *copied_node_ptr = (node*)pvPortMalloc(sizeof(node));
    memcpy(copied_node_ptr, original_node, sizeof(node));

    //malloc a pointer array for all the subnodes
    if(copied_node_ptr->nof_subnodes > 0){
        copied_node_ptr->subnodes = (node**)pvPortMalloc(sizeof(node*) * copied_node_ptr->nof_subnodes);

        //create the actual subnodes and point the pointer array to the subnodes
        for(int i = 0; i < copied_node_ptr->nof_subnodes; i++){
            copied_node_ptr->subnodes[i]->parent_node = copied_node_ptr;

            //duplicate the subnodes as well
            copied_node_ptr->subnodes[i] = duplicate_node(original_node->subnodes[i]);
        }
    }

    return copied_node_ptr;
}

//copies pointer array
node** node::duplicate_group(node **subnodes, uint8_t nof_subnodes){
    node **grouped_ptr_arr = (node**)pvPortMalloc(sizeof(node*) * nof_subnodes);

    for(int i = 0; i < nof_subnodes; i++){
        grouped_ptr_arr[i] = duplicate_node(subnodes[i]);
    }

    return grouped_ptr_arr;
}
//
//adds the node to the OPC ua address space
//can not be called before the base node has been initialised and all nodes are created
uint16_t node::add_OPCua(base_node *base_ptr, UA_NodeId UA_parentNode ){
    // used for getting variable node ID's that can be easily referenced
    static uint16_t nodeID_cnt = 10000; //semi-arbitrary offset from zero
    UA_StatusCode status;

    //create a OPC ua node for this node. Add it as a child to the parent node
    UA_ObjectAttributes oAttr = UA_ObjectAttributes_default;
    //oAttr.displayName = UA_LOCALIZEDTEXT_ALLOC("en-US", name.c_str());
    //nodeID = UA_NODEID_STRING_ALLOC(2, name.c_str());
    nodeID = UA_NODEID_STRING_ALLOC(2, name.c_str());

    status = UA_Server_addObjectNode(base_ptr->server, UA_NODEID_NULL,
                            UA_parentNode,
                            UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                            UA_QUALIFIEDNAME_ALLOC(2, name.c_str()),
                            UA_NODEID_NUMERIC(0, UA_NS0ID_BASEOBJECTTYPE),
                            oAttr, NULL, &nodeID);
    if(status) printf("couldn't add UA object node\r\n");

    //add the devices
    for(int i = 0; i < nof_monitored; i++){

        //some moniotred devices have multiple sensors
        if(monitored[i]->nof_sensors > 1){
            //TODO new node

            //create a new node if there are multiple sensors in a single device
            UA_NodeId obj_nodeID;
            UA_ObjectAttributes oAttr = UA_ObjectAttributes_default;
            oAttr.displayName = UA_LOCALIZEDTEXT_ALLOC("en-US", monitored[i]->name.c_str());
            obj_nodeID = UA_NODEID_STRING_ALLOC(2, monitored[i]->name.c_str());

            UA_QualifiedName oqname = UA_QUALIFIEDNAME_ALLOC(2, monitored[i]->name.c_str());

            status = UA_Server_addObjectNode(base_ptr->server, UA_NODEID_NULL,
                                    nodeID,
                                    UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                                    oqname,
                                    UA_NODEID_NUMERIC(0, UA_NS0ID_BASEOBJECTTYPE),
                                    oAttr, NULL, &obj_nodeID);



            if(status) printf("couldn't add UA object node\r\n");

            //add variables for each of the datapoints
            for(int j = 0; j < monitored[i]->nof_sensors; j++){

                UA_VariableAttributes vAttr = UA_VariableAttributes_default;
                UA_Variant_setScalar(&vAttr.value, (UA_Float*)&datapoints[i], &UA_TYPES[UA_TYPES_FLOAT]);
                string tempname;
                if(monitored[i]->type == PMBUS_mon_type){
                    tempname = name + "_" + monitored[i]->name + "_" + to_string(j + 1);
                }
                else{
                    tempname = name + "_" + monitored[i]->name + "_" + to_string(j + 1);
                }


                UA_NodeId var_nodeID = UA_NODEID_NUMERIC(2, nodeID_cnt);
                UA_QualifiedName vqname = UA_QUALIFIEDNAME_ALLOC(2, tempname.c_str());

                vAttr.displayName = UA_LOCALIZEDTEXT_ALLOC("en-US", tempname.c_str());
                /* Add the variable node to the information model */

                status = UA_Server_addVariableNode(base_ptr->server, var_nodeID,
                                    obj_nodeID,
                                    UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                                    vqname,
                                    UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                                    vAttr, NULL, NULL);

                if(status) printf("couldn't add UA variable node\r\n");

                nodeID_cnt++;
//                /* allocations on the heap need to be freed */

                  //UA_NodeId_clear(&var_nodeID);
                UA_QualifiedName_clear(&vqname);
                UA_LocalizedText_clear(&(vAttr.displayName));
            }

            UA_QualifiedName_clear(&oqname);
            UA_LocalizedText_clear(&(oAttr.displayName));
            UA_ObjectAttributes_clear(&oAttr);
            UA_NodeId_clear(&obj_nodeID);
        }
        else {
            //if the monitored device contains only a single sensor, just add it as a variable
            UA_VariableAttributes vAttr = UA_VariableAttributes_default;

            UA_Variant_setScalar(&vAttr.value, (UA_Float*)&datapoints[i], &UA_TYPES[UA_TYPES_FLOAT]);

            string tempname = name + "_" + monitored[i]->name;
            //UA_NodeId var_nodeID = UA_NODEID_STRING_ALLOC(2, tempname.c_str());
            //UA_NodeId var_nodeID = UA_NODEID_STRING_ALLOC(2, to_string(nodeID_cnt).c_str());
            UA_NodeId var_nodeID = UA_NODEID_NUMERIC(2, nodeID_cnt);

            vAttr.displayName = UA_LOCALIZEDTEXT_ALLOC("en-US", tempname.c_str());
            /* Add the variable node to the information model */

            status = UA_Server_addVariableNode(base_ptr->server, var_nodeID,   //UA_NODEID_NULL
                                nodeID,
                                UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                                UA_QUALIFIEDNAME_ALLOC(2, tempname.c_str()),
                                UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                                vAttr, NULL, NULL);
            if(status) printf("couldn't add UA variable node\r\n");
            nodeID_cnt++;

            /* allocations on the heap need to be freed */
            UA_VariableAttributes_clear(&vAttr);
            UA_NodeId_clear(&var_nodeID);
        }
    }

    //also add all the subnodes
    for(int i = 0; i < nof_subnodes; i++){
        nodeID_cnt = subnodes[i]->add_OPCua(base_ptr, nodeID);
    }
    return nodeID_cnt;
}

//updates OPC a variable values with the stored sensor readout values
void node::update_OPCua(UA_Server *server, uint16_t &datapoint_cnt, float value){
        UA_Variant Var;
        UA_StatusCode status;

        UA_Variant_init(&Var);
        UA_NodeId variable_ID = UA_NODEID_NUMERIC(2, UA_UInt32(datapoint_cnt));

        UA_Variant_setScalar(&Var, &value, &UA_TYPES[UA_TYPES_FLOAT]);
        status = UA_Server_writeValue(server, variable_ID, Var);
        if(status) {
            printf("couldn't write UA_node\r\n");
        }

        datapoint_cnt++;

        /* allocations on the heap need to be freed */
        UA_NodeId_clear(&variable_ID);
}


/*      base node class implementation
 *  ==========================================
 * */

base_node::base_node(lpi2c_rtos_handle_t *_I2C_Handle, UA_Server *mUaServer, uint8_t _I2C_num, uint16_t idx, string _name)
    : node(nullptr, 0, nullptr, 0, nullptr, 0, _name, NONE, NONE){//, nullptr) { //FIXME changed node(nullptr, 0, subnodes, 2, nullptr, 0, _name, NONE, NONE)

//    queue = cmd_queue;
    server = mUaServer;
    I2C_Handle = _I2C_Handle;
    I2C_unit_nr = _I2C_num;
    lofar_idx = idx;
}


void base_node::update_buffer() {
    uint16_t datapoint_cnt = nodeID_offset;
    monitored_counter = 0;
    switch_counter = 0;
    monitored_failure_cnt = 0;
    switch_failure_cnt = 0;

    uint16_t failures = readout_node(I2C_Handle, datapoint_cnt, this);
    if(failures){
        printf("unable to complete %d transfers\r\n", failures);
    }

    process_I2C_logs();
    wait_remainder();
}

//void base_node::command_handling(){
//    struct I2C_cmd_queue_element received_data;
//    memset(&received_data, 0 , sizeof(I2C_cmd_queue_element));
//
//    uint8_t current_state[total_switches];    //stores the value for the switches
//
//    //check commands
//    if(uxQueueMessagesWaiting(handler->cmd_queue)){                      //if the queue isnt empty
//        xQueuePeek(handler->cmd_queue, &received_data, 0);
//        if(received_data.I2C_unit == I2C_unit_nr){                //check if this command uses this I2C_unit
//            xQueueReceive(handler->cmd_queue, &received_data, 0);  //if so, recieve the command data
//
//
//            //save state
//            for(int i = 0; i < total_switches; i++){
//                current_state[i] = switch_ptr_arr[i]->current_config;
//                switch_ptr_arr[i]->set_off(I2C_Handle);     //NOTE unnecessary step since the command will also set the switches, but definitely save
//            }
//
//            printf("TODO: command handling\r\n");
//            //TODO: command handling
//
//            //restore state
//            for(int i = 0; i < total_switches; i++){
//                switch_ptr_arr[i]->set_configuration(I2C_Handle, current_state[i]);
//            }
//        }
//    }
//}

//called each I2C failure
void base_node::I2C_log_failure(type_of_device type){
    if(logging == false){
        return;
    }

    if(type == monitored_type){
        if(monitored_counter > total_monitored){
            printf("there arent that many monitored devices on this node. %d of %d", monitored_counter, total_monitored);
            return; //invalid monitored NOTE: is it sending datapoitns or device numbers?
        }
        //printf("logged that monitored device %d failed\r\n", monitored_counter);
        monitored_failure_cnt++;
        monitored_failure_array[monitored_counter]++;
    }
    else{
        if(switch_counter > total_switched_nodes){
            printf("there arent that many switches on this node. %d of %d", switch_counter, total_switched_nodes);
            return; //invalid non-monitored
        }
        //printf("logged that switching %d failed\r\n", switch_counter);
        switch_failure_cnt++;
        switch_failure_array[switch_counter]++;
    }
}

//called after each base node readout
void base_node::process_I2C_logs(){
    if(logging == false){
        return;
    }


    UA_StatusCode status;
/* when the if statements conditions are met,
 * writes the monitored_failure_array and
 * */

    if(++I2C_publish_cnt >= 10){    //arbitrary value, means it will publish new results every that many calls
        //----------------------------------------------------------------------------------------------
        //set the monitored device
        UA_Variant monitored_var;

        UA_UInt32 monitoredDims[1] = {total_monitored};
        monitored_var.arrayDimensions = monitoredDims;  //size of each dimension
        monitored_var.arrayDimensionsSize = 1;      //number of dimenstions

        UA_Variant_setArray(&monitored_var, monitored_failure_array, total_monitored, &UA_TYPES[UA_TYPES_BYTE]);
        status = UA_Server_writeValue(server, monitored_trackerID, monitored_var);
        if(status) {
            printf("couldn't write I2C monitored array\r\n");
        }

        //----------------------------------------------------------------------------------------------
        //TODO: the non-monitored devices dont get called in a sequential fashion. Therefore this wont work. Needs to be reworked.
        UA_Variant devices_var;
        UA_UInt32 devicesDims[1] = {total_switched_nodes};
        devices_var.arrayDimensions = devicesDims;  //size of each dimension
        devices_var.arrayDimensionsSize = 1;      //number of dimenstions

        UA_Variant_setArray(&devices_var, switch_failure_array, total_switched_nodes, &UA_TYPES[UA_TYPES_BYTE]);
        status = UA_Server_writeValue(server, switch_trackerID, devices_var);
        if(status) {
            printf("couldn't write I2C device array\r\n");
        }

        //NOTE: currently only refreshes once every 60 seconds with completely new data. maybe make it more continuous.
        // the issue with that, is that i dont see how to do that without using a lot of memory. as every device reserves a datapoint

        I2C_publish_cnt = 0;
        memset(monitored_failure_array, 0, total_monitored);
        memset(switch_failure_array, 0, total_switched_nodes);
    }

    UA_Variant monitored_cnt;
    UA_Variant_setScalar(&monitored_cnt, &monitored_failure_cnt, &UA_TYPES[UA_TYPES_UINT16]);
    status = UA_Server_writeValue(server, monitored_failure_countID, monitored_cnt);
    if(status) {
        printf("couldn't write device failure count node\r\n");
    }

    UA_Variant device_cnt;
    UA_Variant_setScalar(&device_cnt, &switch_failure_cnt, &UA_TYPES[UA_TYPES_UINT16]);
    status = UA_Server_writeValue(server, switch_failure_countID, device_cnt);
    if(status) {
        printf("couldn't write device failure count node\r\n");
    }
}

void base_node::wait_remainder(){
    static const uint32_t readout_interval = pdMS_TO_TICKS(1000);        //the intervals between readouts (1000ms)
    static const uint32_t command_interval = 2;                         //check for new commands each this many ms
    sleep_flag = true;

    uint32_t wait_time = next_cycle - pdMS_TO_TICKS(xTaskGetTickCount()); //calculate how long the task has to wait

    if(wait_time > readout_interval){   //overflow protection
        wait_time = readout_interval;
    }

    //calculates how long th readout took
    uint32_t cycle_time = pdMS_TO_TICKS(xTaskGetTickCount()) - (next_cycle - readout_interval);

    if(wait_time <= readout_interval){    //account for overflows and > 1s readouts

        //loop with delay and command check until the wait time is over
        while(wait_time > command_interval){
            ack_cmd_flag = true;
            vTaskDelay(command_interval);
            wait_time = next_cycle - pdMS_TO_TICKS(xTaskGetTickCount());

            //TODO: do stuff, call command handler
        }
        vTaskDelay(wait_time);
        ack_cmd_flag = false;
    }
    else{
        //NOTE if the counter overflows, will just report a bogus wait time and do another readout instantly instead of waiting a second
        printf("took longer than %ums, %u \r\n", readout_interval, pdMS_TO_TICKS(xTaskGetTickCount()) - (next_cycle - readout_interval));
    }

    static UA_Variant cycle_readout_var;
    UA_Variant_init(&cycle_readout_var);
    UA_Variant_setScalar(&cycle_readout_var, &cycle_time, &UA_TYPES[UA_TYPES_UINT32]);
    status_t status = UA_Server_writeValue(server, readout_loopID, cycle_readout_var);
    if(status) {
        printf("couldn't write node\r\n");
    }

    static UA_Variant time_readout_var;
    UA_Variant_init(&time_readout_var);
    UA_String current_time = UA_readable_time();

    UA_Variant_setScalar(&time_readout_var, &current_time, &UA_TYPES[UA_TYPES_STRING]);
    status = UA_Server_writeValue(server, readout_timeID, time_readout_var);
    if(status) {
        printf("couldn't write node\r\n");
    }


    ack_cmd_flag = false;
    sleep_flag = false;
    next_cycle = pdMS_TO_TICKS(xTaskGetTickCount()) + readout_interval; //get the time just before the next buffer update started in ms
}


//creates an byte array the sze of the number of datapoints. Each datapoint will store information about reading out the sensors
void base_node::I2C_monitor_init(){

//
    UA_StatusCode status;

    UA_ObjectAttributes oAttr = UA_ObjectAttributes_default;
    base_trackerID = UA_NODEID_STRING(2, "status_monitor");
    status = UA_Server_addObjectNode(   server, UA_NODEID_NULL,
                                        nodeID,
                                        UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                                        UA_QUALIFIEDNAME_ALLOC(2, "I2C_unit_status_monitor"),
                                        UA_NODEID_NUMERIC(0, UA_NS0ID_BASEOBJECTTYPE),
                                        oAttr, NULL, &base_trackerID);
    if(status) printf("couldn't add base status node\r\n");

    //readout cycle variable node
    //--------------------------------------------------------------------
    UA_VariableAttributes vAttr = UA_VariableAttributes_default;
    UA_Variant_setScalar(&vAttr.value, nullptr, &UA_TYPES[UA_TYPES_UINT32]);
    readout_loopID = UA_NODEID_STRING(2, "readout cycle time");
    vAttr.description = UA_LOCALIZEDTEXT("en-US", "The time it took to go through all devices connected to this I2C unit");

    status = UA_Server_addVariableNode( server, UA_NODEID_NULL,
                                        base_trackerID,
                                        UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                                        UA_QUALIFIEDNAME(2, "readout_time(ms)"),
                                        UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                                        vAttr, NULL, &readout_loopID);
    if(status) printf("couldn't add readout duration node\r\n");

    //last readout time
    //--------------------------------------------------------------------
    vAttr = UA_VariableAttributes_default;
    UA_Variant_setScalar(&vAttr.value, nullptr, &UA_TYPES[UA_TYPES_UINT32]);
    readout_timeID = UA_NODEID_STRING(2, "last_readout");
    vAttr.description = UA_LOCALIZEDTEXT("en-US", "The last time the I2C unit read out all its devices");

    status = UA_Server_addVariableNode( server, UA_NODEID_NULL,
                                        base_trackerID,
                                        UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                                        UA_QUALIFIEDNAME(2, "last_readout"),
                                        UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                                        vAttr, NULL, &readout_timeID);
    if(status) printf("couldn't add readout time node\r\n");

    //------------------------------------------------------
    if(logging == false){
        return;
    }

    UA_VariableAttributes monitored_attr = UA_VariableAttributes_default;
    //required_size = nof_datapoints/sizeof(byte); //every readout can be represented as a single bit

    /* Set the variable value constraints */
    monitored_attr.dataType = UA_TYPES[UA_TYPES_BYTE].typeId;
    monitored_attr.valueRank = UA_VALUERANK_ONE_DIMENSION;
    UA_UInt32 monitoredDims[1] = {total_monitored};
    monitored_attr.arrayDimensions = monitoredDims;  //size of each dimension
    monitored_attr.arrayDimensionsSize = 1;      //number of dimenstions

    memset(monitored_failure_array, 0, total_monitored);
    UA_Variant_setArray(&monitored_attr.value, monitored_failure_array, total_monitored, &UA_TYPES[UA_TYPES_BYTE]);
    monitored_attr.value.arrayDimensions = monitoredDims;
    monitored_attr.value.arrayDimensionsSize = 1;

    monitored_trackerID = UA_NODEID_STRING(2, "monitored_I2C_problems");
    UA_Server_addVariableNode(server, UA_NODEID_NULL,
                                    base_trackerID,
                                    UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                                    UA_QUALIFIEDNAME(2, "monitored_I2C_problems"),
                                    UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                                    monitored_attr, NULL, &monitored_trackerID);


    //// ----------------------------------------------
    //same, but for switches

    UA_VariableAttributes switch_attr = UA_VariableAttributes_default;
    /* Set the variable value constraints */
    switch_attr.dataType = UA_TYPES[UA_TYPES_BYTE].typeId;
    switch_attr.valueRank = UA_VALUERANK_ONE_DIMENSION;
    UA_UInt32 devicesDims[1] = {total_switched_nodes};
    switch_attr.arrayDimensions = devicesDims;  //size of each dimension
    switch_attr.arrayDimensionsSize = 1;      //number of dimenstions

    memset(switch_failure_array, 0, total_switched_nodes);
    UA_Variant_setArray(&switch_attr.value, switch_failure_array, total_switched_nodes, &UA_TYPES[UA_TYPES_BYTE]);
    switch_attr.value.arrayDimensions = devicesDims;
    switch_attr.value.arrayDimensionsSize = 1;

    switch_trackerID = UA_NODEID_STRING(1, "switch_I2C_problems");
    UA_Server_addVariableNode(server, UA_NODEID_NULL,
                                    base_trackerID,
                                    UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                                    UA_QUALIFIEDNAME(1, "switch_I2C_problems"),
                                    UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                                    switch_attr, NULL, &switch_trackerID);

    //--------------------------------------------------------------------------------
    //add monitored failure count
    vAttr = UA_VariableAttributes_default;
    UA_Variant_setScalar(&vAttr.value, &monitored_failure_cnt, &UA_TYPES[UA_TYPES_UINT16]);
    monitored_failure_countID = UA_NODEID_STRING(2, "failed_monitored_devices_count");
    status = UA_Server_addVariableNode(server, UA_NODEID_NULL,   //UA_NODEID_NULL
                        base_trackerID,
                        UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                        UA_QUALIFIEDNAME(2, "failed_monitored_devices_count"),
                        UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                        vAttr, NULL, &monitored_failure_countID);
    if(status) printf("couldn't add node\r\n");


    //add switch failure count node
    vAttr = UA_VariableAttributes_default;
    UA_Variant_setScalar(&vAttr.value, &switch_failure_cnt, &UA_TYPES[UA_TYPES_UINT16]);
    switch_failure_countID = UA_NODEID_STRING(2, "failed_switch_actions_count");
    status = UA_Server_addVariableNode(server, UA_NODEID_NULL,   //UA_NODEID_NULL
                        base_trackerID,
                        UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                        UA_QUALIFIEDNAME(2, "failed_switch_actions_count"),
                        UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                        vAttr, NULL, &switch_failure_countID);
    if(status) printf("couldn't add node\r\n");

    //--------------------------------------------------------------------------------
    //add monitored device count
    vAttr = UA_VariableAttributes_default;
    UA_Variant_setScalar(&vAttr.value, &total_monitored, &UA_TYPES[UA_TYPES_UINT16]);
    UA_NodeId monitored_countID = UA_NODEID_STRING(2, "total_monitored");
    status = UA_Server_addVariableNode(server, UA_NODEID_NULL,   //UA_NODEID_NULL
                        base_trackerID,
                        UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                        UA_QUALIFIEDNAME(2, "total_monitored"),
                        UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                        vAttr, NULL, &monitored_countID);
    if(status) printf("couldn't add node\r\n");
    //--------------------------------------------------------------------------------
    //add switch count
    vAttr = UA_VariableAttributes_default;
    UA_Variant_setScalar(&vAttr.value, &total_switched_nodes, &UA_TYPES[UA_TYPES_UINT16]);
    UA_NodeId switch_countID = UA_NODEID_STRING(2, "total_switch_channels");
    status = UA_Server_addVariableNode(server, UA_NODEID_NULL,   //UA_NODEID_NULL
                        base_trackerID,
                        UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                        UA_QUALIFIEDNAME(2, "total_switch_channels"),
                        UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                        vAttr, NULL, &switch_countID);
    if(status) printf("couldn't add node\r\n");
    //--------------------------------------------------------------------------------
    //add total datapoints
    vAttr = UA_VariableAttributes_default;
    UA_Variant_setScalar(&vAttr.value, &total_datapoints, &UA_TYPES[UA_TYPES_UINT16]);
    UA_NodeId datapoint_countID = UA_NODEID_STRING(2, "total_datapoints");
    status = UA_Server_addVariableNode(server, UA_NODEID_NULL,   //UA_NODEID_NULL
                        base_trackerID,
                        UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                        UA_QUALIFIEDNAME(2, "total_datapoints"),
                        UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                        vAttr, NULL, &datapoint_countID);
    if(status) printf("couldn't add node\r\n");
}
