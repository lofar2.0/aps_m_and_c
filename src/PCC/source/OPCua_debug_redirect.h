/*
 * Copyright 2020 Stichting Nederlandse Wetenschappelijk Onderzoek Instituten,
 * ASTRON Netherlands Institute for Radio Astronomy
 * Licensed under the Apache License, Version 2.0 (the "License");
 *
 * you may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * See ../LICENSE.txt for more info.
 */


#ifndef OPCUA_DEBUG_REDIRECT_H_
#define OPCUA_DEBUG_REDIRECT_H_


#ifdef __cplusplus
extern "C" {
#endif

#define DBG_BUF_LINES 32            //number of debug messages stored WARNING: too high can cause LWIP errors due to limited resources
#define DBG_MAX_LEN 128             //max length a message may hae. After which it wil be ignored
#define STORE_BEFORE_INIT           //stores messages before the OPC ua server has been initialised and writes them after
#define SPLIT_WHEN_TOO_LARGE true



UA_Server *dbg_server_ptr;              //pointer to the server
UA_NodeId dbg_node_id[DBG_BUF_LINES];   //all the node ID's for the debug lines
bool dbg_initialised = false;               //initialised check for the write function



static void debug_msg_buff_init(UA_Server * server){
    dbg_server_ptr = server;
    UA_NodeId Debug_obj_id = UA_NODEID_STRING_ALLOC(2, "dbg_obj_node"); //
    UA_ObjectAttributes oAttr = UA_ObjectAttributes_default;
    oAttr.displayName = UA_LOCALIZEDTEXT("en-US", "Debug messages");
    UA_Server_addObjectNode(dbg_server_ptr, UA_NODEID_NULL,
                            UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER),
                            UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                            UA_QUALIFIEDNAME(2, "Debug_redirect"),
                            UA_NODEID_NUMERIC(0, UA_NS0ID_BASEOBJECTTYPE), oAttr, NULL, &Debug_obj_id);

    UA_String message[DBG_BUF_LINES];
    UA_VariableAttributes mAttr = UA_VariableAttributes_default;
    char dbg_pos[15];
    char init_msg[DBG_MAX_LEN];



    for(int i = 0; i < DBG_BUF_LINES; i++){

        //set stringNodeID
        sprintf(dbg_pos, "dbg_pos_%2d", i);
        dbg_node_id[i] = UA_NODEID_STRING_ALLOC(0, dbg_pos);

        //simple initial message
        sprintf(init_msg, "---\r\n");
        message[i] = UA_STRING(init_msg);


        UA_Variant_setScalarCopy(&mAttr.value, &message[i], &UA_TYPES[UA_TYPES_STRING]);
        UA_Server_addVariableNode(dbg_server_ptr, dbg_node_id[i],
                                  Debug_obj_id,
                                  UA_NODEID_NUMERIC(0, UA_NS0ID_HASCOMPONENT),
                                  UA_QUALIFIEDNAME_ALLOC(i, dbg_pos),
                                  UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE), mAttr, NULL, NULL);
    }
    dbg_initialised = true;
}


static inline void write_debug(int handle, char *data, size_t size ){

    if(!dbg_initialised){
#ifdef STORE_BEFORE_INIT
        //TODO
#endif
        return;
    }

    if(size >= DBG_MAX_LEN){

#if SPLIT_WHEN_TOO_LARGE == true

        //split the message until it fits in a single spot. NOTE a seperate max length for this is something worth considering
        while(size > DBG_MAX_LEN){
            write_debug(handle, data, DBG_MAX_LEN);

            data = data + DBG_MAX_LEN;
            size = size - DBG_MAX_LEN;
        }

#else
        return;
#endif

    }
    static uint32_t dbg_cnt = 0; //counts all debug messages processed
    static uint8_t dbg_ptr = 0; //points to one of the debug lines in the buffer

    handle = handle ; // unused

    //use type UA_string
    UA_String new_msg = {
            .length = size,
            .data = (UA_Byte*)data
    };

    //set value
    UA_Variant myVar;
    UA_Variant_init(&myVar);
    UA_Variant_setScalar(&myVar, &new_msg, &UA_TYPES[UA_TYPES_STRING]);
    UA_Server_writeValue(dbg_server_ptr, dbg_node_id[dbg_ptr], myVar);

    //new name
    char new_qname[12]; //name of node is "dbg_" + dbg_cnt (debug message counter)
    sprintf(new_qname, "dbg_%ld", dbg_cnt);
    UA_Server_writeBrowseName(dbg_server_ptr, dbg_node_id[dbg_ptr], UA_QUALIFIEDNAME(dbg_ptr, new_qname));

    dbg_cnt++;  //note: overflows at uin32_t max. unlikely to cause any issues, still noteworthy
    if (dbg_ptr++ >= DBG_BUF_LINES-1){
        dbg_ptr = 0;
    }
}



//WARNING: only works in nohost (not semihosting), see project > properties > c/c++ Build > Settings > MCU linker > Managed Linker Script > Library > newlib semihost/newlib nohost
int __attribute__((weak)) _write(int handle, char *data, int size ){
    write_debug(handle, data, size);

    return size;    //if OPCua debug is disabled don't do anything
}

#ifdef __cplusplus
}
#endif

#endif /* OPCUA_DEBUG_REDIRECT_H_ */
