/*
 * Copyright 2020 Stichting Nederlandse Wetenschappelijk Onderzoek Instituten,
 * ASTRON Netherlands Institute for Radio Astronomy
 * Licensed under the Apache License, Version 2.0 (the "License");
 *
 * you may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * See ../LICENSE.txt for more info.
 */


#ifndef DATA_STRUCTS_H_
#define DATA_STRUCTS_H_


#define device_address slaveAddress //rename slave to device


#include "fsl_common.h"
#include "peripherals.h"


//how the data is organised, NOTE will likely change over time
struct I2C_cmd_queue_element {
    uint32_t    data;
    uint16_t    RCU_nums;   //bit nr corresponds to RCU to address
    uint8_t     I2C_unit;   //I2C unit to be used, for broadcasts multiple elements may be needed
};


//------------------------------------------------------------------------------------------------------------------------------------
// I2C transfer functions
void I2C_reset(lpi2c_rtos_handle_t *I2C_Handle);    //checks which I2C unit is passed and resets that unit

status_t I2C_read_byte(lpi2c_rtos_handle_t *I2C_Handle, uint8_t address, uint8_t register_address, uint8_t *raw_byte);
status_t I2C_read_word(lpi2c_rtos_handle_t *I2C_Handle, uint8_t address, uint8_t register_address, uint16_t *raw_word);
status_t I2C_read_block(lpi2c_rtos_handle_t *I2C_Handle, uint8_t address, uint8_t register_address, void* raw_data, uint16_t data_size);

status_t I2C_write_byte(lpi2c_rtos_handle_t *I2C_Handle, uint8_t address, uint8_t register_address, uint8_t *write_byte);
status_t I2C_write_word(lpi2c_rtos_handle_t *I2C_Handle, uint8_t address, uint8_t register_address, uint16_t *write_word);
status_t I2C_write_block(lpi2c_rtos_handle_t *I2C_Handle, uint8_t address, uint8_t register_address, void* write_data, uint16_t data_size);
status_t I2C_write_reg(lpi2c_rtos_handle_t *I2C_Handle, uint8_t address, uint8_t *write_byte);   //no register_address, only 1 data byte



#endif /* DATA_STRUCTS_H_ */
