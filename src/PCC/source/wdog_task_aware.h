
#ifndef WDOG_TASK_AWARE_H_
#define WDOG_TASK_AWARE_H_


#define WDOG_BASE WDOG1 //watchdog base

class WDOG_sys_manager {
public:

    uint8_t WDOG_thread_add(){
        /*  WARNING: This is a very simple function
         *  only capable of up to 32 tasks that can be added. There is also currently no way to remove tasks.
         *  Both of these functionailites could easily be added should the need arise in the future
         *
         */
        if (WDOG_nof_THREADS >= sizeof(uint32_t) * 8){
            printf("too many tasks to keep track of");
        }

        WDOG_refresh_value |= 1 << WDOG_nof_THREADS;  //add another bit to the value needed for refresh
        WDOG_sys_health    |= 1 << WDOG_nof_THREADS;                 //assume the task is healthy, as the task ahd to call this function after all
        WDOG_nof_THREADS++;            //increment the number of tasks that need to refresh the watchdog
        return WDOG_nof_THREADS-1;     //return a unique number for each task
    }

    //allows each task to refresh themselves using their WDOG number
    void WDOG_task_refresh(uint8_t number){
        WDOG_sys_health |= 1 << number;

        //check if the wdog should be refreshed
        if(WDOG_sys_health == WDOG_refresh_value){
            WDOG_Refresh(WDOG_BASE);    //refresh function
            WDOG_sys_health = 0;   //reset value
        }
    }

private:
    volatile uint32_t WDOG_sys_health = 0;       //only refershes if equel to the refresh value
    uint32_t WDOG_refresh_value = 0;    //each new task sets an extra bit to 1 in this uin32_t
    uint8_t  WDOG_nof_THREADS = 0;       //counter to determine which bit to set
};


//static WDOG_sys_manager WDOG_sys;

#endif /* WDOG_TASK_AWARE_H_ */
