/*
 * Copyright 2020 Stichting Nederlandse Wetenschappelijk Onderzoek Instituten,
 * ASTRON Netherlands Institute for Radio Astronomy
 * Licensed under the Apache License, Version 2.0 (the "License");
 *
 * you may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * See ../LICENSE.txt for more info.
 */

#include "Devices.h"


//#include <string>
//#include <cmath>
//
////allow for debugging
//#include <fsl_device_registers.h>
//#include "fsl_debug_console.h"
//
////import I2C abstraction functions
//extern "C" {
//	#include <common.h>
//}



/*		pmbus sensor implementation
 * 	==========================================
 * */


pmbus_sensor::pmbus_sensor(uint8_t _cmd, uint8_t _format, string _name) {
	cmd = _cmd;
	format = _format;
	name = _name;
}


uint16_t pmbus_sensor::get_measurement(lpi2c_rtos_handle_t *I2C_Handle, uint16_t address, float &datapoint) {

	status_t status;
	uint16_t raw_word = 0;

	//read the sensor
	status = I2C_read_word(I2C_Handle, address, cmd, &raw_word);

	if(status != kStatus_Success){	//if not succesful, return infinity
//		datapoint = NAN;
		return 1;	//increment the failure counter
	}

	//NOTE there appears to be an occasional wrong read, with a value of 0xffff, retry if this happens
	//TODO fix this, as it is possible to get an actual 0xffff
    if(raw_word == 0xffff){
        DELAY
        status = I2C_read_word(I2C_Handle, address, cmd, &raw_word);
        if(status != kStatus_Success || raw_word == 0xffff){  //if not succesful, return infinity
//            datapoint = NAN;
            return 1;   //increment the failure counter
        }
    }


	//check what format the sensor uses
	if (format == LITERAL) {
		datapoint = format_literal(raw_word);
	}
	else if (format == VOUT_MODE) {	//Vout requires reading an extra byte in a specific register

		uint8_t raw_byte = 0;

		DELAY //todo: this fixes the 'output voltage'sometimes not reading correctly, fix this less hacky

		status = I2C_read_byte(I2C_Handle, address, 0x20, &raw_byte);
		if(status != kStatus_Success){

//			datapoint = NAN;
			return 1; //failure count increment;
		}
		datapoint = format_vout(raw_word, raw_byte);
	}
	else if (format == DIRECT) {
		//not used, not implemented
		datapoint = 0;
	}

	return 0;
}

// see the PMBUS specifications for more information about commands and data formats
float pmbus_sensor::format_vout(uint16_t raw_word, uint8_t raw_byte) {
	//note: raw_byte and raw_word are formally called : VOUT_MODE and VOUT_COMMAND
	// lowest 5 bits of raw_byte contain exponent, raw_word is a 16 bit unsigned int

	uint8_t exponent = raw_byte & 0b00011111;  // 5 lowest bits of the raw_byte
	uint16_t mantissa = raw_word;		// this is a 16 bit unsigned integer

//check if the exponent is negative
	if ((exponent & 0b10000) >= 1) {  // if the exponent is negative(MSB == 1)
		exponent = exponent & 0b01111; 	// set sign bit to 0
		exponent = exponent - 16; 	// two's complement
	}
	float result = float(uint16_t(mantissa) * pow(2, int8_t(exponent)));  // measurement = mantissa * 2 ^ exponent

	if(result >= 3.5){
	    __asm("NOP");   //to catch occasional 7.99 value, obviously wrong
	}
	return result;
}
float pmbus_sensor::format_literal(uint16_t raw_word) {
	// raw_word contains exponent and mantissa, 5 highest bits are exponent
	// 11 lowest bits are mantissa, value is calculated as mantissa*2^exponent

	uint16_t mantissa = raw_word & 0b0000011111111111;  // mask upper 5 bits to 0
	uint16_t exponent = raw_word >> 11;  // shift exponent to the rightmost bit

	if ((exponent & 0b10000) >= 1) {  // if the exponent is negative(MSB == 1)
		exponent = exponent & 0b01111;
		exponent = exponent - 16;
	}
	if ((mantissa & 0b10000000000) >= 1) {  // if the mantissa is negative(MSB == 1)
		mantissa = mantissa & 0b01111111111;
		mantissa = mantissa - 1024;

	}
	//variable to evaluate its value before returned
	float result = float(int16_t(mantissa) * pow(2, int8_t(exponent)));


    if(result == -0.5){
        __asm("NOP");   //to catch occasional -0.5 value, obviously wrong
    }
	return result;  // value = mantissa * 2 ^ exponent
}
float pmbus_sensor::format_direct(uint16_t raw_word) {
	// the direct format is currently not used by anything
	return 0.0;
}

/*		Monitored device implementation
 * 	==========================================
 * */
monitored_device::monitored_device(uint8_t device_address, string device_name, uint8_t _nof_sensors, monitored_type _type) {
	address = device_address;
	name = device_name;
	nof_sensors = _nof_sensors;
	type = _type;
}
monitored_device::monitored_device(const monitored_device &old_dev){
    address = old_dev.address;
    name = old_dev.name;
    nof_sensors = old_dev.nof_sensors;
}


string monitored_device::get_name() {
	return name;
}

uint8_t monitored_device::get_address() {
	return address;
}




/*		pmbus device (derived from'monitored_device' implementation
 * 	==========================================
 * */
//WARNING: removed "pmbus_sensor **sensors" from constructor
pmbus_device::pmbus_device(uint8_t _address, string _name, pmbus_sensor **_sensors)
	: monitored_device{ _address, _name, nof_sensors} {
		sensors = _sensors;
}

	//pmbus_device implementation of device readout.
uint16_t pmbus_device::device_readout(lpi2c_rtos_handle_t *I2C_Handle, float *datapoints_pointer) { //go through the device and read all its sensors
	uint16_t failure_cnt = 0;

#if LOGGING_GENERAL == true
	printf( "\t %s:\r\n", name.c_str());
#endif

	for (int i = 0; i < nof_sensors; i++) {
		failure_cnt+= sensors[i]->get_measurement(I2C_Handle, address, datapoints_pointer[i]);
		//buff_cnt++;

#if LOGGING_GENERAL == true
		printf( "\t %s: %f\r\n", sensors[i]->name.c_str(), buffer[buff_cnt-1]); //note: real get_measurement is in pmbus_sensor
#endif
		//printf("%f\r\n",buffer[buff_cnt]);
	}

	return failure_cnt;
}



/*		Static initialisation of all pmbus sensors
 * 	==========================================
 * */
//static pmbus_sensor Vin = pmbus_sensor(0x88, LITERAL, "Input voltage (V)");
//static pmbus_sensor Vout = pmbus_sensor(0x8B, VOUT_MODE, "output voltage (V)");
//static pmbus_sensor Temp = pmbus_sensor(0x8D, LITERAL, "temperature  (C)");
//static pmbus_sensor Iout = pmbus_sensor(0x8C, LITERAL, "output current (A)");
//static pmbus_sensor *sensorss[4] = { &Vin, &Vout, &Temp, &Iout};
//pmbus_sensor *pmbus_device::sensors = &sensorss;

//pmbus_sensor pmbus_device::sensors[nof_sensors] = {	//create all sensors in a pmbus device, static for all pmbus_device objects
//			pmbus_sensor(0x88, LITERAL, "Input voltage (V)"),
//			pmbus_sensor(0x8B, VOUT_MODE, "output voltage (V)"),
//			pmbus_sensor(0x8D, LITERAL, "temperature  (C)"),
//			pmbus_sensor(0x8C, LITERAL, "output current (A)")
//};


/*		device class implementation (base class for non monitored devices
 * 	==========================================
 * */
device::device(uint8_t _address, device_type _type, string _name) {
	name = _name;
	type = _type;
	address = _address;
}

device::device(const device &old_device){
    name = old_device.name;
    type = old_device.type;
    address = old_device.address;
}



I2C_switch::I2C_switch(uint8_t device_address,  string device_name, uint8_t _number)
	: device (device_address, I2C_switch_device, device_name){number = _number;}

//copy constructor
I2C_switch::I2C_switch(const I2C_switch &old_switch){
    name = old_switch.name;
    type = old_switch.type;
    address = old_switch.address;
    current_config = 0;

}


status_t I2C_switch::set_channel(lpi2c_rtos_handle_t *I2C_Handle, uint8_t channel) { //select single channel (0-7 for channels 1-8)
	status_t status;

	if (channel < 8) {
		uint8_t write_data = 1 << channel;

		//DELAY
		status = I2C_write_reg(I2C_Handle, address, &write_data);
//		status = I2C_write_byte(I2C_Handle, address, write_data ,&write_data);

		if(status == kStatus_Success){
			current_config = write_data;
		}
		else{
		    printf("failed setting channels\r\n");
		}
	}
	else {
#if LOGGING_I2C_FAILURE == true
		printf( "invalid channel, turning off all channels \r\n");
#endif
		return set_off(I2C_Handle);
	}
return status;
}


status_t I2C_switch::set_off(lpi2c_rtos_handle_t *I2C_Handle) {
	status_t status;
	uint8_t off = 0x00;
	//0x20241700
	this;
#if LOGGING_GENERAL == true
	printf( "turning off all channels on current switch \r\n");
#endif

	status = I2C_write_reg(I2C_Handle, address, &off);

	if(status == kStatus_Success){
		current_config = off;
	}

#if LOGGING_I2C_FAILURE == true
	else {
		printf("I2C switch not found (set_off)\r\n");
	}
#endif

	return status;
}

status_t I2C_switch::set_broadcast(lpi2c_rtos_handle_t *I2C_Handle) {
	status_t status;
    uint8_t broadcast = 0xFF;

	//	status = I2C_write_byte(I2C_Handle, address, &off);
	status = I2C_write_reg(I2C_Handle, address, &broadcast);
	if(status == kStatus_Success){
		current_config = 0xFF;
	}
	else {
#if LOGGING_I2C_FAILURE == true
		printf("I2C switch unreachable (set_broadcast)\r\n");
#endif
	}
	return status;
}

status_t I2C_switch::set_configuration(lpi2c_rtos_handle_t *I2C_Handle, uint8_t configuration) { //the 8 channels are encoded in a byte (E.g. bit 0 controls channel 0)
	status_t status;

	//status = I2C_write_reg(I2C_Handle, address, &configuration);
	status = I2C_write_reg(I2C_Handle, address, &configuration);
	if(status == kStatus_Success){
		current_config = configuration;
	}
	else {
#if LOGGING_I2C_FAILURE == true
		printf("I2C switch unreachable (set_configuration)\r\n");
#endif
	}
	return status;
}

uint16_t IO_Expander::init(lpi2c_rtos_handle_t *I2C_Handle){
    IO_config(I2C_Handle, 0x0000);  //set all pins as output pins
    return 0;
}


IO_Expander::IO_Expander(uint8_t device_address,  string device_name)
	: device(device_address, IO_expander_device ,device_name) {
		//TODO: set all to output
}


	//configure which pins are inputs and outputs (input = 1, output = 0,  default = 1)
status_t IO_Expander::IO_config(lpi2c_rtos_handle_t *I2C_Handle, uint16_t IO_configuration){
	status_t status;

	status = I2C_write_word(I2C_Handle, address, output_port_0_register, &IO_configuration);
	if(status == kStatus_Success){
		configuration_register = IO_configuration;
#if LOGGING_GENERAL == true
		uint16_t IO;
		printf( "IO-expander configuration set to: ");
		for (int i = 0; i < 16; i++) { //go through all IO's
			IO = configuration_register << (15 - i); //get specific bit
			IO = IO >> 15;
			printf( "%d", IO);
			}
		printf( "\r\n");
#endif
	}
	else {
#if LOGGING_I2C_FAILURE == true
		printf("IO extender unreachable\r\n");
#endif
	}
	return status;
}


//set the values of the output pins (no effect on input pins)
status_t IO_Expander::set_output_values(lpi2c_rtos_handle_t *I2C_Handle, uint16_t out_val) {
	status_t status;

	status = I2C_write_word(I2C_Handle, address, output_port_0_register, &out_val);
	if(status == kStatus_Success){
		output_values = out_val;
//#if LOGGING_GENERAL == true todo: change back
		printf( "set IO expander outputs to: %d \r\n", output_values);
//#endif
	}
	else {
#if LOGGING_I2C_FAILURE == true
		printf("IO extender unreachable\r\n");
#endif
	}

	return status;
}

	//read the value on the input pins
status_t IO_Expander::read_input_values() {
    status_t status = 0;
	uint8_t temp1 = 0, temp2 = 0;
	//according to the data sheet, you have to write to the read-only registers first, before reading them

	//write_byte(address, input_port_0_register, 0x00);					//this is a weird requirement
	//temp1 = uint8_t(read_byte(address, input_port_0_register));		//read the lower byte

	//write_byte(address, input_port_1_register, 0x00);					//this is a weird requirement
	//temp2 = uint8_t(read_byte(address, input_port_1_register));		//read the upper byte

	//if read was succesful
	input_readout = temp1 + (temp2 << 8); //add the lwoer and upper bytes into a uint16_t

#if LOGGING_GENERAL == true
	printf( "IO inputs readout: %X\r\n", input_readout);
#endif

	return status;
}

