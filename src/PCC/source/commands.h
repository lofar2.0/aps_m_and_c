/*
 * Copyright 2020 Stichting Nederlandse Wetenschappelijk Onderzoek Instituten,
 * ASTRON Netherlands Institute for Radio Astronomy
 * Licensed under the Apache License, Version 2.0 (the "License");
 *
 * you may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * See ../LICENSE.txt for more info.
 */



#ifndef COMMANDS_H_
#define COMMANDS_H_


//#include <stdint.h>
extern "C" {

#include "common.h"

#include "open62541.h"
}

#include "pb_encode.h"
#include "pb_decode.h"
#include "RCU2.pb.h"


#include "devices.h"
#include "node.h"

//#include "init_and_util.h"
#include "wdog_task_aware.h"
//#include <string.h>


//Defined because these arguments are required and very large and used for all OPC ua methods. Using a simple define improves readability
#define method_func_inargs       UA_Server *server,                                     \
                                const UA_NodeId *sessionId, void *sessionHandle,        \
                                const UA_NodeId *methodId, void *methodContext,         \
                                const UA_NodeId *objectId, void *objectContext,         \
                                size_t inputSize, const UA_Variant *input,              \
                                size_t outputSize, UA_Variant *output


#define callback_func_inargs    UA_Server *server,                                      \
                                const UA_NodeId *sessionId, void *sessionContext,       \
                                const UA_NodeId *nodeid, void *nodeContext,             \
                                const UA_NumericRange *range, const UA_DataValue *data


#ifndef MAX_I2C_UNITS
#define MAX_I2C_UNITS 4
#endif
#define MAX_CMD_SIZE 32


#define ROM_address    0x50
#define CMD_MAX_LENGTH 20 //give any received commands a max length



#define IGNORE_NODE_STRUCTURE  //node structure changed from hierarchy to flat.

class RCU;
class HBA;


//used by the cmd handler. tells how each RCU unit is configured
struct RCU_config{
    uint16_t                datasize;//size of all other data
    uint16_t                version;   //version number
    uint16_t                number;    //RCU number. ensures RCU's can be connnected out of order
    void*                   RCU_data;  //info about the RCU
};


//NOTE: assumed to be static after compilation
struct I2C_config{
    base_node              *base_ptr;     //pointer to the I2C unit
    uint8_t                 nof_RCUs;       //number of RCU's
    uint8_t                 RCU_num_offset;  //starting RCU number, set in order of initialisation
    RCU                    *RCUs;           //contains the configuration for all of the RCU's
    uint8_t                 nof_switches;
    I2C_switch             *I2C_switches;  //pointer to any I2C switches this might contain
    TaskHandle_t           *taskhandle;
};


struct Vtable_entry {
    uint64_t                    id;     //ID of the command
    char*                       name;   //name of the register

    //device address / register
    uint32_t                    addr;
    uint32_t                    reg;
    //data width bits
    bool                        has_width;
    uint32_t                    width;
    //RCU device type
    bool                        has_d_type;
    device_register_device_type d_type;

    //stores the current value
    bool                        has_value;
    uint32_t                    value;
    //bit offset
    bool                        has_bitoffset;
    uint32_t                    bitoffset;
    //scaling factor
    bool                        has_scale;
    float                       scale;
    //minimum value
    bool                        has_minimum;
    int32_t                     minimum;
    //maximum value
    bool                        has_maximum;
    int32_t                     maximum;
    //RW attributes
    bool                        has_RW;
    variable_ReadWriteType      RW;

    //nodes that can be used for reading or setting the value
    UA_NodeId                   control_point_nodeID;
    UA_NodeId                   monitor_point_nodeID;
};

typedef struct _SPI {
    Vtable_entry CLK;
    Vtable_entry SDO;
    Vtable_entry SDI;
    Vtable_entry DIR;
    bool has_CS;
    Vtable_entry CS;

    UA_NodeId                   control_point_nodeID;
    UA_NodeId                   monitor_point_nodeID;
} SPI;



//struct cmd{ //commands that need to be propogated to the HBA's
//    char        cmd_type[4];
//    uint8_t     data_size;  //the size of "cmd_data" in bytes
//    uint8_t      RCU_num;    //the RCU to address 1-32 (0 is broadcast)
//    int8_t      HBA_num;    //the RCU to address 1-16 (0 is broadcast, < 0 is not HBA)
//    UA_String       name;    //name of the command (usually a register)
//    uint8_t*    cmd_data;   //data of the command
//
//    //example:      { .type =1, .RCU_num =0, .HBA_num =0, .data_size =2, .command ="IO_extender"      }
//};
struct cmd{ //commands that need to be propogated to the HBA's
    char            cmd_type[4];
    uint8_t         RCU_num;    //the RCU to address 1-32 (0 is broadcast)
    int16_t         HBA_num;    //the RCU to address 1-16 (0 is broadcast, < 0 is not HBA)
    uint32_t        id;         //ID of the command
    uint64_t        data;

    //example:      { .type =1, .RCU_num =0, .HBA_num =0, .data_size =2, .command ="IO_extender"      }
};

struct Register_values{
  uint8_t addr;
  uint8_t reg;
  uint8_t value;
};



class cmd_handler{
public:

    const char *command_codes[5] = {"RCU", "HBA", "UNB", "SYN", "OTH"};

    //if there is a known upcoming deadline, set to true
    bool active_deadline = false;
    UA_DateTime next_deadline;
    uint16_t lofar_idx = 2;

    QueueHandle_t cmd_queue = nullptr;
    static cmd_handler *self;  //several static functions required. Workaround since there will only be 1 cmd_handler
    static uint32_t nodeID_cnt;
    uint8_t total_switches; //counts the amount of I2C switches there are

    uint8_t     total_RCUs = 0;

    uint8_t     nof_registered_nodes = 0;
    I2C_config I2C_configs[MAX_I2C_UNITS];        //information about how the I2C unit is configured

    UA_Server   *server;            //and server pointer


    UA_NodeId   cmd_handler_nodeID,     //base node for the command handler
                status_nodeID,      //contains current status info about the command handler
                info_nodeID,        //contains statuc info about the command handler
                deadline_nodeID, previous_deadline_nodeID,    //contains the time for the upcoming deadline and the previous one
                flush_nodeID, interval_nodeID,
                RCU_nodes_nodeID;              //node containing all the underlaying RCUs


    cmd_handler(UA_Server *_server, uint16_t idx);
    void add_base_node(base_node *base_ptr, I2C_switch *I2Cswitch, uint8_t nof_RCUs=8, TaskHandle_t *taskhandle=nullptr);
    void get_RCU_config(I2C_config &config);  //go through all the RCU's. read the rom and get the data out of that

    void check_commands();  //checks if there are any commands in the queue and attempts to excecute them
    void sync_loop();       //checks if there is an active deadline and will a bit before the deadline block the entire I2C bus from any other traffic
    void do_sync();         //the actual sync command


    status_t readout_loop();

    status_t process_RCU_cmd(cmd *cmd); //temp

    void add_OPCua_stuff();
    void new_deadline(UA_DateTime dateTime);

    //void add_OPCua_func();
    bool virtual_switch(I2C_config *config, int8_t channel, bool includes_offset=true, bool force_set=false);

    static void new_deadline_unix(callback_func_inargs);         //client function to set a new deadline
    static void set_RCU_read_interval(callback_func_inargs);     //adds a periodic RCU read command
    static void flush_cmd_queue(callback_func_inargs);           //flushes commands from the command queue


    static void set_deadline(callback_func_inargs);
    static void read_point(callback_func_inargs);
    static void afterWrite(callback_func_inargs);

    void cmd_handler_task();
    static void task_wrapper(void *args);    //freeRTOS doesnt like me using non-static member functions, Using wrapper

    void sync();
};


class RCU {
public:
    uint16_t    version;        //version number
    I2C_config  *config;
    cmd_handler *handler;
    //UA_NodeId   RCU_nodeID;
    uint8_t RCUnum; //rcunum of the base node
    string RCU_name;

    static uint8_t RCU_cnt;
    static const uint8_t max_table_size = 32;
    static const uint8_t max_name_size = 32;

    uint8_t variable_table_entries = 0; //counter, increments for each entry
    Vtable_entry variable_table[max_table_size];

    uint8_t SPI_bridge_table_entries = 0; //counter, increments for each entry
    SPI SPI_bridge_table[max_table_size];

    uint8_t Mode_table_entries = 0; //counter, increments for each entry
    uint8_t Mode_number = 0;
    device_register Mode_table[max_table_size]; //contains the entries
    uint8_t     Mode_index[max_table_size];
    uint8_t Mode_values[max_table_size][8];
    uint8_t Mode_value_len[max_table_size];
    Mode      mode_table_entry_mode[max_table_size]; // contains the mdoe of the entry
    UA_String Mode_names[max_table_size];


    uint8_t ant_table_entries = 0; //counter, increments for each entry
    Ant_types ant_table[max_table_size];

    RCU(I2C_config *_config, cmd_handler *_handler, uint8_t _RCUnum);


    status_t compose_command(cmd *cmd, Vtable_entry *entry, uint64_t &value);
    status_t set_mode(bool set_switches=false,  _Mode_Modes Mode=Mode_Modes_Init);
    status_t blink_led();
    void add_OPC_ua_method();
    void sync_vtable_registers(Vtable_entry *entry, uint64_t value);

    static bool variable_decode_pb(pb_istream_t *istream, const pb_field_t *field, void **arg);
    static bool mode_decode_pb(pb_istream_t *istream, const pb_field_t *field, void **arg);
    static bool spi_decode_pb(pb_istream_t *istream, const pb_field_t *field, void **arg);
    static bool input_decode_pb(pb_istream_t *istream, const pb_field_t *field, void **arg);
    static bool device_decode_pb(pb_istream_t *istream, const pb_field_t *field, void **arg);
    static bool name_decode_pb(pb_istream_t *istream, const pb_field_t *field, void **arg);
    static bool name2_decode_pb(pb_istream_t *istream, const pb_field_t *field, void **arg);


    // NOTE: below copied from paulus
    void LoadPb(uint16_t data_size,const uint8_t *test_pb);

    uint8_t get_stored_i2cregister(Vtable_entry device);
    void set_stored_i2cregister(Vtable_entry device,uint8_t value);

    uint8_t register_values_entries = 0;
    Register_values register_values_table[max_table_size];

    void set_register( Vtable_entry device, uint8_t *bytes,uint8_t length);
    uint16_t get_register( Vtable_entry device);
    void set_I2Cregister( Vtable_entry device, uint8_t *bytes,uint8_t length);
    uint16_t get_I2Cregister( Vtable_entry device);
    void set_SPIregister( Vtable_entry device, uint8_t *bytes,uint8_t length);
    uint16_t get_SPIregister( Vtable_entry device);

    void set_variable(uint8_t number,uint8_t *bytes,uint8_t length);
    uint16_t get_variable(uint8_t number);
    void set_method(uint8_t number);

    uint16_t i2c_writecount=0;  //For debugging
    uint16_t i2c_readcount=0;  //For debugging



};

class HBA{
public:

};


#endif /* COMMANDS_H_ */
