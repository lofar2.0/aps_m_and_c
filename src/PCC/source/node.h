/*
 * Copyright 2020 Stichting Nederlandse Wetenschappelijk Onderzoek Instituten,
 * ASTRON Netherlands Institute for Radio Astronomy
 * Licensed under the Apache License, Version 2.0 (the "License");
 *
 * you may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * See ../LICENSE.txt for more info.
 */


#ifndef NODE_H_
#define NODE_H_




//#include <iostream>

#include <string>
#include <cmath>

//#include "fsl_common.h"
//#include "peripherals.h"

#include <fsl_device_registers.h>
#include "fsl_debug_console.h"

//#include "fsl_lpi2c.h"
//#include "fsl_lpi2c_freertos.h"

extern "C" {
	#include <common.h>
    #include "open62541.h"
}


#include "devices.h"

enum type_of_device : bool {
    monitored_type = true,
    unmonitored_type = false
};


class base_node;

class node {
public:

	node(monitored_device 	**monitored_ptr_arr = nullptr, 	uint8_t _nof_monitored=0,	// **monitored_ptr_arr: pointer to pointer array of all monitored devices,
		 node 				**subnodes_ptr_arr=nullptr, 	uint8_t _nof_subnodes=0,	// **subnodes_ptr_arr: pointer to pointer array of all subnodes
		 device 			**devices_ptr_arr=nullptr, 		uint8_t _nof_devices=0,		// **devices_ptr_arr: pointer to pointer array of all non-monitored devices
		 string 			_name = "node",
		 int8_t 			_switch_channel = NONE, int8_t _switch_nr = NONE);			//, I2C_switch *_switcher = nullptr)	// data about how to reach this node and where it is located

    node(const node &old_node);

    uint16_t readout_node(lpi2c_rtos_handle_t *I2C_Handle, uint16_t &datapoint_cnt, base_node *base_ptr);   //read all the sensors
    uint16_t init(lpi2c_rtos_handle_t *I2C_Handle, uint16_t next_num);    //goes through the tree calling the init function for each (monitored) device

    //todo: compact all functions that are only used once into a general get_tree_info() function
    void get_tree_info(uint16_t &_nof_datapoints, uint16_t &_nof_devices, uint16_t &_nof_monitored, uint16_t &_nof_nodes, uint16_t &_nof_switched_nodes, uint16_t &_nof_switches);
	uint16_t get_nof_datapoints();  //get the number of datapoints in this node and the subnodes
	uint16_t get_nof_monitored();       //get the number of total monitored devices'
	void store_switches(base_node *base, uint8_t &switch_count);  //locate the switches and put them in a pointer array

	base_node *get_base_node();       //travels up the tree to the base node

	uint16_t add_OPCua(base_node *base_ptr, UA_NodeId UA_parentNode );    //creates OPC ua variable and object nodes for each sensor
	static void update_OPCua(UA_Server *server, uint16_t &datapoint_cnt, float value);        //called by Read_node, sets the new values in OPC ua

	node* duplicate_node(node *original_node);             //returns a pointer to a node. All subnodes are also copied. NOTE Devices arent copied
	node** duplicate_group(node **subnodes, uint8_t nof_subnodes);  //duplicates a group of nodes

	string name;
	uint16_t node_ID_num = 0;   //gives each node a unique identification

	int8_t switch_nr;		//tells to which switch of the previous node this node is connected to (NONE means previous node doesn't have a switch)
	int8_t switch_channel;	//to which channel of the previous switch this node is connected to

	I2C_switch *switcher = nullptr;	//points to the switch object of a node
	// limited to 1 switch currently, see devices/subnodes on how to change if neccecary

	uint8_t nof_subnodes;			// for keeping track how many subnodes this node has
	node ** subnodes = nullptr;				//pointer to pointer array to all subnodes

	uint8_t nof_monitored;			// for keeping track how many devices this node has
	monitored_device ** monitored = nullptr;	//pointer to pointer array to all monitored devices

	uint8_t nof_devices;			// for keeping track how many devices this node has
	device ** devices = nullptr;				//pointer to pointer array to all devices

	uint16_t nof_datapoints = 0;        //how many datapoints this node stores (relates to the number of monitored devices the node has)
	float *datapoints = nullptr;   // array for storing these values

	node* parent_node = nullptr;    //pointer to parent node

	UA_NodeId nodeID;   //reference to this node in OPC ua
};





//allows the implementation of functions a base node is interested in and allowing for easier overview of all subnodes
class base_node : public node {

public:
	base_node(lpi2c_rtos_handle_t *_I2C_Handle, UA_Server *mUaServer, uint8_t _I2C_num, uint16_t idx, string _name = "base_node");
//
//	static cmd_handler *handler;
	const bool logging = false;     //if false, disables error logging


	lpi2c_rtos_handle_t *I2C_Handle;	//reference to the I2C unit used
	QueueHandle_t queue = nullptr;
	UA_Server *server;  //pointer to server
    uint16_t nodeID_offset = 0; //base offset for figuring out the nodeID of a base node
    uint8_t I2C_unit_nr = 0;
    uint16_t lofar_idx = 1;

    TaskHandle_t taskhandle = xTaskGetCurrentTaskHandle();
    bool cmd_busy_flag = false; //gets set by the command handler. if true dont start any new I2C commands
    bool ack_cmd_flag = false;
    bool sleep_flag = false; //when all sensors are read, this flag gets set high

//    cmd_handler handler;

	//these variables contain a bunch of information about the base node what it contains
	uint16_t total_datapoints = 0, total_monitored = 0, total_devices = 0, total_switched_nodes = 0, total_switches = 0, total_nodes = 1;

	I2C_switch **switch_ptr_arr = nullptr;   //pointer to a pointer array to all switches
	uint8_t *switch_states = nullptr;       //pointer to an array that contains the state of all switches before a command started being processed


	void update_buffer();	//reads out all the sensors

	uint32_t next_cycle = pdMS_TO_TICKS(xTaskGetTickCount()) + 10;
	void wait_remainder();

    // I2C problem monitoring
    //------------------------------------------------------------------------------
    //various nodeID's for I2C monitoring
    UA_NodeId   base_trackerID, monitored_trackerID, switch_trackerID,                    //nodeID's for the tracking arrays (counts failures, byte per device)
                monitored_failure_countID, switch_failure_countID, skip_countID,          //counts total failures per readout per type
                readout_loopID, readout_timeID;

    //array with a byte for each (+monitored)device , tracks a number of past failures NOTE: could also be changed into multidimensional array for extra info
    uint8_t *monitored_failure_array = nullptr;   //contains an array of data indicating that a particular device failed to be read. value indicates how many of the previous 256 reads failed
    uint8_t *switch_failure_array = nullptr;
    uint8_t I2C_publish_cnt = 0;
    uint16_t switch_counter = 0, monitored_counter = 0;     //counter for keeping track which (monitored) device has just been used
    uint16_t monitored_failure_cnt = 0, switch_failure_cnt = 0; //raw count of a readout


    /*  I2C problem tracking functions
     *  has 4 steps:
     *  1. I2C_monitor_init() initialisation,  only happens during object instatiation
     *  2. I2C_log_failure(...) failure logging  happens each read/write that fails
     *  3. process_I2C_logs() process logs. called after each base node readout, update the raw failure counts per type
     *  4  process_I2C_logs() once enough cycles have passed, publishes failure count for each device
    */
    void I2C_monitor_init();    //initialises the I2C error logger
    void I2C_log_failure(type_of_device type);   //logs the failuers into an array
    void process_I2C_logs();  //processes the logs

};



#endif /* NODE_H_ */


