/*
 * Copyright 2020 Stichting Nederlandse Wetenschappelijk Onderzoek Instituten,
 * ASTRON Netherlands Institute for Radio Astronomy
 * Licensed under the Apache License, Version 2.0 (the "License");
 *
 * you may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * See ../LICENSE.txt for more info.
 */

#define APPLICATION_URI_SERVER "https://lofar.eu"

#if defined(__cplusplus)
extern "C" {
#endif /* __cplusplus */

///* FreeRTOS includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "timers.h"
#include "semphr.h"

    //OPC ua - Open62541 amalgamated source file
#include "open62541.h"

    //LWIP default settings, see source/lwipopt.h for used settings
#include "lwip/opt.h"

    //LWIP
#include "lwip/netifapi.h"
#include "lwip/tcpip.h"
#include "netif/ethernet.h"
#include "lwip/prot/dhcp.h"
#include "lwip/sys.h"
#include "lwip/ip_addr.h"


//Ethernet driver
#include "enet_ethernetif.h"

/* Freescale includes. (most are partially generated through MCUxpresso SDK)*/
#include "fsl_device_registers.h"
#include "fsl_debug_console.h"

//#include "system_MIMXRT1062.h"
#include "board.h"
#include "peripherals.h"
#include "pin_mux.h"                //generated based on configuration
#include "clock_config.h"           //generated based on configuration
#include "fsl_gpio.h"
#include "fsl_lpi2c.h"
#include "fsl_iomuxc.h"
#include "fsl_semc.h"       //external memory controller


#include <cr_section_macros.h>


#include "common.h" //easier to use I2C function on top of the RTOS transfer function
#include "OPCua_debug_redirect.h" //redirects debug messages (printf) through OPC ua by implementing _write()
//#include "OPCua_methods.h"


#if defined(__cplusplus)
}
#endif /* __cplusplus */


#include "wdog_task_aware.h"
#include "base_nodes_config.h"      //tells how the APS is wired

#include "node.h"
#include "commands.h"
#include "init_and_util.h"   //all initialisation and utility functions to clear up this file




#include <string>


using namespace std;


struct I2C_manager_args {
    UA_Server *server;
    lpi2c_rtos_handle_t *I2C_handle;
    uint8_t I2C_unit_nr;
    cmd_handler *handler;
    uint16_t idx;

};

QueueHandle_t   I2C_command_queue     = NULL; //sends the command to the I2C unit managers
//QueueHandle_t   I2C_monitor_queue     = NULL; //sends the reads to the I2C unit managers

/*******************************************************************************
 * Definitions
 ******************************************************************************/
#define TASK_PRIO           (configMAX_PRIORITIES - 2)      //the priority of the main tasks, all have the second highest priority
#define NOF_I2C_UNITS       4       //there are a total of 4 I2C units and 2 additional flexio's available to use for I2C, only the I2C units are used
#define COMMAND_QUEUE_SIZE  ( 16 * 1024 ) / sizeof(cmd) //reserve 32 kB for the qeueu
#define MONITOR_QUEUE_SIZE  ( 32 * 1024 ) / sizeof(cmd) //reserve 32 kB for the qeueu

/* MAC address configuration. */
#define configMAC_ADDR  { 0x02, 0x12, 0x13, 0x10, 0x15, 0x11 }  //TODO: embedded artist has a small file that contains the actual mac address
#define ENET_PHY_ADDRESS BOARD_ENET0_PHY_ADDRESS /* Address of PHY interface. */
#define NETIF_INIT_FN ethernetif0_init //(ethernet) network interface initialisation function

//I2C monitor task config
#define I2C_monitor_task_prio           16
#define I2C_monitor_stacksize           32*1024
//I2C control task config
#define I2C_control_task_prio           12
#define I2C_control_task_stacksize      32*1024
//server manager task config
#define server_manager_task_prio        14
#define server_manager_task_stacksize   8*1024
//OPC ua task config
#define OPCua_task_prio                 15
#define OPCua_task_stacksize            8*1024
//LWIP task config
#define LWIP_task_prio                  15
#define LWIP_task_stacksize             2*1024


//OPC ua config
#define PORT                            50000
#define DEFAULT_NAMESPACE               "https://lofar.eu"           //default namespace used by created objects in teh OPC ua server

/*******************************************************************************
 * Prototypes
 ******************************************************************************/

static void I2C_manager_task(void *arg);       //manages all devices on I2C4 unit

static void lwip_init(void *arg);               //this thread initialises the lwip stack
static void opcua_thread(void *arg);            //initialises server and runs it in the background
static void server_manager_thread(void *arg);   //fills and maintains the servers address space

static void status_node(UA_Server *server, int16_t idx);     //adds and updates a special node that tracks some system information
//static void add_Methods(UA_Server *server);
/*******************************************************************************
 * Globals
 ******************************************************************************/
//create a pointer array to the various I2C rtos handles
#define MAX_I2C_UNITS 4
lpi2c_rtos_handle_t *I2C_handle[NOF_I2C_UNITS] = {&LPI2C1_masterHandle, &LPI2C2_masterHandle, &LPI2C3_masterHandle, &LPI2C4_masterHandle};



/*******************************************************************************
 * Code
 ******************************************************************************/

int main(void)
{
    //pull up for I2C2, these pins are required to be low during reset, otherwise the device will boot wrong
    //here, after reset, the pins can be pulled up. The I2C2 pins are connected to this pin through 2.2k resistors on each pin
    //GPIO_PinWrite(GPIO1, 27U, 1U);    //tp49

    //initialise microcontroller components
    BOARD_ConfigMPU();
    BOARD_InitPins();
    BOARD_BootClockRUN();
    BOARD_InitSEMC();//initialise the external memory
    RTOS_heap_init();               //configure the heap for freeRTOS heap_5
    BOARD_InitDebugConsole();
    BOARD_Init_ETH_peripheral();    //intialise part of the Ethernet pins and clocks
    BOARD_InitBootPeripherals();    //initialise all peripherals

//    while(1){
//        delay();
//    }


    //not used yet. probably needed later
//    lpi2c_rtos_handle_t FLEXI2C2_masterHandle;
//    lpi2c_rtos_handle_t FLEXI2C3_masterHandle;

    //LPI2C_RTOS_Init(&LPI2C4_masterHandle, LPI2C4_PERIPHERAL, &LPI2C4_masterConfig, LPI2C4_CLOCK_FREQ);


    //Freertos reserves highest priority for its own use, set interrupt priority lower.
    NVIC_SetPriority(LPI2C1_IRQn, 2);
    NVIC_SetPriority(LPI2C2_IRQn, 2);
    NVIC_SetPriority(LPI2C3_IRQn, 2);
    NVIC_SetPriority(LPI2C4_IRQn, 2);

    NVIC_SetPriority(GPIO1_Combined_0_15_IRQn, 2);
    NVIC_SetPriority(TRNG_IRQn, 2);
//    NVIC_SetPriority(FLEXIO1_IRQn, 2);
//    NVIC_SetPriority(FLEXIO3_IRQn, 2);


    //create a queue the size of COMMAND_QUEUE_SIZE. each element the size of a pointer to a data
    I2C_command_queue = xQueueCreate( COMMAND_QUEUE_SIZE, sizeof(cmd));
//    I2C_monitor_queue = xQueueCreate( MONITOR_QUEUE_SIZE, sizeof(cmd));
    printf("queue is %d deep and has a total size of %d kB\r\n", COMMAND_QUEUE_SIZE, ( COMMAND_QUEUE_SIZE * sizeof(cmd)) /1000);


    //initialise LWIP stack
    if (sys_thread_new("lwip_init", lwip_init, NULL, LWIP_task_stacksize, LWIP_task_prio) == NULL){
        LWIP_ASSERT("lwip_init(): Task creation failed.", 0);
    }

    /* Start scheduling. */
    vTaskStartScheduler();


    //should never reach this unless the task scheduler was stopped
    for(;;){
        printf("Oh no, someone stopped the task scheduler.");
    }
}

static void lwip_init(void *arg) {


    static struct netif netif;
    ip4_addr_t netif_ipaddr, netif_netmask, netif_gw;
    ethernetif_config_t enet_config = { .phyAddress = ENET_PHY_ADDRESS,
                                        .clockName = CoreSysClk,
                                        .macAddress = configMAC_ADDR,
    };

    //TODO: manually added the addresses here, this will not do for the final product
    uint8_t *netif_ip_address = get_ip_address(netif_ip);
    uint8_t *netmask_ip_address = get_ip_address(netmask_ip);
    uint8_t *gateway_ip_address = get_ip_address(gateway_ip);

    IP4_ADDR(&netif_ipaddr,     netif_ip_address[0],    netif_ip_address[1],    netif_ip_address[2],    netif_ip_address[3]);
    IP4_ADDR(&netif_netmask,    netmask_ip_address[0],  netmask_ip_address[1],  netmask_ip_address[2],  netmask_ip_address[3]);
    IP4_ADDR(&netif_gw,         gateway_ip_address[0],  gateway_ip_address[1],  gateway_ip_address[2],  gateway_ip_address[3]);

    //initialise and start the tcpip thread
    tcpip_init(NULL, NULL);

    //add and set up the network interface
    netif_add(&netif, &netif_ipaddr, &netif_netmask, &netif_gw, &enet_config, NETIF_INIT_FN, tcpip_input);
    netif_set_default(&netif);
    netif_set_up(&netif);

    //print a  debug message
    printf("ETH MAC addr: %X:%X:%X:%X:%X:%X\r\n", enet_config.macAddress[0],
            enet_config.macAddress[1], enet_config.macAddress[2],
            enet_config.macAddress[3], enet_config.macAddress[4],
            enet_config.macAddress[5]);
    printf(" IPv4 Address     : %u.%u.%u.%u\r\n", ((u8_t*) &netif_ipaddr)[0],
            ((u8_t*) &netif_ipaddr)[1], ((u8_t*) &netif_ipaddr)[2],
            ((u8_t*) &netif_ipaddr)[3]);
    printf(" IPv4 Subnet mask : %u.%u.%u.%u\r\n", ((u8_t*) &netif_netmask)[0],
            ((u8_t*) &netif_netmask)[1], ((u8_t*) &netif_netmask)[2],
            ((u8_t*) &netif_netmask)[3]);
    printf(" IPv4 Gateway     : %u.%u.%u.%u\r\n", ((u8_t*) &netif_gw)[0],
            ((u8_t*) &netif_gw)[1], ((u8_t*) &netif_gw)[2],
            ((u8_t*) &netif_gw)[3]);
    printf("************************************************\r\n");

    //start the OPC ua server
    vTaskDelay(20);

    if (NULL == sys_thread_new("opcua_thread", opcua_thread, NULL, OPCua_task_stacksize, OPCua_task_prio)) { //20000 //16384
        LWIP_ASSERT("opcua(): Task creation failed.", 0);
    }

    //delete this task
    vTaskDelay(20);
    vTaskDelete(NULL);
}

static void opcua_thread(void *arg) {

    UA_StatusCode retval; //contains function status code for Open62541 functions

    UA_UInt16 portNumber = PORT;

    //create the server and get a default config
    UA_Server *mUaServer = UA_Server_new();
    UA_ServerConfig *uaServerConfig = UA_Server_getConfig(mUaServer);
    UA_ServerConfig_setMinimal(uaServerConfig, portNumber, 0);

//    //set custom buffers, since default 64k is pretty high
//    uaServerConfig->networkLayers->localConnectionConfig.recvBufferSize = 65536;
//    uaServerConfig->networkLayers->localConnectionConfig.sendBufferSize = 65536;

    //VERY IMPORTANT: Set the hostname with your IP before allocating the server
    char ip4_string[17];
    uint8_t *ip_address = get_ip_address();

    sprintf(ip4_string, "%u.%u.%u.%u", ip_address[0], ip_address[1], ip_address[2], ip_address[3]);
    UA_ServerConfig_setCustomHostname(uaServerConfig, UA_STRING(ip4_string));

    //start the thread to fill the server address space and manage it
    if (sys_thread_new("server_manager", server_manager_thread, mUaServer, server_manager_task_stacksize, server_manager_task_prio) == NULL){
        LWIP_ASSERT("server_manager(): Task creation failed.", 0);
    }

    UA_Boolean running = true;  //if set to false will stop the server

    //start the server
    retval = UA_Server_run(mUaServer, &running);
    UA_Server_delete(mUaServer);

    if (retval != 0) {
        //gets here whenever something unexpected happens. The server should not shut down
        printf("Server shut down unexpectedly\r\n");
    }

//    vTaskDelay(2000);
    vTaskDelete(NULL);
}

static void timewaster_task(void *arg){
/*  the idle task can sometimes behave oddly. This is a hacky workaround
 * Most likely reason is an incorrect configuration regarding interrupt priorites or task switching behaviour
 *  the task has a very low priority of 1
 */

    while(1){
        __asm("NOP"); /* delay */
        vTaskDelay(0);
    }
}

/*
the server manager thread first fills the servers address space with methods, objects and variables
it then, in a loop, continuously reads an I2C sensor and updates the server value with the new value
*/
static void server_manager_thread(void *arg){

    UA_Server* mUaServer = (UA_Server*)arg;
//    const uint8_t WDOG_task_number = WDOG_sys.WDOG_thread_add(); //add this task to the WDOG_sys_manager

    //char namespaceName = DEFAULT_NAMESPACE;
    uint16_t LOFAR_namespace_idx = UA_Server_addNamespace(mUaServer, DEFAULT_NAMESPACE);  //adds namespace 2. The namespcae used for the device manager
    printf("namespace: %s has index %d\r\n", DEFAULT_NAMESPACE, LOFAR_namespace_idx);

    debug_msg_buff_init(mUaServer); //when newlib is in nohost will write debug messages to OPC ua instead

    // Currently some issues with task not being available. this is a dummy task with a very low priority to work around that
    TaskHandle_t timewaster = sys_thread_new("timewaster_task", timewaster_task, NULL, 128, 13);

    //create the commadn handler object
    cmd_handler cmd_handler_obj = cmd_handler(mUaServer, LOFAR_namespace_idx);  //creates an object and a task

    //set a pointer to the command handler for the sync interrupt to get
    set_get_cmd_handler_ptr(&cmd_handler_obj);
    cmd_handler_obj.cmd_queue = I2C_command_queue;

    //create the command handler task
    TaskHandle_t cmd_task_handler = sys_thread_new("cmd_handler_task", cmd_handler_obj.task_wrapper, &cmd_handler_obj, I2C_control_task_stacksize, I2C_control_task_prio);
    if (cmd_task_handler == NULL) LWIP_ASSERT("cmd_handler_task(): Task creation failed.", 0);

    //struct of input args for I2C manager tasks
    struct I2C_manager_args I2C_args = {.server =  mUaServer, .I2C_handle = &LPI2C1_masterHandle, .I2C_unit_nr = 1, .handler = &cmd_handler_obj, .idx = LOFAR_namespace_idx};

    //start I2C manager
    TaskHandle_t I2C_thread = sys_thread_new("I2C_manager_init", I2C_manager_task, (void*)&I2C_args, I2C_monitor_stacksize, I2C_monitor_task_prio);
    if (I2C_thread == NULL){
        LWIP_ASSERT("I2C_manager_init(): Task creation failed.", 0);
    }

    /*
     * NOTE: all I2 unit tasks can be here
     *
     * I2C1_manager_task
     * I2C2_manager_task
     * I2C3_manager_task
     * I2C4_manager_task
     */

    vTaskPrioritySet( timewaster, 1);
    vTaskDelay(pdMS_TO_TICKS(1000));    //wait for 1 second
//    vTaskPrioritySet( cmd_task_handler, I2C_control_task_prio + 1 );
//    vTaskPrioritySet( I2C_thread, I2C_monitor_task_prio - 1 );

    for(;;){
        status_node(mUaServer, LOFAR_namespace_idx); //update (or init) status node
        blinky();   //toggle LED
        printf("...\r\n");

//        WDOG_sys.WDOG_task_refresh(WDOG_task_number);    //refresh this task

        vTaskDelay(pdMS_TO_TICKS(1000));    //wait for 1 second
    }
}

static void I2C_manager_task(void *arg){
    I2C_manager_args *args = (I2C_manager_args*)arg;                            //pointer to the OPC ua server

    printf("initialising I2C_unit base node\r\n");

    TEST_base_node I2C_unit(args->I2C_handle, args->server, args->I2C_unit_nr, args->idx, args->handler, "I2C_unit");   //create the base node object


    //holds the time the next readout interval is supposed to start
//    uint32_t next_time = pdMS_TO_TICKS(xTaskGetTickCount()) + 1;
//    wait_remainder(next_time);

//    const uint8_t WDOG_task_number = WDOG_sys.WDOG_thread_add();    //add this task to the WDOG_sys_manager

    for(;;){
        I2C_unit.update_buffer();                       //read out all sensors and update OPC ua
//        WDOG_sys.WDOG_task_refresh(WDOG_task_number);   //refresh this task

//        wait_remainder(next_time);
    }
}


//status node contains various bits of info about the device
void status_node(UA_Server *server,  int16_t idx){
    static bool init = false;

    //general system status nodes
    static UA_NodeId temperature_nodeID, available_heap_nodeID, runtime_nodeID;

    //general system status Variants
    static UA_Variant   *temperature_var, *available_heap_var, *runtime_var;

    float temperature = TEMPMON_GetCurrentTemperature(TEMPMON); //uses internal temperature monitor. base address used as input argument
    uint32_t available_heap  = xPortGetFreeHeapSize();  //returns the available heap
    UA_String runtime =  UA_STRING(get_readable_runtime());

    UA_StatusCode status;

    if(init == false){

        UA_NodeId compile_time_nodeID, compile_date_nodeID, name_nodeID, interval_nodeID, runtime_uid_nodeID;


        //NOTE: UIDs are generated at runtime because im not quite sure how to get the mcu specific GUID or hoow important it is to get
        uint32_t UID[4];
        TRNG_GetRandomData(TRNG, UID, sizeof(UID));
        char UID_string[35];    //128bit is 16 bytes or 32 characters + \0
        sprintf(UID_string, "%08lx%08lx%08lx%08lx", UID[0], UID[1], UID[2], UID[3]);

        UA_String runtime_UID    = UA_STRING(UID_string);
        UA_String compile_time   = UA_STRING(__TIME__);
        UA_String compile_date   = UA_STRING(__DATE__);
        UA_String device_name    = UA_STRING("MANDC_PCC_TEST");  //beautiful name I know
        UA_Float  readout_interval = 1;

        volatile opc_ua_time OPCua_time = opc_ua_time(server);

//        //--------------------------------------------------------------------------------
//        //add status node
//        UA_ObjectAttributes oAttr = UA_ObjectAttributes_default;
//        status_nodeID = UA_NODEID_STRING(1, "Monitor_and_Control_status");
//        oAttr.description = UA_LOCALIZEDTEXT("en-US", "The node that contains various bits of information about the server");
//        status = UA_Server_addObjectNode(server, UA_NODEID_NULL,
//                                UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER),
//                                UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
//                                UA_QUALIFIEDNAME(1, "Monitor_and_Control_status"),
//                                UA_NODEID_NUMERIC(0, UA_NS0ID_BASEOBJECTTYPE),
//                                oAttr, NULL, &status_nodeID);
//        if(status) printf("couldn't add base status node\r\n");
        //--------------------------------------------------------------------------------
        //add internal temperature node
        UA_VariableAttributes vAttr = UA_VariableAttributes_default;
        temperature_nodeID = UA_NODEID_STRING(idx, "PCC_internal_temperature");
        vAttr.description = UA_LOCALIZEDTEXT("en-US", "The internal temperature of the microcontroller in degrees celsius(C)");
        UA_Variant_setScalar(&vAttr.value, &temperature, &UA_TYPES[UA_TYPES_FLOAT]);
        status = UA_Server_addVariableNode(server, UA_NODEID_NULL,   //UA_NODEID_NULL
                            UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER),
                            UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                            UA_QUALIFIEDNAME(idx, "PCC_internal_temperature(C)"),
                            UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                            vAttr, NULL, &temperature_nodeID);
        if(status) printf("couldn't add temperature node\r\n");

        //--------------------------------------------------------------------------------
        //add available heap node
        vAttr = UA_VariableAttributes_default;
        vAttr.description = UA_LOCALIZEDTEXT("en-US", "The amount of free heap memory in bytes");
        UA_Variant_setScalar(&vAttr.value, &available_heap, &UA_TYPES[UA_TYPES_UINT32]);
        available_heap_nodeID = UA_NODEID_STRING(idx, "PCC_free_heap");
        status = UA_Server_addVariableNode(server, UA_NODEID_NULL,   //UA_NODEID_NULL
                            UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER),
                            UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                            UA_QUALIFIEDNAME(idx, "PCC_free_heap(B)"),
                            UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                            vAttr, NULL, &available_heap_nodeID);
        if(status) printf("couldn't add available heap node\r\n");

        //--------------------------------------------------------------------------------
        //add runtime node
        vAttr = UA_VariableAttributes_default;
        vAttr.description = UA_LOCALIZEDTEXT("en-US", "The total runtime of the program in a human readable format");
        UA_Variant_setScalar(&vAttr.value, &runtime, &UA_TYPES[UA_TYPES_STRING]);
        runtime_nodeID = UA_NODEID_STRING(idx, "PCC_runtime");
        status = UA_Server_addVariableNode(server, UA_NODEID_NULL,   //UA_NODEID_NULL
                            UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER),
                            UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                            UA_QUALIFIEDNAME(idx, "PCC_runtime"),
                            UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                            vAttr, NULL, &runtime_nodeID);
        if(status) printf("couldn't add runtime node\r\n");
        //--------------------------------------------------------------------------------
        // add compile time node
        vAttr = UA_VariableAttributes_default;
        vAttr.description = UA_LOCALIZEDTEXT("en-US", "The time at which this code was compiled");
        UA_Variant_setScalar(&vAttr.value, &compile_time, &UA_TYPES[UA_TYPES_STRING]);
        compile_time_nodeID = UA_NODEID_STRING(idx, "PCC_compile_time");
        status = UA_Server_addVariableNode(server, UA_NODEID_NULL,   //UA_NODEID_NULL
                            UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER),
                            UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                            UA_QUALIFIEDNAME(idx, "PCC_compile_time"),
                            UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                            vAttr, NULL, &compile_time_nodeID);
        if(status) printf("couldn't add compile time node\r\n");
        //--------------------------------------------------------------------------------
        // add interval node
        vAttr = UA_VariableAttributes_default;
        vAttr.description = UA_LOCALIZEDTEXT("en-US", "the interval in seconds (s) in which the sensors values are updated");
        UA_Variant_setScalar(&vAttr.value, &readout_interval, &UA_TYPES[UA_TYPES_FLOAT]);
        interval_nodeID = UA_NODEID_STRING(idx, "PCC_readout_interval");
        status = UA_Server_addVariableNode(server, UA_NODEID_NULL,   //UA_NODEID_NULL
                            UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER),
                            UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                            UA_QUALIFIEDNAME(idx, "PCC_readout_interval"),
                            UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                            vAttr, NULL, &interval_nodeID);
        if(status) printf("couldn't add interval node\r\n");
        //--------------------------------------------------------------------------------
        //add compile date node
        vAttr = UA_VariableAttributes_default;
        vAttr.description = UA_LOCALIZEDTEXT("en-US", "The date at which the program was compiled");
        UA_Variant_setScalar(&vAttr.value, &compile_date, &UA_TYPES[UA_TYPES_STRING]);
        compile_date_nodeID = UA_NODEID_STRING(idx, "PCC_compile_date");
        status = UA_Server_addVariableNode(server, UA_NODEID_NULL,   //UA_NODEID_NULL
                            UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER),
                            UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                            UA_QUALIFIEDNAME(idx, "PCC_compile_date"),
                            UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                            vAttr, NULL, &compile_date_nodeID);
        if(status) printf("couldn't add runtime node\r\n");
        //--------------------------------------------------------------------------------
        //add name date node
        vAttr = UA_VariableAttributes_default;
        vAttr.description = UA_LOCALIZEDTEXT("en-US", "The name of this device");
        UA_Variant_setScalar(&vAttr.value, &device_name, &UA_TYPES[UA_TYPES_STRING]);
        name_nodeID = UA_NODEID_STRING(idx, "PCC_device_name");
        status = UA_Server_addVariableNode(server, UA_NODEID_NULL,   //UA_NODEID_NULL
                            UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER),
                            UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                            UA_QUALIFIEDNAME(idx, "PCC_device_name"),
                            UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                            vAttr, NULL, &name_nodeID);
        if(status) printf("couldn't add device name node\r\n");
        //--------------------------------------------------------------------------------
        //add runtime_UID node
        vAttr = UA_VariableAttributes_default;
        vAttr.description = UA_LOCALIZEDTEXT("en-US", "A unique identifier generated during initialisation of the code");
        UA_Variant_setScalar(&vAttr.value, &runtime_UID, &UA_TYPES[UA_TYPES_STRING]);
        runtime_uid_nodeID = UA_NODEID_STRING(idx, "PCC_runtime_UID");
        status = UA_Server_addVariableNode(server, UA_NODEID_NULL,   //UA_NODEID_NULL
                            UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER),
                            UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                            UA_QUALIFIEDNAME(idx, "PCC_runtime_UID"),
                            UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE),
                            vAttr, NULL, &runtime_uid_nodeID);
        if(status) printf("couldn't add runtime_UID node\r\n");


        init = true;
    }
    //initialise the non constants variable node variants
    UA_Variant_init(temperature_var);
    UA_Variant_init(available_heap_var);
    UA_Variant_init(runtime_var);


    //update temperature
   // UA_Variant_delete(temperature_var);
    UA_Variant_setScalar(temperature_var, &temperature, &UA_TYPES[UA_TYPES_FLOAT]);
    status = UA_Server_writeValue(server, temperature_nodeID, *temperature_var);
    if(status) {
        printf("couldn't write temperature node\r\n");
    }
    //UA_NodeId_clear(&temperature_nodeID);

    //update available heap
    //UA_Variant_delete(available_heap_var);
    UA_Variant_setScalar(available_heap_var, &available_heap, &UA_TYPES[UA_TYPES_UINT32]);
    status = UA_Server_writeValue(server, available_heap_nodeID, *available_heap_var);
    if(status) {
        printf("couldn't write heap node\r\n");
    }
    //UA_NodeId_clear(&available_heap_nodeID);

    //update runtime
    //UA_Variant_delete(runtime_var);
    UA_Variant_setScalar(runtime_var, &runtime, &UA_TYPES[UA_TYPES_STRING]);
    status = UA_Server_writeValue(server, runtime_nodeID, *runtime_var);
    if(status) {
        printf("couldn't write runtime\r\n");
    }
    //UA_NodeId_clear(&runtime_nodeID);
}
