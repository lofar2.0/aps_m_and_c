/*
 * Copyright 2020 Stichting Nederlandse Wetenschappelijk Onderzoek Instituten,
 * ASTRON Netherlands Institute for Radio Astronomy
 * Licensed under the Apache License, Version 2.0 (the "License");
 *
 * you may not use this file except in compliance with the License.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * See ../LICENSE.txt for more info.
 */


#ifndef BASE_NODES_CONFIG_H_
#define BASE_NODES_CONFIG_H_


extern "C" {
    #include "open62541.h"
}

#include "commands.h"
#include "node.h"




//root node used during testing
class TEST_base_node : public base_node {
public:
	TEST_base_node(lpi2c_rtos_handle_t *_I2C_Handle, UA_Server *mUaServer, uint8_t _I2C_num, uint16_t idx,  cmd_handler *_cmd_handler = nullptr,string _name = "TEST_base_node")
		: base_node(_I2C_Handle, mUaServer, _I2C_num, idx, _name){

	    //all stuff exclusive to the particular base nodes goes here
	    nof_subnodes = base_nof_subnodes;
	    subnodes = base_subnodes;

	    //go through the entire tree and gather some info
	    get_tree_info(total_datapoints, total_devices, total_monitored, total_nodes, total_switched_nodes, total_switches);

	    //allocate an array to track all the failed monitored devices
	    if(total_monitored){
	        monitored_failure_array = (uint8_t*)pvPortMalloc(sizeof(uint8_t) * total_monitored);
	    }

	    //allocate an array to track all the failed switches
	    if(total_switched_nodes){
	        switch_failure_array = (uint8_t*)pvPortMalloc(sizeof(uint8_t) * total_switched_nodes);
	    }

	    //allocate an array to store the state of all the switches and pointers to all the switches
	    if(total_switches){
	        switch_states = (uint8_t*)pvPortMalloc(sizeof(uint8_t) * total_switches);
	        switch_ptr_arr = (I2C_switch**)pvPortMalloc(sizeof(I2C_switch*) * total_switches);
        }

	    //fill the pointer array to all the switches
        uint8_t switch_counter = 0;
        store_switches(this, switch_counter);

        //adds the command handler to the base node, if applicable
        handler = _cmd_handler;
        if(handler != nullptr){
            handler->add_base_node(this, &switch_0, 1, &taskhandle);
        }


        //add all nodes to OPC ua and get a value to later reference all the nodeID's
	    nodeID_offset = add_OPCua(this, UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER)) - total_datapoints;

	    //initialise the I2C issue monitor
	    I2C_monitor_init();
	}


private:

/*===================================================================================
		test base node configuration starts here
===================================================================================*/

//	//all sensors of a pmbus device we're interested in
//	pmbus_sensor Vin = pmbus_sensor(0x88, LITERAL, "Input voltage (V)");
//	pmbus_sensor Vout = pmbus_sensor(0x8B, VOUT_MODE, "output voltage (V)");
//	pmbus_sensor Temp = pmbus_sensor(0x8D, LITERAL, "temperature  (C)");
//	pmbus_sensor Iout = pmbus_sensor(0x8C, LITERAL, "output current (A)");
//	//reference all sensors in a pointer array
//	static const uint8_t nof_sensors = 4;
//	pmbus_sensor *sensors[nof_sensors] = { &Vin, &Vout, &Temp, &Iout};
//
//	// create all devices that are constantly monitored
//	pmbus_device BMR451_0 = pmbus_device(0x4F, "BMR451_0x4F1", sensors);
//	pmbus_device BMR462_1 = pmbus_device(0x65, "BMR462_0x652", sensors);
//    pmbus_device BMR451_2 = pmbus_device(0x4F, "BMR451_0x4F3", sensors);
//    pmbus_device BMR462_3 = pmbus_device(0x65, "BMR462_0x654", sensors);
//    pmbus_device BMR451_4 = pmbus_device(0x4F, "BMR451_0x4F5", sensors);
//    pmbus_device BMR462_5 = pmbus_device(0x65, "BMR462_0x656", sensors);
//    pmbus_device BMR451_6 = pmbus_device(0x4F, "BMR451_0x4F7", sensors);
//    pmbus_device BMR462_7 = pmbus_device(0x65, "BMR462_0x658", sensors);
//    pmbus_device BMR451_8 = pmbus_device(0x4F, "BMR451_0x4F9", sensors);
//    pmbus_device BMR462_9 = pmbus_device(0x65, "BMR462_0x6510", sensors);
//    pmbus_device BMR451_10 = pmbus_device(0x4F, "BMR451_0x4F11", sensors);
//    pmbus_device BMR462_11 = pmbus_device(0x65, "BMR462_0x6512", sensors);
//    pmbus_device BMR451_12 = pmbus_device(0x4F, "BMR451_0x4F13", sensors);
//    pmbus_device BMR462_13 = pmbus_device(0x65, "BMR462_0x6514", sensors);
//    pmbus_device BMR451_14 = pmbus_device(0x4F, "BMR451_0x4F15", sensors);
//    pmbus_device BMR462_15 = pmbus_device(0x65, "BMR462_0x6516", sensors);
//    pmbus_device BMR451_16 = pmbus_device(0x4F, "BMR451_0x4F17", sensors);
//    pmbus_device BMR462_17 = pmbus_device(0x65, "BMR462_0x6518", sensors);
//
//
//	static const uint8_t nof_monitored = 18;	//input here the number of devices on an FPGA_node
//	monitored_device *monitored[nof_monitored] = { 	&BMR451_0, &BMR462_1,   &BMR451_2, &BMR462_3,   &BMR451_4, &BMR462_5,   &BMR451_6, &BMR462_7,   &BMR451_8, &BMR462_9,   &BMR451_10, &BMR462_11,   &BMR451_12, &BMR462_13,   &BMR451_14, &BMR462_15,   &BMR451_16, &BMR462_17};

//	//non monitored devices
//	IO_Expander IO_0 = IO_Expander(0x73, "IO_expander 0");
//
//	//create pointer array of all non-monitored devices in a node
//	static const uint8_t nof_IO_expander = 1;
//	device  *IO_expanders[nof_IO_expander] = { &IO_0 };



	//create all the subnodes attached to a switch (*devices, nof_devices, *subnodes, nof_subnodes, name, SW_channel, SW_nr, *switch)

	//switch A
//	node subnode_A = node(monitored, nof_monitored, nullptr, 0, nullptr, 0, "device_node_A", 5);
//	node subnode_B = node(monitored, nof_monitored, nullptr, 0, nullptr, 0, "device_node_B", 6);
	node subnode_A = node(nullptr, 0, nullptr, 0, nullptr, 0, "device_node_A", 5);
	node subnode_B = node(nullptr, 0, nullptr, 0, nullptr, 0, "device_node_B", 6);

	//reference all subnodes in a pointer array
	static const uint8_t nof_switch_subnodes = 2; //each switch has 4 subnodes attached to it
	node * switch_subnodes_SW_0[nof_switch_subnodes] = { &subnode_A, &subnode_B };


	//these switches are used to select the correct subnode channel
	I2C_switch switch_0 = I2C_switch(0x70, "I2C_switch_0"); //can have 0x70 to 0x77
	device *switch_0_ptr[1] = { &switch_0 };

	// create the RCU switch nodes
    node subnode_0 = node(nullptr, 0, switch_subnodes_SW_0, nof_switch_subnodes, switch_0_ptr, 1, "switch node", NONE, 0);

    cmd_handler *handler = nullptr;

	//create a pointer array pointing to the switch subnodes
	static const uint8_t base_nof_subnodes = 1;
	node *base_subnodes[base_nof_subnodes]{ &subnode_0 };

//	node root = node(nullptr, 0, base_subnodes, base_nof_subnodes, nullptr, 0, "Base node", NONE, NONE);
};
/*===================================================================================
		TEST base node configuration ends here
===================================================================================*/

#endif /* BASE_NODES_CONFIG_H_ */
