## Device manager
The device manager contains a configuration of how the APS should be read out, the code to read out all the devices on the APS and the code to switch the I2C switches at the right time. 

The device manager holds a 'device tree' like structure that matches how all the devices are connected to the microcontroller. The device tree is made up out of nodes which themselves can contain other nodes and 'device' and 'monitored_device' class objects. Those classes are used as the base class for the actual implementation of the devices.

## OPC ua server
This is the part of the code that runs the OPC ua server. This project will be merged with the device manager in the near future. Currently it contains a functioning networking stack and a functioning OPC ua server. The server itself contains the most basic OPC ua features, such as variables/objects that can be read by a client and methods that a client can call.


### TODO:
- [x] Add protocol buffer to the command handler

- [x] test PCC board components (startup/reset, Ethernet, I2C)

- [x] better integrate command handler and monitoring subsystems

- [ ] provide documentation about monitoring points and documentation

- [x] make boards available for testing by others

- [x] convert method calls into variables with callback attached

- [x] Make the software more stable, flexible and usable

- [ ] Add Watchdog to system (currently doesnt recover after wdog reset)

- [ ] Get the read system fully working

### OLD:
- [x] Optimise LWIP settings

- [x] Merge the 2 seperate projects together (OPC ua server and device manager)

- [x] Add the ability to redirect debug messages through OPC ua

- [x] create the OPC ua servers address space during initialisation of the device manager.

- [x] have the device manager push the newest sensor values to the servers address space.

- [x] Add fault detection and handling for I2C devices/ units.

- [x] Complete the command handler code to halt the sensor readout whenever an new command has been received by the client

- [x] Use external SDRAM for 32MB extra memory

- [ ] ~~Add subscription support if possible~~ requires far more storage, not possible for embeddded devices

- [x] provide additional nodes about the status of the system



