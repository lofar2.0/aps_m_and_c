# README

This project was made in MCUXpresso, an eclipse based development environment for NXP microcontrollers. This IDE takes care of mapping the code to memory, the flash/ external memory controllers as well as several other configurations. 
https://www.nxp.com/design/software/development-software/mcuxpresso-software-and-tools/mcuxpresso-integrated-development-environment-ide:MCUXpresso-IDE


EmbeddedArtist provides instructions for downloading and modifying their and NXP's files to work with their modules. Instructions can be found under the Rescources tab
https://www.embeddedartists.com/products/imx-rt1062-oem/

Using the OPC ua server application will likely require manually changing IP addresses for both the LWIP stack and the OPC ua server.

## Python client
The python client requires FreeOPCu
https://github.com/FreeOpcUa/python-opcua

There is also a client specifically for reading out debug messages. Note that this only works when the M&C server is compiled without semihosting. The redirection works by storing a number of previous debug messages

