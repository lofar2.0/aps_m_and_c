import sys
import time
import logging
sys.path.insert(0, "..")

Temp_only = True

try:
    from IPython import embed
except ImportError:
    import code

    def embed():
        vars = globals()
        vars.update(locals())
        shell = code.InteractiveConsole(vars)
        shell.interact()

from opcua import Client
from opcua import ua


def datachange_notification(self, node, val, data):
    print("Python: New data change event", node, val)


def event_notification(self, event):
    print("Python: New event", event)


def explore(node):
    children = node.get_children()
    for i in range(len(children)):

        print("node ", i, ": ", children[i].get_browse_name())
        try:
            print("has a value of: ", children[i].get_value())
        except:
            print("no variables to read")

        explore(children[i])


def explore_temp(node):
    children = node.get_children()
    for i in range(len(children)):
        try:
            print("temperature: ", children[i].get_value())
        except:
            print("no variables to read")
        explore(children[i])


if __name__ == "__main__":
    logging.basicConfig(level=logging.ERROR)
    # logger = logging.getLogger("KeepAlive")
    # logger.setLevel(logging.DEBUG)

    # client = Client("opc.tcp://LAPTOP-N0VQ3UDT:4840/")
    client = Client("opc.tcp://192.168.137.102:4840/")
    # client = Client("opc.tcp://169.254.91.66:4840/")
    # I hope this is secure, because I have zero clue about security
    # client.set_security_string("Basic256Sha256,SignAndEncrypt,certificate-example.der,private-key-example.pem")

    while 1:
        try:
            client.connect()
            print("I'm in")

            ################
            # this section  contains some code about navigating around the address space
            ####################
            # Client has a few methods to get proxy to UA nodes that should always be in address space such as Root or Objects

            while 1:
                print("")
                print("=============================")

                root = client.get_root_node()
                print("Root node is: ", root)
                print("name of root is: ", root.get_display_name())

                # get the objects nodes NodeID
                Object = client.get_objects_node()
                print("\tObject node: ", Object)
                print("\tname of object node is: ", Object.get_browse_name())

                # get all the children that "Object" has
                children = Object.get_children()
                print("\tchildren of Object: ", children)
                for i in range(len(children)):
                    print("\t\tchild", i, ": ", children[i].get_browse_name())

                print("\t\t\t", "has a value of: ", children[1].get_value())

                while Temp_only:
                    explore_temp(children[1])
                    time.sleep(1)


                explore(children[1])
                time.sleep(5)



            # sub.unsubscribe(handle)
            # sub.delete()
        except:
            print(" no connection || something went wrong")
            # if already connected, but the client makes a faulty request causes quick requeusts to the server
            # leading to "memp_malloc: out of memory in pool NETCONN" fom the server
            time.sleep(1)
