# this script connects to a server (specified in this file) and prints any debug messages the server has
# The server stores its most recent degug messages in a buffer. This client polls that buffer every 2 seconds
# The client checks for duplicates and prints any new debug messages.
# currently a small issue where the order is incorrect when the server buffer jumps back from max_pos to 0

import sys
import time
import logging
sys.path.insert(0, "..")

from opcua import Client

if __name__ == "__main__":
	logging.basicConfig(level=logging.WARNING) #INFO ERROR WARNING NOTSET DEBUG

	# for now, just connect to this client
	client = Client("opc.tcp://192.168.137.102:4840/")

	try:
		# connect to client
		client.connect()
		print("I'm in")

		# get the root node
		root = client.get_root_node()

		# get the debug node
		debug_node = root.get_child(["0:Objects", "1:Debug_redirect"])

		# get the debug messages
		debug_msg = debug_node.get_children()

		nof_msg = len(debug_msg)

		print("")
		print("~~~~~~~~~~~~~~~~~~~~~~~~~")

		# time.sleep(1)

		name_store = []

		# store all node names and print all debug messages
		for i in range(nof_msg):
			time.sleep(0.01)
			name_store.append(1)
			name_store[i] = debug_msg[i].get_browse_name()
			# print(debug_msg[i].get_browse_name())
			print(debug_msg[i].get_value(), end='')



		while True:

			# go through all the variable nodes
			for i in range(nof_msg):
				duplicate = False
				name_holder = debug_msg[i].get_browse_name()	 # get a new variable node name

				# stored preemtively to decrease chances of being changed in the meantime by the server
				msg_holder = debug_msg[i].get_value()

				for j in range(nof_msg):	 # check if this name is already stored
					if name_holder == name_store[j]:	 # if it already exists, skip
						duplicate = True
						break

				if not duplicate:	 # if this message hasnt been stored yet
					# TODO: check higher entries first when i == 0. In that case they might store newer entries and mess the order up
					name_store[i] = name_holder
					print(name_holder, ": ", msg_holder, end='')

				time.sleep(0.01)

			time.sleep(2)

	except:
		print(" no connection || error")
		time.sleep(1)
	finally:
		print(" disconnect")
		client.disconnect()
		time.sleep(1)
